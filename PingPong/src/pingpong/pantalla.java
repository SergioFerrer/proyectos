/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pingpong;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;



public class pantalla extends javax.swing.JFrame {
    Timer rel;
    int direcion=0;
    
    public pantalla() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pan = new javax.swing.JPanel();
        pelota = new javax.swing.JLabel();
        bp = new javax.swing.JLabel();
        bc = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        start = new javax.swing.JButton();
        reset = new javax.swing.JButton();
        fin = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pelota.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pingpong/Sin título.jpg"))); // NOI18N

        bp.setBackground(new java.awt.Color(0, 0, 204));
        bp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pingpong/barrap.jpg"))); // NOI18N

        bc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pingpong/barraia.jpg"))); // NOI18N

        jButton1.setText("down");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setLabel("up");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        start.setText("START");
        start.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startActionPerformed(evt);
            }
        });

        reset.setText("RESET");
        reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetActionPerformed(evt);
            }
        });

        fin.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        fin.setForeground(new java.awt.Color(204, 0, 51));

        javax.swing.GroupLayout panLayout = new javax.swing.GroupLayout(pan);
        pan.setLayout(panLayout);
        panLayout.setHorizontalGroup(
            panLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panLayout.createSequentialGroup()
                        .addComponent(bc)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(pelota, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(168, 168, 168)
                        .addComponent(bp)
                        .addGap(79, 79, 79))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(panLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(start, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(reset, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(panLayout.createSequentialGroup()
                .addGap(146, 146, 146)
                .addComponent(fin, javax.swing.GroupLayout.PREFERRED_SIZE, 248, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(169, Short.MAX_VALUE))
        );
        panLayout.setVerticalGroup(
            panLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(fin, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panLayout.createSequentialGroup()
                        .addComponent(bc)
                        .addGap(32, 32, 32))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panLayout.createSequentialGroup()
                        .addComponent(pelota)
                        .addGap(72, 72, 72))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panLayout.createSequentialGroup()
                        .addComponent(bp)
                        .addGap(55, 55, 55)))
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(start, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(reset, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jButton1.getAccessibleContext().setAccessibleName("ba");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
     if(!(bp.getY()<=0)){
         bp.setLocation(bp.getX(), bp.getY()-17);
     }
     else {
         bp.setLocation(bp.getX(),0);
     }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
         if(!(bp.getY()+bp.getHeight()>=pan.getY()+pan.getHeight())){
         bp.setLocation(bp.getX(), bp.getY()+17);
     }
    
    
    }//GEN-LAST:event_jButton1ActionPerformed

    private void startActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startActionPerformed
        rel = new Timer (10,new reloj());
        rel.start();
        start.setVisible(false);
        reset.setVisible(true);
        direcion = 0;
    }//GEN-LAST:event_startActionPerformed

    private void resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetActionPerformed
        start.setVisible(true);
        reset.setVisible(false);
        direcion = -1;
        double x= (pan.getX()+pan.getWidth())/2.0;
        double y= (pan.getY()+pan.getHeight())/2.0;
        pelota.setLocation((int)x,(int) y);
        fin.setText("");
        bc.setLocation(15, (int)y-bc.getHeight()/2);
    }//GEN-LAST:event_resetActionPerformed

  
    public static void main(String args[]) {
       
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(pantalla.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(pantalla.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(pantalla.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(pantalla.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new pantalla().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel bc;
    private javax.swing.JLabel bp;
    private javax.swing.JLabel fin;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JPanel pan;
    private javax.swing.JLabel pelota;
    private javax.swing.JButton reset;
    private javax.swing.JButton start;
    // End of variables declaration//GEN-END:variables

public class reloj implements ActionListener{

      
        public void actionPerformed(ActionEvent e){
         if(direcion <0){rel.stop();}
            if (direcion==0){direcion = elegir();}
         
         //ver si alguien ha ganado
            if(pelota.getX()==0 ){
            start.setVisible(true);
            reset.setVisible(false);
            direcion = -1;
            double x= (pan.getX()+pan.getWidth())/2.0;
            double y= (pan.getY()+pan.getHeight())/2.0;
            pelota.setLocation((int)x,(int) y);
            bc.setLocation(15, (int)y-bc.getHeight()/2);
            fin.setText("HAS GANADO!!");
            
            }   
            if(pelota.getX()==pan.getWidth() ){
            start.setVisible(true);
            reset.setVisible(false);
            direcion = -1;
            double x= (pan.getX()+pan.getWidth())/2.0;
            double y= (pan.getY()+pan.getHeight())/2.0;
            pelota.setLocation((int)x,(int) y);
            bc.setLocation(15, (int)y-bc.getHeight()/2);
            fin.setText("HAS PERDIDO!!");
            
            }
            
            
            //calculo movimiento barra IA
         if(pelota.getX()<=(pan.getX()+pan.getWidth())/2){
             switch(direcion){
                 case 1: 
                     if(!(bc.getY()<=0)){
                     bc.setLocation(bc.getX(),bc.getY()-1); break;
                     }
                 case 2: 
                     if(!(bc.getY()+bc.getHeight()>=pan.getY()+pan.getHeight())){
                     bc.setLocation(bc.getX(),bc.getY()+1);break;
                     }
                 case 3:   if(!(bc.getY()+bc.getHeight()>=pan.getY()+pan.getHeight())){
                     bc.setLocation(bc.getX(),bc.getY()+1);break;
                     }   
                 case 4: if(!(bc.getY()<=0)){
                     bc.setLocation(bc.getX(),bc.getY()-1); break;
                     }
                     
             
         
         }
         }
         else{
             switch(direcion){
                 case 4: if(!(bc.getY()<=0)){
                     bc.setLocation(bc.getX(),bc.getY()-1);break;
                 }
                 case 3:if(!(bc.getY()+bc.getHeight()>=pan.getY()+pan.getHeight())){
                     bc.setLocation(bc.getX(),bc.getY()+1); break;
                     }
             }
         
         }
        
         
         switch(direcion){
         
             case 1:
                 pelota.setLocation(pelota.getX()-1,pelota.getY()-1);
                 break;
             case 2:
                 pelota.setLocation(pelota.getX()-1,pelota.getY()+1);
                 break;
             case 3:
                 pelota.setLocation(pelota.getX()+1,pelota.getY()+1);
                 break;
             case 4:
                 pelota.setLocation(pelota.getX()+1,pelota.getY()-1);
                 break;
            }
         double centro = pelota.getY()+(pelota.getHeight()/2.0);
         //toca mi barra
         if((pelota.getX()+pelota.getWidth()==bp.getX())&& centro>=bp.getY()&&centro<= bp.getY()+bp.getHeight()){//toca mi barra
             switch(direcion){
                 case 4: direcion = 1; break;
                 case 3: direcion = 2; break;
          }
          
         }    
         if (pelota.getY()+pelota.getHeight()>= pan.getY()+pan.getHeight()) {
             //toca nivel inferior
             switch(direcion){
                 case 2: direcion = 1; break;
                 case 3: direcion = 4; break;
             }
         }
        if (pelota.getY()<=pan.getY()){ //toca nivel superior;
            switch(direcion){
                case 4: direcion = 3; break;
                case 1: direcion = 2; break;
            }
        }
        
        if ((pelota.getX()== bc.getX()+bc.getWidth())&&centro>=bc.getY()&& centro<=bc.getY()+bc.getHeight()){
           //toca la barra de la IA
            switch(direcion){
                case 1: direcion = 4; break;
                case 2: direcion = 3; break;
            }
        
      
        }  
        
        
    }
}            
          
        
                
                
            
            
        







    public static int elegir(){
        int direc = (int)(Math.random()*4+1);
        return direc;
    }


  
}



