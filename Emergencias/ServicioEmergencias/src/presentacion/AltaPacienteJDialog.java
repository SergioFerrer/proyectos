package presentacion;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logica.OC;
import logica.Paciente;

public class AltaPacienteJDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	JRadioButton rdbtnHombre;
	JRadioButton rdbtnMujer;
	
	private boolean llamado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			AltaPacienteJDialog dialog = new AltaPacienteJDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public AltaPacienteJDialog() {
		llamado= false;
		setTitle("ALTA PACIENTE");
		setBounds(100, 100, 450, 300);
		setResizable(false);
		getContentPane().setLayout(new BorderLayout());
		setLocationRelativeTo(null);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblDni = new JLabel("DNI:");
		lblDni.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDni.setBounds(18, 18, 66, 16);
		contentPanel.add(lblDni);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNombre.setBounds(18, 46, 66, 16);
		contentPanel.add(lblNombre);
		
		JLabel lblApellidos = new JLabel("Apellidos:");
		lblApellidos.setHorizontalAlignment(SwingConstants.RIGHT);
		lblApellidos.setBounds(18, 74, 66, 16);
		contentPanel.add(lblApellidos);
		
		JLabel lblDireccin = new JLabel("Direcci\u00F3n:");
		lblDireccin.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDireccin.setBounds(18, 102, 66, 16);
		contentPanel.add(lblDireccin);
		
		JLabel lblEdad = new JLabel("Edad:");
		lblEdad.setHorizontalAlignment(SwingConstants.RIGHT);
		lblEdad.setBounds(18, 130, 66, 16);
		contentPanel.add(lblEdad);
		
		JLabel lblTelfono = new JLabel("Tel\u00E9fono:");
		lblTelfono.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTelfono.setBounds(18, 158, 66, 16);
		contentPanel.add(lblTelfono);
		
		JLabel lblSexo = new JLabel("Sexo:");
		lblSexo.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSexo.setBounds(18, 186, 66, 16);
		contentPanel.add(lblSexo);
		
		textField = new JTextField();
		textField.setBounds(106, 15, 122, 22);
		contentPanel.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(106, 43, 122, 22);
		contentPanel.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(106, 71, 267, 22);
		contentPanel.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(106, 99, 267, 22);
		contentPanel.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(106, 127, 55, 22);
		contentPanel.add(textField_4);
		textField_4.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setBounds(106, 155, 122, 22);
		contentPanel.add(textField_5);
		textField_5.setColumns(10);
		
		rdbtnHombre = new JRadioButton("Hombre");
		buttonGroup.add(rdbtnHombre);
		rdbtnHombre.setBounds(106, 185, 77, 18);
		contentPanel.add(rdbtnHombre);
		
		rdbtnMujer = new JRadioButton("Mujer");
		buttonGroup.add(rdbtnMujer);
		rdbtnMujer.setBounds(185, 185, 66, 18);
		contentPanel.add(rdbtnMujer);
		
		JLabel lblNewLabel = new JLabel("(8 d\u00EDgitos con o sin la letra)");
		lblNewLabel.setBounds(238, 19, 196, 14);
		contentPanel.add(lblNewLabel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Insertar");
				okButton.setEnabled(true);
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try{
							String dni = textField.getText();
							String nombre = textField_1.getText();
							String apellidos = textField_2.getText();
							String direccion = textField_3.getText();
							String edadString = textField_4.getText();
							int edad = Integer.parseInt(edadString);
							String telefonoString = textField_5.getText();
							int telefono = Integer.parseInt(telefonoString);
							char sexo = 'N';
							if(rdbtnHombre.isSelected()) sexo = 'H';
							if(rdbtnMujer.isSelected()) sexo = 'M';
							if(dni.length() == 8 || dni.length() == 9){
								String subDni = dni.substring(0, 8);
								int numDni = Integer.parseInt(subDni);
								dni = "" + numDni;
							}
							else dni = "";
							if(dni.equals("")) JOptionPane.showMessageDialog(null, "El DNI insertado no es correcto",
									"",JOptionPane.ERROR_MESSAGE);
							else if (nombre.equals("") || apellidos.equals("") 
									|| direccion.equals("")|| sexo == 'N' || edad<0){
								JOptionPane.showMessageDialog(null, "Inserta bien los campos",
										"",JOptionPane.ERROR_MESSAGE);
							}
							else{
								Paciente p = new Paciente(dni, nombre, 
										apellidos, direccion, edad, telefono, sexo);
								OC prueba = OC.dameControlador();
								if(!prueba.altaPaciente(p)) {
									JOptionPane.showMessageDialog(null, "Paciente ya existente en la BD",
											"",JOptionPane.ERROR_MESSAGE);
								
								}
								else{
									JOptionPane.showMessageDialog(null, "Paciente a�adido a la BD", 
											"", JOptionPane.INFORMATION_MESSAGE);
									textField.setText("");
									textField_1.setText("");
									textField_2.setText("");
									textField_3.setText("");
									textField_4.setText("");
									textField_5.setText("");
									rdbtnHombre.setSelected(false);
									rdbtnMujer.setSelected(false);
									dispose();
									if(llamado) {
									AltaEmergenciaJDialog altaEmerg = new AltaEmergenciaJDialog(p);
									altaEmerg.setModal(true);
									altaEmerg.setVisible(true);
									}
								}
							}
							
						}catch(Exception e1){
							JOptionPane.showMessageDialog(null, "Inserta bien los campos"
									,"Error",JOptionPane.ERROR_MESSAGE);
							}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Salir");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	public void setLlamado() {
		llamado=!llamado;
	}
	
	public void sendDNI(String dni) {
		textField.setText(dni);
	}
}
