package presentacion;

import java.util.ArrayList;

import logica.Sintoma;

public class LlamadaDeEmergencia {

	private double IDregistro;
	private double latitud, longitud;
	private String fecha,hora; //RegistroEmergencia
	private String dni,nombre,apellidos;
	private int telefono; //Paciente
	private String numRegistro;
	private String equipo;		//Ambulancia
	private String companyia; //Privada
	private String nombreHosp; //Hospital
	private String sintomas; //Lo he puesto como String para en ListarPaciente convertir 
							//el ArrayList de sintomas que devuelve registroEmergencia en
							//String con formato adecuado para entrar en tabal. Si no funciona
							//cambiar porque aqu� hay una posible fuente de error (lo dem�s iba
							//antes de tocar esto).
	
	public LlamadaDeEmergencia(double IDreg, double lat, double lon, String fech, String hor, String id, String nom,
			String apel, int tel, String nReg, String eq, String comp, String nH, String sin){
		
		IDregistro = IDreg;
		latitud = lat;
		longitud = lon;
		fecha = fech;
		hora = hor;
		dni = id;
		nombre = nom;
		apellidos = apel;
		telefono = tel;
		numRegistro = nReg;
		equipo = eq;
		companyia = comp;
		nombreHosp = nH;
		sintomas = sin;
	}

	public double getIDregistro() {
		return IDregistro;
	}

	public void setIDregistro(double iDregistro) {
		IDregistro = iDregistro;
	}

	public double getLatitud() {
		return latitud;
	}

	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}

	public double getLongitud() {
		return longitud;
	}

	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String getNumRegistro() {
		return numRegistro;
	}

	public void setNumRegistro(String numRegistro) {
		this.numRegistro = numRegistro;
	}

	public String getEquipo() {
		return equipo;
	}

	public void setEquipo(String equipo) {
		this.equipo = equipo;
	}

	public String getCompanyia() {
		return companyia;
	}

	public void setCompanyia(String companyia) {
		this.companyia = companyia;
	}

	public String getNombreHosp() {
		return nombreHosp;
	}

	public void setNombreHosp(String nombreHosp) {
		this.nombreHosp = nombreHosp;
	}

	public String getSintomas() {
		return sintomas;
	}

	public void setSintomas(String sintomas) {
		this.sintomas = sintomas;
	}
}
