package presentacion;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;

import logica.Duo;
import logica.Especialidad;
import logica.OC;
import logica.Paciente;
import logica.RegistroEmergencia;
import logica.Sintoma;

public class AltaEmergenciaJDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;
	private JButton okButton;
	private JButton btnIndicarCoordenadas;
	public JTable table;
	private JComboBox comboBox;
	private Paciente p;
	private ArrayList<Duo> duos = new ArrayList<Duo>();
	public ArrayList<Sintoma> sintomas = new ArrayList<Sintoma>();
	private AltaSintoma sintoma;
	private Sintoma s;
	private String especialidad;
	private Duo d;
	public Duo de;
	public AltaEmergenciaJDialog aux;
	public RegistroEmergencia RE;

	class DuoTableModel extends AbstractTableModel {

		private static final long serialVersionUID = 1L;
		private String[] columnNames = { "Especialidad", "S�ntoma" };

		private ArrayList<Duo> data = new ArrayList<Duo>();

		public int getColumnCount() {
			return columnNames.length;
		}

		public int getRowCount() {
			return data.size();
		}

		public String getColumnName(int col) {
			return columnNames[col];
		}

		// ARREGLAR
		public Object getValueAt(int row, int col) {
			Duo in = data.get(row);
			switch (col) {
			case 0:
				return in.espe;
			case 1:
				return in.sint;
			default:
				return null;
			}

		}

		public void clear() {
			data.clear();
		}

		public Class<? extends Object> getColumnClass(int c) {
			return getValueAt(0, c).getClass();
		}

		public void addRow(Duo row) {
			data.add(row);
			this.fireTableDataChanged();

		}

		public void delRow(int row) {
			data.remove(row);
			this.fireTableDataChanged();

		}
	}

	/**
	 * Launch the application.
	 */
	public DuoTableModel model;

	public static void main(String[] args) {
		try {
			AltaEmergenciaJDialog dialog = new AltaEmergenciaJDialog(null);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public AltaEmergenciaJDialog(Paciente pa) {
		aux = this;
		p = pa;
		setTitle("ALTA EMERGENCIA");
		setBounds(100, 100, 469, 635);
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JLabel lblDni = new JLabel("DNI:");
		lblDni.setHorizontalAlignment(SwingConstants.LEFT);
		lblDni.setBounds(36, 9, 66, 16);
		contentPanel.add(lblDni);

		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setHorizontalAlignment(SwingConstants.LEFT);
		lblNombre.setBounds(36, 40, 66, 16);
		contentPanel.add(lblNombre);

		JLabel lblApellidos = new JLabel("Apellidos:");
		lblApellidos.setHorizontalAlignment(SwingConstants.LEFT);
		lblApellidos.setBounds(36, 68, 66, 16);
		contentPanel.add(lblApellidos);

		JLabel lblDireccin = new JLabel("Direcci\u00F3n:");
		lblDireccin.setHorizontalAlignment(SwingConstants.LEFT);
		lblDireccin.setBounds(36, 96, 66, 16);
		contentPanel.add(lblDireccin);

		JLabel lblEdad = new JLabel("Edad:");
		lblEdad.setHorizontalAlignment(SwingConstants.LEFT);
		lblEdad.setBounds(36, 124, 66, 16);
		contentPanel.add(lblEdad);

		JLabel lblTelfono = new JLabel("Tel\u00E9fono:");
		lblTelfono.setHorizontalAlignment(SwingConstants.LEFT);
		lblTelfono.setBounds(36, 152, 66, 16);
		contentPanel.add(lblTelfono);

		JLabel lblSexo = new JLabel("Sexo:");
		lblSexo.setHorizontalAlignment(SwingConstants.LEFT);
		lblSexo.setBounds(36, 180, 66, 16);
		contentPanel.add(lblSexo);

		JLabel lblNewLabel_7 = new JLabel("Latitud:");
		lblNewLabel_7.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_7.setBounds(36, 208, 66, 16);
		contentPanel.add(lblNewLabel_7);

		JLabel lblNewLabel_8 = new JLabel("Longitud:");
		lblNewLabel_8.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_8.setBounds(36, 236, 66, 16);
		contentPanel.add(lblNewLabel_8);

		JLabel lblNewLabel_9 = new JLabel("Nueva Latitud:");
		lblNewLabel_9.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_9.setBounds(36, 290, 99, 23);
		contentPanel.add(lblNewLabel_9);

		JLabel lblNewLabel_10 = new JLabel("Nueva Longitud:");
		lblNewLabel_10.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_10.setBounds(36, 318, 99, 23);
		contentPanel.add(lblNewLabel_10);

		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(136, 6, 122, 22);
		contentPanel.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setBounds(136, 37, 122, 22);
		contentPanel.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setEditable(false);
		textField_2.setBounds(136, 65, 267, 22);
		contentPanel.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setEditable(false);
		textField_3.setBounds(136, 93, 267, 22);
		contentPanel.add(textField_3);
		textField_3.setColumns(10);

		textField_4 = new JTextField();
		textField_4.setEditable(false);
		textField_4.setBounds(136, 121, 55, 22);
		contentPanel.add(textField_4);
		textField_4.setColumns(10);

		textField_5 = new JTextField();
		textField_5.setEditable(false);
		textField_5.setBounds(136, 149, 122, 22);
		contentPanel.add(textField_5);
		textField_5.setColumns(10);

		textField_6 = new JTextField();
		textField_6.setEditable(false);
		textField_6.setBounds(136, 177, 122, 22);
		contentPanel.add(textField_6);
		textField_6.setColumns(10);

		textField_7 = new JTextField();
		textField_7.setEditable(false);
		textField_7.setBounds(136, 205, 122, 22);
		contentPanel.add(textField_7);
		textField_7.setColumns(10);

		textField_8 = new JTextField();
		textField_8.setEditable(false);
		textField_8.setBounds(136, 233, 122, 22);
		contentPanel.add(textField_8);
		textField_8.setColumns(10);

		textField_9 = new JTextField();
		textField_9.setBounds(136, 290, 122, 22);
		contentPanel.add(textField_9);
		textField_9.setColumns(10);

		textField_10 = new JTextField();
		textField_10.setBounds(136, 318, 122, 22);
		contentPanel.add(textField_10);
		textField_10.setColumns(10);

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		{
			btnIndicarCoordenadas = new JButton("Indicar coordenadas");
			btnIndicarCoordenadas.setBounds(270, 301, 167, 28);
			contentPanel.add(btnIndicarCoordenadas);
			btnIndicarCoordenadas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					try {
						double latitud = Double.parseDouble(textField_9
								.getText());
						double longitud = Double.parseDouble(textField_10
								.getText());
						/*
						 * RegistroEmergencia RE = new
						 * RegistroEmergencia(latitud, longitud, "fecha",
						 * "hora", null, p);
						 */
						textField_7.setText("" + latitud);
						textField_8.setText("" + longitud);
						textField_9.setText("");
						textField_10.setText("");
						btnIndicarCoordenadas.setText("Cambiar coordenadas");
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(null,
								"Inserta bien los campos", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			});
		}

		JLabel lblEspecialidad = new JLabel("Especialidad:");
		lblEspecialidad.setBounds(36, 367, 88, 16);
		contentPanel.add(lblEspecialidad);

		comboBox = new JComboBox();
		comboBox.setBounds(136, 362, 122, 25);
		contentPanel.add(comboBox);
		ArrayList<Especialidad> especialidades = OC.dameControlador()
				.listarEspecialidades();
		for (int i = 0; i < especialidades.size(); i++) {
			comboBox.addItem(especialidades.get(i).getNombre());
		}

		JButton btnAadirSntomas = new JButton("A\u00F1adir s\u00EDntoma");
		btnAadirSntomas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String especialidad = (String) comboBox.getSelectedItem();
				sintoma = new AltaSintoma(aux, especialidad);
				sintoma.setModal(true);
				sintoma.setVisible(true);

				/*
				 * DuoTableModel model=(DuoTableModel) table.getModel();
				 * 
				 * model.addRow(de);
				 */

			}

		});
		btnAadirSntomas.setBounds(270, 362, 167, 25);
		contentPanel.add(btnAadirSntomas);
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(36, 414, 401, 119);
		contentPanel.add(scrollPane_1);

		table = new JTable(new DuoTableModel());
		scrollPane_1.setViewportView(table);

		textField.setText(p.getDni());
		textField_1.setText(p.getNombre());
		textField_2.setText(p.getApellidos());
		textField_3.setText(p.getDireccion());
		textField_4.setText("" + p.getEdad());
		textField_5.setText("" + p.getTelefono());

		JButton btnAadirEmergencia = new JButton("A\u00F1adir Emergencia");
		btnAadirEmergencia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					RE = new RegistroEmergencia(Double
							.parseDouble(textField_7.getText()), Double
							.parseDouble(textField_8.getText()), "fecha",
							"hora", sintomas, p);
					int id = (int)RE.getIDregistro();
					for(int k=0; k<sintomas.size();k++) {
						sintomas.get(k).setID(id);
					}
					RE.setSintomas(sintomas);
					RE = OC.dameControlador().asignarAmbyHosp(RE);
					if (RE == null) {
						JOptionPane
								.showMessageDialog(
										null,
										"No hay ambulancias u hospitales para atender a la emergencia",
										"", JOptionPane.ERROR_MESSAGE);
					}
					else {
						
					
					
					if (OC.dameControlador().esPrivada(RE.getAmbulancia())) {
						
						JOptionPane.showMessageDialog(
								null,
								"Ambulancia privada asignada: "
										+ RE.getAmbulancia().getNumRegistro()
										+ "\nHospital asignado: "
										+ RE.getHospital().getNombre()
										+ "\nCompa��a: "
										+ OC.dameControlador().companyiaDeAmb(
												RE.getAmbulancia()), "",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(null,
								"Ambulancia p�blica asignada: "
										+ RE.getAmbulancia().getNumRegistro()
										+ "\nHospital asignado: "
										+ RE.getHospital().getNombre(), "",
								JOptionPane.INFORMATION_MESSAGE);
					}
					/*Calendar c = Calendar.getInstance();
					RE.setFecha(c.DAY_OF_MONTH+"/"+c.MONTH+"/"+c.YEAR);
					RE.setHora(c.HOUR_OF_DAY+":"+c.MINUTE);
					System.out.println(RE.getFecha()+"\n" + RE.getHora());*/
					}
					dispose();
					

				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null,
							"Inserta bien los campos", "Error",
							JOptionPane.ERROR_MESSAGE);
				}

			}
		});
		btnAadirEmergencia.setBounds(153, 561, 183, 28);
		contentPanel.add(btnAadirEmergencia);
		
		JButton btnNewButton = new JButton("Cancelar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnNewButton.setBounds(346, 563, 91, 26);
		contentPanel.add(btnNewButton);
		if (p.getSexo() == 'H')
			textField_6.setText("Hombre");
		else
			textField_6.setText("Mujer");

	}
}
