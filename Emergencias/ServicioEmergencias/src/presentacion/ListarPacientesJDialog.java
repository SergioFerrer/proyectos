package presentacion;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import logica.OC;
import logica.Paciente;



public class ListarPacientesJDialog extends JDialog {
	private JTable table;
	class PacienteTableModel extends AbstractTableModel {

		private static final long serialVersionUID = 1L;
		private String[] columnNames = { "DNI", "Nombre" , "Apellidos", "Direcci�n" ,
		"Edad", "Tel�fono","Sexo"};

		private ArrayList<Paciente> data=new ArrayList<Paciente>();

		public int getColumnCount() {
		return columnNames.length;
		}
		public int getRowCount() {
		return data.size();
		}
		public String getColumnName(int col) {
		return columnNames[col];
		}
		public Object getValueAt(int row, int col) {
		Paciente in =data.get(row);
		switch(col){
		case 0: return in.getDni();
		case 1: return in.getNombre();
		case 2: return in.getApellidos();
		case 3: return in.getDireccion();
		case 4: return in.getEdad();
		case 5: return in.getTelefono();
		case 6: return in.getSexo();
		default: return null;
		}

		}

		public void clear(){
		data.clear();}
		public Class<? extends Object> getColumnClass(int c) {
		return getValueAt(0, c).getClass();
		}
		public void addRow(Paciente row) {
		data.add(row);
		this.fireTableDataChanged();

		}

		public void delRow(int row) {
		data.remove(row);
		this.fireTableDataChanged();

		}
		}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ListarPacientesJDialog dialog = new ListarPacientesJDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ListarPacientesJDialog() {
		setTitle("LISTAR PACIENTES");
		setBounds(100, 100, 800, 500);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		setLocationRelativeTo(null);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane.gridheight = 5;
		gbc_scrollPane.gridwidth = 7;
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		getContentPane().add(scrollPane, gbc_scrollPane);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane.setViewportView(scrollPane_1);
		
		table = new JTable(new PacienteTableModel());
	    scrollPane_1.setViewportView(table);
	    cargaPacientes();
		table.getColumn("Apellidos").setPreferredWidth(150);
		table.getColumn("Direcci�n").setPreferredWidth(150);
		table.getColumn("Sexo").setPreferredWidth(30);
		table.getColumn("Edad").setPreferredWidth(30);
	}
	public void cargaPacientes(){
		try{
		//ArrayList<Paciente> listaPacientes= (ArrayList<Paciente>);
		OC oc = OC.dameControlador();
		//Iterator<Paciente>it= listaPacientes.iterator();
		ArrayList<Paciente> listaPacientes = oc.listarPacientes();
		//Paciente pa;
		PacienteTableModel model=(PacienteTableModel) table.getModel();
		model.clear();
		//while (it.hasNext()){
		//pa=it.next();
		//model.addRow(pa);
		//}
		for (int i = 0; i<listaPacientes.size();i++){
			model.addRow(listaPacientes.get(i));
		}
		}catch (Exception e){
		JOptionPane.showMessageDialog(this,e.getMessage(),"ERROR",
		JOptionPane.ERROR_MESSAGE);
		}
		}

}
