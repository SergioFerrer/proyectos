package presentacion;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import logica.OC;
import logica.RegistroEmergencia;
import logica.Sintoma;

public class ListarLlamadasJDialog extends JDialog {
	private JTable table;
	class EmergenciaTableModel extends AbstractTableModel {

		private static final long serialVersionUID = 1L;
		private String[] columnNames = { "ID registro", "Latitud" , "Longitud", "Fecha" ,
		"Hora", "DNI", "nombre", "apellidos", "telefono", "ID ambulancia", "equipo", "compa�ia",
		"IDHospital", "sintomas"};

		private ArrayList<LlamadaDeEmergencia> data = new ArrayList<LlamadaDeEmergencia>();

		public int getColumnCount() {
		return columnNames.length;
		}
		public int getRowCount() {
		return data.size();
		}
		public String getColumnName(int col) {
		return columnNames[col];
		}
		public Object getValueAt(int row, int col) {
		LlamadaDeEmergencia in =data.get(row);
		switch(col){
		case 0: return in.getIDregistro();
		case 1: return in.getLatitud();
		case 2: return in.getLongitud();
		case 3: return in.getFecha();
		case 4: return in.getHora();
		case 5: return in.getDni();
		case 6: return in.getNombre();
		case 7: return in.getApellidos();
		case 8: return in.getTelefono();
		case 9: return in.getNumRegistro();
		case 10: return in.getEquipo();
		case 11: return in.getCompanyia();
		case 12: return in.getNombreHosp();
		case 13: return in.getSintomas();
		default: return null;
		}

		}

		public void clear(){
		data.clear();}
		public Class<? extends Object> getColumnClass(int c) {
		return getValueAt(0, c).getClass();
		}
		public void addRow(LlamadaDeEmergencia row) {
		data.add(row);
		this.fireTableDataChanged();

		}

		public void delRow(int row) {
		data.remove(row);
		this.fireTableDataChanged();

		}
		}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ListarLlamadasJDialog dialog = new ListarLlamadasJDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ListarLlamadasJDialog() {
		setTitle("LISTAR LLAMADAS");
		Esperar esperar = new Esperar();
		esperar.setVisible(true);
		
		setBounds(50, 50, 1100, 500);
	    setLocationRelativeTo(null);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane.gridheight = 5;
		gbc_scrollPane.gridwidth = 7;
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		getContentPane().add(scrollPane, gbc_scrollPane);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane.setViewportView(scrollPane_1);
		
		table = new JTable(new EmergenciaTableModel());
	    scrollPane_1.setViewportView(table);
	    cargaLlamadas();
	    esperar.dispose();
	}
	
	public void cargaLlamadas(){
		try{
		OC oc = OC.dameControlador();
		ArrayList<RegistroEmergencia> listaRegistrosEmergencia = oc.listarRegistros();
		ArrayList<LlamadaDeEmergencia> listaLlamadas = new ArrayList<LlamadaDeEmergencia>();
		RegistroEmergencia re;
		
		for (int i = 0; i<listaRegistrosEmergencia.size(); i++){
			re = listaRegistrosEmergencia.get(i);
			String companyia;
			String sintomas = "";
			ArrayList<Sintoma> sint = re.getSintomas();
			int z; /*
			for(z = 0; z<sint.size(); z++) {
				sintomas+= sint.get(z).getDescripcion()+"\n" ;
			}
			System.out.println(sintomas);*/
			table.getColumn("sintomas").setPreferredWidth(200);
			table.getColumn("apellidos").setPreferredWidth(100);
			table.getColumn("equipo").setPreferredWidth(150);
			
			if (oc.esPrivada(re.getAmbulancia())) companyia = oc.companyiaDeAmb(re.getAmbulancia());
			else companyia = "P�blica";
			int j;
			for(j=0; j<re.getSintomas().size()-1; j++) sintomas += re.getSintomas().get(j).getDescripcion()+", ";
			if(re.getSintomas().size() != 0) sintomas += re.getSintomas().get(j).getDescripcion();
			LlamadaDeEmergencia le = new LlamadaDeEmergencia(re.getIDregistro(), re.getLatitud(),
					re.getLongitud(), re.getFecha(), re.getHora(), re.getPaciente().getDni(),
					re.getPaciente().getNombre(), re.getPaciente().getApellidos(),
					re.getPaciente().getTelefono(), re.getAmbulancia().getNumRegistro(),
					re.getAmbulancia().getEquipo(), companyia, re.getHospital().getNombre(), sintomas);
			listaLlamadas.add(le);
		}
		
		EmergenciaTableModel model=(EmergenciaTableModel) table.getModel();
		
		model.clear();
		
		for (int i = 0; i<listaLlamadas.size();i++){
			model.addRow(listaLlamadas.get(i));
		}
		
		
		}catch (Exception e){
		JOptionPane.showMessageDialog(this,e.getMessage(),"ERROR",
		JOptionPane.ERROR_MESSAGE);
		}
		}

}
