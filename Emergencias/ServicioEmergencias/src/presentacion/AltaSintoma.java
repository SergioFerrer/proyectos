package presentacion;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import logica.Duo;
import logica.Sintoma;
import presentacion.AltaEmergenciaJDialog.DuoTableModel;

public class AltaSintoma extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private JTextField textField_1;
	private String espe;
	private Sintoma s;
	public AltaEmergenciaJDialog aux2;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			AltaSintoma dialog = new AltaSintoma(null, "");
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public AltaSintoma(AltaEmergenciaJDialog alta, String especialidad) {
		aux2 = alta;
		espe = especialidad;
		setTitle("ALTA S�NTOMA");
		setBounds(100, 100, 332, 158);
		setResizable(false);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		setLocationRelativeTo(null);

		JLabel lblNewLabel = new JLabel("Descripci\u00F3n:");
		lblNewLabel.setBounds(10, 11, 78, 14);
		contentPanel.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Duraci\u00F3n:");
		lblNewLabel_1.setBounds(10, 36, 78, 14);
		contentPanel.add(lblNewLabel_1);

		JLabel lblNewLabel_3 = new JLabel("Estado:");
		lblNewLabel_3.setBounds(10, 61, 78, 14);
		contentPanel.add(lblNewLabel_3);

		textField = new JTextField();
		textField.setBounds(97, 61, 161, 20);
		contentPanel.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(97, 36, 86, 20);
		contentPanel.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(97, 11, 161, 20);
		contentPanel.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblhoras = new JLabel("(horas)");
		lblhoras.setBounds(193, 39, 46, 14);
		contentPanel.add(lblhoras);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("A\u00F1adir s\u00EDntoma");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {

							String estado = textField.getText();
							int duracion = Integer.parseInt(textField_1
									.getText());
							String descripcion = textField_2.getText();
							if (estado.equals("") || descripcion.equals("")) {
								JOptionPane.showMessageDialog(null,
										"Inserta bien los campos", "Error",
										JOptionPane.ERROR_MESSAGE);

							} else {
								s = new Sintoma(estado, duracion, descripcion,
										espe, 0);
								Duo d = new Duo(espe, s.getEstado());
								aux2.de = d;
								aux2.sintomas.add(s);
								aux2.model = (DuoTableModel) aux2.table
										.getModel();

								aux2.model.addRow(aux2.de);
								dispose();
							}
						} catch (Exception excep) {
							JOptionPane.showMessageDialog(null,
									"Inserta bien los campos", "Error",
									JOptionPane.ERROR_MESSAGE);

						}

					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancelar");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	public Sintoma getSintoma() {

		return s;
	}
}
