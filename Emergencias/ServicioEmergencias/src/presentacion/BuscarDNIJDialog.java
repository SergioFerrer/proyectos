package presentacion;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import logica.OC;
import logica.Paciente;

public class BuscarDNIJDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			BuscarDNIJDialog dialog = new BuscarDNIJDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public BuscarDNIJDialog() {
		setTitle("B�squeda DNI");
		setBounds(100, 100, 450, 93);
		getContentPane().setLayout(new BorderLayout());
		setLocationRelativeTo(null);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("DNI:");
		lblNewLabel.setBounds(20, 20, 55, 16);
		contentPanel.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(64, 17, 200, 22);
		contentPanel.add(textField);
		textField.setColumns(10);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					String dni = textField.getText();
					Paciente p = OC.dameControlador().buscarPaciente(dni);
					if(p==null) {
						dispose();
						AltaPacienteJDialog altaPaciente = new AltaPacienteJDialog();
						altaPaciente.sendDNI(dni);
						altaPaciente.setLlamado();
						altaPaciente.setModal(true);
						altaPaciente.setVisible(true);
					}
					p = OC.dameControlador().buscarPaciente(dni);
					//else {
						dispose();
						AltaEmergenciaJDialog altaEmerg = new AltaEmergenciaJDialog(p);
						altaEmerg.setModal(true);
						altaEmerg.setVisible(true);
					//}
				}catch(Exception e1){
					JOptionPane.showMessageDialog(null, "Inserta bien los campos",
							"Error",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnBuscar.setBounds(299, 14, 90, 28);
		contentPanel.add(btnBuscar);
	}
}
