package presentacion;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import logica.Ambulancia;
import logica.OC;

public class CambiarCoordenadasJDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private JTable table;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JButton okButton;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			CambiarCoordenadasJDialog dialog = new CambiarCoordenadasJDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public CambiarCoordenadasJDialog() {
		setTitle("POSICIONAMIENTO");
		setBounds(100, 100, 500, 350);
		setResizable(false);
		getContentPane().setLayout(new BorderLayout());
		setLocationRelativeTo(null);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblIntrocudeElNmero = new JLabel("Introducir n\u00FAm. registro:");
		lblIntrocudeElNmero.setBounds(10, 11, 164, 23);
		contentPanel.add(lblIntrocudeElNmero);
		{
			textField = new JTextField();
			textField.setBounds(155, 11, 192, 23);
			contentPanel.add(textField);
			textField.setColumns(10);
			
		}
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					String numreg = textField.getText();
					Ambulancia ambulancia = OC.dameControlador().buscarAmbulancia(numreg);
					textField_1.setText(ambulancia.getNumRegistro());
					textField_2.setText(ambulancia.getEquipo());
					textField_3.setText("" + ambulancia.getLatitud());
					textField_4.setText("" + ambulancia.getLongitud());
					okButton.setEnabled(true);
					
				}catch(Exception e1){
					JOptionPane.showMessageDialog(null, "El n�mero de registro no existe, insterte otro.",
							"Error",JOptionPane.ERROR_MESSAGE);
					}
				}
			});
		btnBuscar.setBounds(391, 11, 91, 23);
		contentPanel.add(btnBuscar);
		
		JLabel lblNewLabel_1 = new JLabel("Equipo:");
		lblNewLabel_1.setBounds(10, 108, 107, 23);
		contentPanel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Latitud:");
		lblNewLabel_2.setBounds(10, 142, 107, 23);
		contentPanel.add(lblNewLabel_2);
		
		JLabel lblNewLabel = new JLabel("N\u00FAmero registro:");
		lblNewLabel.setBounds(10, 74, 107, 23);
		contentPanel.add(lblNewLabel);
		
		JLabel lblNewLabel_3 = new JLabel("Longitud:");
		lblNewLabel_3.setBounds(10, 176, 107, 23);
		contentPanel.add(lblNewLabel_3);
		
		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setBounds(155, 74, 192, 23);
		contentPanel.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setEditable(false);
		textField_2.setBounds(155, 108, 327, 23);
		contentPanel.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setEditable(false);
		textField_3.setBounds(155, 142, 91, 23);
		contentPanel.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setEditable(false);
		textField_4.setBounds(155, 176, 91, 23);
		contentPanel.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Nueva Latitud:");
		lblNewLabel_4.setBounds(10, 210, 107, 23);
		contentPanel.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Nueva Longitud:");
		lblNewLabel_5.setBounds(10, 244, 107, 23);
		contentPanel.add(lblNewLabel_5);
		
		textField_5 = new JTextField();
		textField_5.setBounds(155, 210, 132, 23);
		contentPanel.add(textField_5);
		textField_5.setColumns(10);
		
		textField_6 = new JTextField();
		textField_6.setBounds(155, 244, 132, 23);
		contentPanel.add(textField_6);
		textField_6.setColumns(10);
		
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("Cambiar coordenadas");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						try{
							String numreg = textField_1.getText();
							double latitud = Double.parseDouble(textField_5.getText());
							double longitud = Double.parseDouble(textField_6.getText());
							OC.dameControlador().cambiarCoordenadas(numreg,latitud,longitud);
							textField_3.setText("" + latitud);
							textField_4.setText("" + longitud);
							textField_5.setText("");
							textField_6.setText("");
							
						}catch(Exception e1){
							JOptionPane.showMessageDialog(null, "Inserta bien los campos",
									"Error",JOptionPane.ERROR_MESSAGE);
						}
					}
				});
				okButton.setEnabled(false);
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancelar");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
