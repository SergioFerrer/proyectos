package presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class EmergenciasApp {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EmergenciasApp window = new EmergenciasApp();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public EmergenciasApp() {
		initialize();
		frame.setLocationRelativeTo(null);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		
		JMenu mnOperadoremergencia = new JMenu("OperadorEmergencia");
		menuBar.add(mnOperadoremergencia);
		
		JMenuItem mntmAltaPaciente = new JMenuItem("Alta paciente");
		mntmAltaPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AltaPacienteJDialog altaPaciente = new AltaPacienteJDialog();
				altaPaciente.setModal(true);
				altaPaciente.setVisible(true);
			}
		});
		mnOperadoremergencia.add(mntmAltaPaciente);
		
		JMenuItem mntmNuevaEmergencia = new JMenuItem("Nueva emergencia");
		mntmNuevaEmergencia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BuscarDNIJDialog buscarDNI = new BuscarDNIJDialog();
				buscarDNI.setModal(true);
				buscarDNI.setVisible(true);
			}
		});
		mnOperadoremergencia.add(mntmNuevaEmergencia);
		
		JMenu mnConductorambulancia = new JMenu("ConductorAmbulancia");
		menuBar.add(mnConductorambulancia);
		
		JMenuItem mntmCambiarDisponibilidad = new JMenuItem("Cambiar disponibilidad");
		mntmCambiarDisponibilidad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CambiarDiponibilidadJDialog cambiardisponibilidad = new CambiarDiponibilidadJDialog();
				cambiardisponibilidad.setModal(true);
				cambiardisponibilidad.setVisible(true);
			}
		});
		mnConductorambulancia.add(mntmCambiarDisponibilidad);
		
		JMenuItem mntmCambiarCoordenadas = new JMenuItem("Cambiar coordenadas");
		mntmCambiarCoordenadas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CambiarCoordenadasJDialog cambiarcoordenadas = new CambiarCoordenadasJDialog();
				cambiarcoordenadas.setModal(true);
				cambiarcoordenadas.setVisible(true);
			}
		});
		mnConductorambulancia.add(mntmCambiarCoordenadas);
		
		JMenu mnPersonalcomision = new JMenu("PersonalComision");
		menuBar.add(mnPersonalcomision);
		
		JMenuItem mntmListarEspecialidades = new JMenuItem("Listar especialidades");
		mntmListarEspecialidades.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ListarEspecialidadesJDialog listarespecialidades = new ListarEspecialidadesJDialog();
				listarespecialidades.setModal(true);
				listarespecialidades.setVisible(true);
			}
		});
		mnPersonalcomision.add(mntmListarEspecialidades);
		
		JMenuItem mntmListarPacientes = new JMenuItem("Listar pacientes");
		mntmListarPacientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ListarPacientesJDialog listarpacientes = new ListarPacientesJDialog();
				listarpacientes.setModal(true);
				listarpacientes.setVisible(true);
			}
		});
		mnPersonalcomision.add(mntmListarPacientes);
		
		JMenuItem mntmListarLlamadas = new JMenuItem("Listar llamadas");
		mntmListarLlamadas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ListarLlamadasJDialog listarllamadas = new ListarLlamadasJDialog();
				listarllamadas.setModal(true);
				listarllamadas.setVisible(true);
			
			}
		});
		mnPersonalcomision.add(mntmListarLlamadas);
	}

}
