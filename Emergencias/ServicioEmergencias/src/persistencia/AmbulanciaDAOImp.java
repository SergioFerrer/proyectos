
	//****************************************************************
	//****    PATR�N SINGLETON
	//****************************************************************
//	private static PacienteDAOImp paciente; 
//	
//	public static PacienteDAOImp dameDAO() throws DAOExcepcion{
//		if (paciente==null)		paciente = new PacienteDAOImp();
//		return paciente;
//	}
	
//PacienteDAOImp
package persistencia;
import logica.*;
import java.sql.*;
import java.util.ArrayList;


import excepciones.DAOExcepcion;

public class AmbulanciaDAOImp implements IAmbulanciaDAO {
	protected ConnectionManager connManager;

	public AmbulanciaDAOImp() throws DAOExcepcion {
		super();
		try{
			connManager= new ConnectionManager("dbModel2");
		}
		catch (ClassNotFoundException e){	throw new DAOExcepcion(e);}
	}
	
	public void crearAmbulancia(Ambulancia pa) throws DAOExcepcion {
	// TODO Auto-generated method stub			
		try{
			connManager.connect();
			connManager.updateDB("insert into AMBULANCIA (NUMREGISTRO, EQUIPO, LATITUD, LONGITUD) values ('"+pa.getNumRegistro()+"','"+pa.getEquipo()+"','"+pa.getLatitud()+"', '"+pa.getLongitud());
			connManager.close();
		}
		catch (Exception e){	throw new DAOExcepcion(e);}
	}
	  
	public ArrayList <Ambulancia> listarAmbulancias() throws DAOExcepcion{
		try{
			connManager.connect();
			ResultSet rs=connManager.queryDB("select * from AMBULANCIA");						
			connManager.close();
	  	  
			ArrayList<Ambulancia> listaAmbulancias=new ArrayList<Ambulancia>();
				
			try{				
				while (rs.next()){
					Ambulancia pa = buscarAmbulancia(rs.getString("NUMREGISTRO"));	 
					listaAmbulancias.add(pa);
				}
				return listaAmbulancias;
				}
			catch (Exception e){	throw new DAOExcepcion(e);}
			}
		catch (DAOExcepcion e){		throw e;}	
	 }
	  
	public Ambulancia buscarAmbulancia(String numR)throws DAOExcepcion{
		try{
			connManager.connect();
			ResultSet rs=connManager.queryDB("select * from AMBULANCIA where NUMREGISTRO= '"+numR+"'");
			connManager.close();
		
			if (rs.next()){
				Ambulancia aux = new Ambulancia(rs.getString("NUMREGISTRO"), rs.getString("EQUIPO"),
												rs.getDouble("LATITUD"), rs.getDouble("LONGITUD"), null);
				return aux;
			}else
				return null;	
		}
		catch (SQLException e){	throw new DAOExcepcion(e);}	
	}
	
	public boolean esPrivada(String numR){
		Privada aux = null;
		try{
		aux = DAL.getInstance().pri.buscarPrivada(numR);
		}catch(DAOExcepcion e){}
		if(aux == null){
			return false;
		}else return true;
	}
	
	public String esPublica(String numR){
		BHospital aux = null;
		try{
			aux = DAL.getInstance().bhos.buscarBHospital(numR);
		}catch(DAOExcepcion e){}
			return aux.getHospital().getNombre();
	}
}
