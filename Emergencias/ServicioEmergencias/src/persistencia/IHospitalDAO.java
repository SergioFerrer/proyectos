package persistencia;

import java.util.List;

import logica.*;
import excepciones.DAOExcepcion;

public interface IHospitalDAO {
	public Hospital buscarHospital(String nombre)throws DAOExcepcion;
	public void crearHospital (Hospital h)throws DAOExcepcion;
	public List <Hospital> listarAmbulancias() throws DAOExcepcion;
}
