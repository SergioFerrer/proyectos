package persistencia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import logica.*;
import excepciones.DAOExcepcion;

public class SintomaDAOImp {
	protected ConnectionManager connManager;

	public SintomaDAOImp() throws DAOExcepcion {
		super();
		try{
			connManager= new ConnectionManager("dbModel2");
		}
		catch (ClassNotFoundException e){	throw new DAOExcepcion(e);}
	}
	
	public void crearSintoma(Sintoma s) throws DAOExcepcion {
	// TODO Auto-generated method stub			
		try{
			connManager.connect();
			connManager.updateDB("insert into SINTOMA (ESTADO, DURACION, DESCRIPCION, NOMBRE, IDREGISTRO) values ('"+s.getEstado()+"','"+s.getDuracion()+"','"+s.getDescripcion()+"', '"+s.getEspecialidad()+"', '"+s.getId()+"');");
			connManager.close();
		}
		catch (Exception e){	throw new DAOExcepcion(e);}
	}
	  
	public ArrayList <Sintoma> listarSintomas() throws DAOExcepcion{
		try{
			connManager.connect();
			ResultSet rs=connManager.queryDB("select * from SINTOMA");						
			connManager.close();
	  	  
			ArrayList<Sintoma> listaSintomas=new ArrayList<Sintoma>();
				
			try{				
				while (rs.next()){
					Sintoma s = new Sintoma(rs.getString("ESTADO"), rs.getInt("DURACION"), rs.getString("DESCRIPCION"), rs.getString("NOMBRE"), rs.getInt("IDREGISTRO"));
					listaSintomas.add(s);
				}
				return listaSintomas;
				}
			catch (Exception e){	throw new DAOExcepcion(e);}
			}
		catch (DAOExcepcion e){		throw e;}	
	 }
	  
	public Sintoma buscarSintoma(String esp,int id)throws DAOExcepcion{
		try{
			connManager.connect();
			ResultSet rs=connManager.queryDB("select * from SINTOMA where IDREGISTRO= '"+id+"' and ESPECIALIDAD = '"+esp+"'");
			connManager.close();
		
			if (rs.next()){
				Sintoma aux = new Sintoma(rs.getString("ESTADO"), rs.getInt("DURACION"), rs.getString("DESCRIPCION"), rs.getString("ESPECIALIDAD"), rs.getInt("IDREGISTRO"));

				return aux;
			}
			else
				return null;	
		}
		catch (SQLException e){	throw new DAOExcepcion(e);}	
	}
}
