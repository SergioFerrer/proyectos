package persistencia;
import excepciones.*;
public class DAL {
   public static DAL yo =null;
   public AmbulanciaDAOImp amb;
   public EspecialidadDAOImp esp;
   public HospitalDAOImp hos;
   public PacienteDAOImp pac;
   public SintomaDAOImp sin;
   public RegistroDAOImp reg;
   public BHospitalDAOImp bhos;
   public PrivadaDAOImp pri;
   private  DAL(){
    	try{
    	amb = new AmbulanciaDAOImp();
    	esp = new EspecialidadDAOImp();
    	hos = new HospitalDAOImp();
    	pac = new PacienteDAOImp();
    	sin = new SintomaDAOImp();
    	reg = new RegistroDAOImp();
    	bhos = new BHospitalDAOImp();
    	pri = new PrivadaDAOImp();
    	}catch(DAOExcepcion e){}
	}
    
    public static DAL getInstance(){
    	if(yo==null) yo = new DAL();
    	return yo;
    	
    }
}
