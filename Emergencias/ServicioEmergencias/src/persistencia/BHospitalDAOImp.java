package persistencia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import logica.*;
import excepciones.DAOExcepcion;

public class BHospitalDAOImp {
	protected ConnectionManager connManager;

	public BHospitalDAOImp() throws DAOExcepcion {
		super();
		try{
			connManager= new ConnectionManager("dbModel2");
		}
		catch (ClassNotFoundException e){	throw new DAOExcepcion(e);}
	}
	
	public void crearBHospital(BHospital p) throws DAOExcepcion {
	// TODO Auto-generated method stub			
		try{
			connManager.connect();
			connManager.updateDB("insert into BHOSPITAL (NUMREGISTRO,NOMBRE) values ('"+p.getNumRegistro()+"','"+p.getHospital().getNombre()+"');");
			connManager.close();
		}
		catch (Exception e){	throw new DAOExcepcion(e);}
	}
	  
	public ArrayList <BHospital> listarBHospitales() throws DAOExcepcion{
		try{
			connManager.connect();
			ResultSet rs=connManager.queryDB("select * from BHOSPITAL");						
			connManager.close();
	  	  
			ArrayList<BHospital> listaBHospitales=new ArrayList<BHospital>();
				
			try{				
				while (rs.next()){
					BHospital p = buscarBHospital(rs.getString("NUMREGISTRO")); 
					listaBHospitales.add(p);
				}
				return listaBHospitales;
				}
			catch (Exception e){	throw new DAOExcepcion(e);}
			}
		catch (DAOExcepcion e){		throw e;}	
	 }
	  
	public BHospital buscarBHospital(String numreg)throws DAOExcepcion{
		try{
			connManager.connect();
			ResultSet rs=connManager.queryDB("select * from BHOSPITAL where NUMREGISTRO= '"+numreg+"'");
			connManager.close();
		
			if (rs.next()){
				Ambulancia amb = DAL.getInstance().amb.buscarAmbulancia(rs.getString("NUMREGISTRO"));
				Hospital h = DAL.getInstance().hos.buscarHospital(rs.getString("NOMBRE"));
				BHospital aux = new BHospital(numreg, amb.getEquipo(),amb.getLatitud(), amb.getLongitud(), amb.getRegistros(), h);
				return aux;
			}
			else
				return null;	
		}catch (SQLException e){	throw new DAOExcepcion(e);}	
	}
}