package persistencia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import logica.*;
import excepciones.DAOExcepcion;

public class RegistroDAOImp {
	protected ConnectionManager connManager;
	public RegistroDAOImp() throws DAOExcepcion {
		super();
		try{
			connManager= new ConnectionManager("dbModel2");
		}
		catch (ClassNotFoundException e){	throw new DAOExcepcion(e);}
	}
	
	public void crearRegistro(RegistroEmergencia r) throws DAOExcepcion {
	// TODO Auto-generated method stub			
		try{
			SintomaDAOImp sin = DAL.getInstance().sin;
			connManager.connect();
			connManager.updateDB("insert into REGISTROEMERGENCIA (IDREGISTRO, LATITUD, LONGITUD, FECHA, HORA,IDAMBULANCIA,IDHOSPITAL,DNI) values ('"+r.getIDregistro()+"','"+r.getLatitud()+"','"+r.getLongitud()+"','"+r.getFecha()+"', '"+r.getHora()+"', '"+r.getAmbulancia().getNumRegistro()+"', '"+r.getHospital().getNombre()+"', '"+r.getPaciente().getDni()+"');");
			ArrayList<Sintoma> sintomas = r.getSintomas();
			for(int i = 0; i<sintomas.size();i++){
				sin.crearSintoma(sintomas.get(i));
			}
			connManager.close();
		}
		catch (Exception e){	throw new DAOExcepcion(e);}
	}

	public ArrayList <RegistroEmergencia> listarRegistros() throws DAOExcepcion{
		try{
			connManager.connect();
			ResultSet rs=connManager.queryDB("select * from REGISTROEMERGENCIA");						
			connManager.close();
	  	  
			ArrayList<RegistroEmergencia> listaRegistros=new ArrayList<RegistroEmergencia>();
				
			try{				
				while (rs.next()){
					RegistroEmergencia r = buscarRegistro(rs.getInt("IDREGISTRO"));
					listaRegistros.add(r);
				}
				return listaRegistros;
				}
			catch (Exception e){	throw new DAOExcepcion(e);}
			}
		catch (DAOExcepcion e){		throw e;}	
	 }
	  
	public RegistroEmergencia buscarRegistro(int id)throws DAOExcepcion{
		try{
			connManager.connect();
			ResultSet rs=connManager.queryDB("select * from REGISTROEMERGENCIA where IDREGISTRO= '"+id+"'");
			connManager.close();
			DAL dal = DAL.getInstance();
			SintomaDAOImp sin= dal.sin;
			if (rs.next()){
				//le linkamos el paciente y lo creamos
				Paciente pac = DAL.getInstance().pac.buscarPaciente(rs.getString("DNI"));
				RegistroEmergencia aux = new RegistroEmergencia(rs.getDouble("LATITUD"), rs.getDouble("LONGITUD"), rs.getString("FECHA"), rs.getString("HORA"),null, pac);
				//limínkamos los sintomas
			ArrayList<Sintoma> sintomas = sin.listarSintomas();
			for(int i = 0; i<sintomas.size();i++){
				if(sintomas.get(i).getId()==aux.getIDregistro()){
					aux.addSintoma(sintomas.get(i));
				}
			}
			//hosp y amb
			aux.setAmbulancia(DAL.getInstance().amb.buscarAmbulancia(rs.getString("IDAMBULANCIA")));
			aux.setHospital(DAL.getInstance().hos.buscarHospital(rs.getString("IDHOSPITAL")));
			return aux;
	
			}else
				return null;	
		}
		catch (SQLException e){	throw new DAOExcepcion(e);}	
	}
}
