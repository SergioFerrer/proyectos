package persistencia;

import java.util.List;

import logica.Especialidad;
import excepciones.DAOExcepcion;

public interface IEspecialidadDAO {
	public Especialidad buscarEspecialidad(String especialidad)throws DAOExcepcion;
	public void crearEspecialidad (Especialidad p)throws DAOExcepcion;
	public List <Especialidad> listarEspecialidades() throws DAOExcepcion;
}
