package persistencia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logica.Especialidad;
import excepciones.DAOExcepcion;

public class EspecialidadDAOImp implements IEspecialidadDAO {
	protected ConnectionManager connManager;
	
	public EspecialidadDAOImp() throws DAOExcepcion {
		super();
		try{
			connManager= new ConnectionManager("dbModel2");
		}
		catch (ClassNotFoundException e){	throw new DAOExcepcion(e);}
	}
	
	public void crearEspecialidad(Especialidad p) throws DAOExcepcion {
		// TODO Auto-generated method stub			
			try{
				connManager.connect();
				connManager.updateDB("insert into ESPECIALIDAD (NOMBRE) values ('"+p.getNombre()+"')");
				connManager.close();
			}
			catch (Exception e){	throw new DAOExcepcion(e);}
		}
		  
		public ArrayList <Especialidad> listarEspecialidades() throws DAOExcepcion{
			try{
				connManager.connect();
				ResultSet rs=connManager.queryDB("select * from ESPECIALIDAD");						
				connManager.close();
		  	  
				ArrayList<Especialidad> listaEspecialidades=new ArrayList<Especialidad>();
					
				try{				
					while (rs.next()){
						Especialidad pa = buscarEspecialidad(rs.getString("NOMBRE"));	 
						listaEspecialidades.add(pa);
					}
					return listaEspecialidades;
					}
				catch (Exception e){	throw new DAOExcepcion(e);}
				}
			catch (DAOExcepcion e){		throw e;}	
		 }
		  
		public Especialidad buscarEspecialidad(String nombre)throws DAOExcepcion{
			try{
				connManager.connect();
				ResultSet rs=connManager.queryDB("select * from ESPECIALIDAD where NOMBRE= '"+nombre+"'");
				connManager.close();
			
				if (rs.next()){
					Especialidad aux = new Especialidad(rs.getString("NOMBRE"));
			

					return aux;
				}
				else
					return null;	
			}
			catch (SQLException e){	throw new DAOExcepcion(e);}	
		}
}
