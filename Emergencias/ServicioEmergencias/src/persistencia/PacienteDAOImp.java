	//****************************************************************
	//****    PATR�N SINGLETON
	//****************************************************************
//	private static PacienteDAOImp paciente; 
//	
//	public static PacienteDAOImp dameDAO() throws DAOExcepcion{
//		if (paciente==null)		paciente = new PacienteDAOImp();
//		return paciente;
//	}
	
//PacienteDAOImp
package persistencia;
import java.sql.*;
import java.util.ArrayList;

import logica.Paciente;
import excepciones.DAOExcepcion;

public class PacienteDAOImp implements IPacienteDAO {
	protected ConnectionManager connManager;

	public PacienteDAOImp() throws DAOExcepcion {
		super();
		try{
			connManager= new ConnectionManager("dbModel2");
		}
		catch (ClassNotFoundException e){	throw new DAOExcepcion(e);}
	}
	
	public void crearPaciente(Paciente pa) throws DAOExcepcion {
	// TODO Auto-generated method stub			
		try{
			connManager.connect();
			System.out.println("insert into PACIENTE (DNI, NOMBRE, APELLIDOS, DIRECCION, TELEFONO, EDAD, SEXO) values ('"+pa.getDni()+"','"+pa.getNombre()+"','"+pa.getApellidos()+"', '"+pa.getDireccion()+"', '"+pa.getTelefono()+"', '"+pa.getEdad()+"','"+pa.getSexo()+"');");
			connManager.updateDB("insert into PACIENTE (DNI, NOMBRE, APELLIDOS, DIRECCION, TELEFONO, EDAD, SEXO) values ('"+pa.getDni()+"','"+pa.getNombre()+"','"+pa.getApellidos()+"', '"+pa.getDireccion()+"', '"+pa.getTelefono()+"', '"+pa.getEdad()+"','"+pa.getSexo()+"');");
			connManager.close();
		}
		catch (Exception e){	throw new DAOExcepcion(e);}
	}
	  
	public ArrayList <Paciente> listarPacientes() throws DAOExcepcion{
		try{
			connManager.connect();
			ResultSet rs=connManager.queryDB("select * from PACIENTE");						
			connManager.close();
	  	  
			ArrayList<Paciente> listaPacientes=new ArrayList<Paciente>();
				
			try{				
				while (rs.next()){
					Paciente pa = buscarPaciente(rs.getString("DNI"));	 
					listaPacientes.add(pa);
				}
				return listaPacientes;
				}
			catch (Exception e){	throw new DAOExcepcion(e);}
			}
		catch (DAOExcepcion e){		throw e;}	
	 }
	  
	public Paciente buscarPaciente(String dni)throws DAOExcepcion{
		try{
			connManager.connect();
			ResultSet rs=connManager.queryDB("select * from PACIENTE where DNI= '"+dni+"'");
			connManager.close();
		
			if (rs.next()){
				Paciente aux = new Paciente(rs.getString("DNI"), rs.getString("NOMBRE"),rs.getString("APELLIDOS"), 
											rs.getString("DIRECCION"), rs.getInt("EDAD"), 
											rs.getInt("TELEFONO"), rs.getString("SEXO").charAt(0));

				return aux;
			}
			else
				return null;	
		}
		catch (SQLException e){	throw new DAOExcepcion(e);}	
	}
}
