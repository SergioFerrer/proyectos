package persistencia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;




import logica.*;
import excepciones.DAOExcepcion;

public class HospitalDAOImp {
	protected ConnectionManager connManager;

	public HospitalDAOImp() throws DAOExcepcion {
		super();
		try{
			connManager= new ConnectionManager("dbModel2");
		}
		catch (ClassNotFoundException e){	throw new DAOExcepcion(e);}
	}
	
	public Hospital buscarHospital(String nombre)throws DAOExcepcion{
		try{
			connManager.connect();
			ResultSet rs=connManager.queryDB("select * from HOSPITAL where NOMBRE= '"+nombre+"'");

		
			if (rs.next()){
				Hospital aux = new Hospital(rs.getString("NOMBRE"), rs.getString("DIRECCION"),rs.getDouble("LATITUD"), 
											rs.getDouble("LONGITUD"),new ArrayList<Especialidad>(),null,null);
			rs = connManager.queryDB("select IDESPECIALIDAD from ATIENDE where NOMBRE= '"+aux.getNombre()+"'");
			ResultSet rs2;
			while (rs.next()){
				String e = rs.getString("IDESPECIALIDAD");	 
				rs2 = connManager.queryDB("select NOMBRE FROM ESPECIALIDAD where NOMBRE = '"+e+"'");
				if (rs2.next()){
					Especialidad e2 = new Especialidad(rs2.getString("NOMBRE"));
					aux.anyadirEspecialidad(e2);
			
				}
				
			}
			connManager.close();
			return aux;
			}else{
				connManager.close();
				return null;	
			}
		}catch (SQLException e){	throw new DAOExcepcion(e);	
	}
}
	public void crearHospital (Hospital h)throws DAOExcepcion{
		try{
			connManager.connect();
			connManager.updateDB("insert into HOSPITAL (NOMBRE,DIRECCION,LATITUD,LONGITUD) values ('"+h.getNombre()+"','"+h.getDireccion()+"','"+h.getLatitud()+"','"+h.getLongitud()+"')");
			connManager.close();
		}
		catch (Exception e){	throw new DAOExcepcion(e);}	
		
	}
	public ArrayList <Hospital> listarHospitales() throws DAOExcepcion{
		try{
			connManager.connect();
			ResultSet rs=connManager.queryDB("select * from HOSPITAL");						
			connManager.close();
	  	  
			ArrayList<Hospital> listaHospitales=new ArrayList<Hospital>();
				
			try{				
				while (rs.next()){
					Hospital h = buscarHospital(rs.getString("NOMBRE"));	 
					listaHospitales.add(h);
				}
				return listaHospitales;
				}
			catch (Exception e){	throw new DAOExcepcion(e);}
			}
		catch (DAOExcepcion e){		throw e;}	
		
	}
}
