//IPacienteDAO
package persistencia;
import logica.*;
import java.util.List;

import excepciones.*;

public interface IAmbulanciaDAO {
	public Ambulancia buscarAmbulancia(String dni)throws DAOExcepcion;
	public void crearAmbulancia (Ambulancia p)throws DAOExcepcion;
	public List <Ambulancia> listarAmbulancias() throws DAOExcepcion;
}
