package persistencia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import logica.*;
import excepciones.DAOExcepcion;

public class PrivadaDAOImp {
	protected ConnectionManager connManager;

	public PrivadaDAOImp() throws DAOExcepcion {
		super();
		try{
			connManager= new ConnectionManager("dbModel2");
		}
		catch (ClassNotFoundException e){	throw new DAOExcepcion(e);}
	}
	
	public void crearPrivada(Privada p) throws DAOExcepcion {
	// TODO Auto-generated method stub			
		try{
			connManager.connect();
			connManager.updateDB("insert into PRIVADA (NUMREGISTRO,COMPANYIA) values ('"+p.getNumRegistro()+"','"+p.getCompanyia()+"');");
			connManager.close();
		}
		catch (Exception e){	throw new DAOExcepcion(e);}
	}
	  
	public ArrayList <Privada> listarPrivadas() throws DAOExcepcion{
		try{
			connManager.connect();
			ResultSet rs=connManager.queryDB("select * from PRIVADA");						
			connManager.close();
	  	  
			ArrayList<Privada> listaPrivadas=new ArrayList<Privada>();
				
			try{				
				while (rs.next()){
					Privada p = buscarPrivada(rs.getString("NUMREGISTRO")); 
					listaPrivadas.add(p);
				}
				return listaPrivadas;
				}
			catch (Exception e){	throw new DAOExcepcion(e);}
			}
		catch (DAOExcepcion e){		throw e;}	
	 }
	
	public String getCompanyia(String a)throws DAOExcepcion{
		try{
			connManager.connect();
			ResultSet rs=connManager.queryDB("select * from PRIVADA where NUMREGISTRO= '"+a+"'");
			connManager.close();
		
			if (rs.next()){
				return rs.getString("COMPANYIA");
				
			}
			else
				return null;	
		}catch (SQLException e){	throw new DAOExcepcion(e);}	
	}
	  
	public Privada buscarPrivada(String numreg)throws DAOExcepcion{
		try{
			connManager.connect();
			ResultSet rs=connManager.queryDB("select * from PRIVADA where NUMREGISTRO= '"+numreg+"'");
			connManager.close();
		
			if (rs.next()){
				Ambulancia amb = DAL.getInstance().amb.buscarAmbulancia(rs.getString("NUMREGISTRO"));
				Privada aux = new Privada(numreg, amb.getEquipo(),amb.getLatitud(), amb.getLongitud(), amb.getRegistros(), rs.getString("COMPANYIA"));
				return aux;
			}
			else
				return null;	
		}catch (SQLException e){	throw new DAOExcepcion(e);}	
	}
}