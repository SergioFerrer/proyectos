package logica;

public class Sintoma {
	private String estado;
	private int duracion;
	private String descripcion;
	private String e;
	private int id;
	
	public Sintoma(String est, int dur, String desc, String esp, int i){
		estado = est;
		duracion = dur;
		descripcion = desc;
		e = esp;
		id = i;
	}
	
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public int getDuracion() {
		return duracion;
	}
	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getEspecialidad() {
		return e;
	}
	public void setEspecialidad(String esp) {
		this.e = esp;
	}
	public int getId() {
		return id;
	}
	public void setID(int i) {
		id = i;
	}
}
