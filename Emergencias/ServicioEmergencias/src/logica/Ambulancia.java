package logica;
import java.util.ArrayList;


public class Ambulancia {
	private String numRegistro;
	private String equipo;
	private double latitud;
	private double longitud;
	private ArrayList<RegistroEmergencia> registros;
	private boolean disponible;
	
	public Ambulancia(String numReg, String eq, double lat, double lon, ArrayList<RegistroEmergencia> alre){
		numRegistro = numReg;
		equipo = eq;
		latitud = lat;
		longitud = lon;
		registros = alre;
		disponible = true;
	}
	
	public String getNumRegistro() {
		return numRegistro;
	}
	public boolean getDisponible() {
		return disponible;
	}
	
	public void setDisponible(boolean disp) {
		this.disponible = disp;
	}
	
	public void setNumRegistro(String numRegistro) {
		this.numRegistro = numRegistro;
	}
	public String getEquipo() {
		return equipo;
	}
	public void setEquipo(String equipo) {
		this.equipo = equipo;
	}
	public double getLatitud() {
		return latitud;
	}
	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}
	public double getLongitud() {
		return longitud;
	}
	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}
	public ArrayList<RegistroEmergencia> getRegistros() {
		return registros;
	}
	public void setRegistros(ArrayList<RegistroEmergencia> registros) {
		this.registros = registros;
	}
	
//Regsitros
	public RegistroEmergencia obtenerRegistro(int ID){
		RegistroEmergencia aux;
		for(int i = 0; i<registros.size(); i++){
			aux = registros.get(i);
			if(aux.getIDregistro() == ID){
				return aux;
			}
		}
		return null;
	}
	public RegistroEmergencia borrarRegistro(int ID){
		RegistroEmergencia aux;
		for(int i = 0; i<registros.size(); i++){
			aux = registros.get(i);
			if(aux.getIDregistro() == ID){
				return registros.remove(i);
			}
		}
		return null;
	}
	
	public void cambiarDisponibilidad(){
		disponible = !disponible;
	}
	public void aņadirRegistro(RegistroEmergencia reg){
		registros.add(reg);
	}
}
