package logica;
import java.util.*;

public class Hospital {
	private String nombre, direccion;
	private double latitud, longitud;
	private ArrayList<Especialidad> especialidades;
	// HACER ESTO
	private ArrayList<RegistroEmergencia> registros;
	private ArrayList<BHospital> ambulancias;

	public Hospital(String nom, String dir, double lat, double lon, ArrayList<Especialidad> ale,
			ArrayList<RegistroEmergencia> alre,  ArrayList<BHospital> albh){
		nombre = nom;
		direccion = dir;
		latitud = lat;
		longitud = lon;
		especialidades = ale;
		registros = alre;
		ambulancias = albh;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public double getLatitud() {
		return latitud;
	}

	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}

	public double getLongitud() {
		return longitud;
	}

	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}

	public ArrayList<Especialidad> getEspecialidades() {
		return especialidades;
	}

	public void setEspecialidades(ArrayList<Especialidad> especialidades) {
		this.especialidades = especialidades;
	}

	public ArrayList<RegistroEmergencia> getRegistros() {
		return registros;
	}

	public void setRegistros(ArrayList<RegistroEmergencia> registros) {
		this.registros = registros;
	}

	public ArrayList<BHospital> getAmbulancias() {
		return ambulancias;
	}

	public void setAmbulancias(ArrayList<BHospital> ambulancias) {
		this.ambulancias = ambulancias;
	}

	// Especialidad
	public Especialidad obtenerEspecialidad(String nombre) {
		Especialidad aux;
		for (int i = 0; i < especialidades.size(); i++) {
			aux = especialidades.get(i);
			if (aux.getNombre().equals(nombre))
				return aux;
		}
		return null;
	}

	public Especialidad eliminarEspecialidad(String nombre) {
		Especialidad aux;
		for (int i = 0; i < especialidades.size(); i++) {
			aux = especialidades.get(i);
			if (aux.getNombre().equals(nombre))
				return especialidades.remove(i);
		}
		return null;
	}

	public void anyadirEspecialidad(Especialidad esp) {
		especialidades.add(esp);
	}

	// Registro Emergencia
	public RegistroEmergencia obtenerRegistroEmergencia(int id) {
		RegistroEmergencia aux;
		for (int i = 0; i < registros.size(); i++) {
			aux = registros.get(i);
			if (aux.getIDregistro() == id)
				return aux;
		}
		return null;
	}

	public void anyadirRegistroEmergencia(RegistroEmergencia e) {
		registros.add(e);
	}

	public RegistroEmergencia eliminarRegistroEmergencia(int id) {
		RegistroEmergencia aux;
		for (int i = 0; i < registros.size(); i++) {
			aux = registros.get(i);
			if (aux.getIDregistro() == id) {
				registros.remove(i);
				return aux;
			}
		}
		return null;
	}

	// Ambulances
	public void anyadirAmbulancia(BHospital a) {
		ambulancias.add(a);
	}

	public BHospital obtenerAmbulancia(String num) {
		BHospital aux;
		for (int i = 0; i < ambulancias.size(); i++) {
			aux = ambulancias.get(i);
			if (aux.getNumRegistro().equals(num)) {
				return aux;
			}
		}
		return null;
	}

	public BHospital eliminarAmbulancia(String num) {
		BHospital aux;
		for (int i = 0; i < ambulancias.size(); i++) {
			aux = ambulancias.get(i);
			if (aux.getNumRegistro().equals(num)) {
				ambulancias.remove(i);
				return aux;
			}
		}
		return null;
	}
}
