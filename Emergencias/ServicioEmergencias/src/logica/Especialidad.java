package logica;

public class Especialidad {
		private String nombre;
		
		public Especialidad(String nom){
			nombre = nom;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
}
