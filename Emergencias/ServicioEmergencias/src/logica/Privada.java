package logica;

import java.util.ArrayList;


public class Privada extends Ambulancia{
	private String companyia;
	
	public Privada(String numReg, String eq, double lat, double lon, ArrayList<RegistroEmergencia> alre, String comp){
		super(numReg, eq, lat, lon, alre);
		companyia = comp;
	}
	
	public String getCompanyia() {
		return companyia;
	}

	public void setCompanyia(String companyia) {
		this.companyia = companyia;
	}
}
