package logica;
import java.util.*;

import persistencia.*;
import excepciones.*;
public class ServicioEmergencia {
	private ArrayList<Ambulancia> ambulancias;
	private ArrayList<Hospital> hospitales;
	private ArrayList<RegistroEmergencia> registros;
	private ArrayList<Paciente> pacientes;
	private DAL dal;
	
	
	public ServicioEmergencia(){
		dal = DAL.getInstance();
		try{
			ambulancias = dal.amb.listarAmbulancias();
			hospitales = dal.hos.listarHospitales();
			pacientes = dal.pac.listarPacientes();
			registros = new ArrayList<RegistroEmergencia>();
		}catch(DAOExcepcion e){}
		
	}

	
	public ArrayList<Ambulancia> getAmbulancias() {
		return ambulancias;
	}
	public void setAmbulancias(ArrayList<Ambulancia> ambulancias) {
		this.ambulancias = ambulancias;
	}
	public ArrayList<Hospital> getHospitales() {
		return hospitales;
	}
	public void setHospitales(ArrayList<Hospital> hospitales) {
		this.hospitales = hospitales;
	}
	public ArrayList<RegistroEmergencia> getRegistros() {
		return registros;
	}
	public void setRegistros(ArrayList<RegistroEmergencia> registros) {
		this.registros = registros;
	}

//Ambulancias

public boolean esPrivada(Ambulancia a){
	return dal.amb.esPrivada(a.getNumRegistro());
}

public String hospitalDeAmb(Ambulancia a){
	return dal.amb.esPublica(a.getNumRegistro());
}

public String companyiaDeAmb(Ambulancia a){
	String c = "";
	try{ 
	c =dal.pri.getCompanyia(a.getNumRegistro());
	}catch(DAOExcepcion e){}
	return c;
}

public void cambiarDisponibilidad(String numreg){
	Ambulancia aux = obtenerAmbulancia(numreg);
	if(aux != null){
		aux.cambiarDisponibilidad();
	}
}

public void cambiarCoordenadas(String numreg,double lat, double lon){
	Ambulancia aux = obtenerAmbulancia(numreg);
	if(aux != null){
		aux.setLatitud(lat);
		aux.setLongitud(lon);
	}
}
public void anyadirAmbulancia (Ambulancia a){
	ambulancias.add(a);
	
	try{
	dal.amb.crearAmbulancia(a);
	}catch(DAOExcepcion e){}
}

public Ambulancia obtenerAmbulancia(String num){
	Ambulancia aux;
	for(int i = 0; i<ambulancias.size();i++){
		aux = ambulancias.get(i);
		if(aux.getNumRegistro().equals(num)){
			return aux;
		}
	}
	return null;
}

public Ambulancia eliminarAmbulancia(String num){
	Ambulancia aux;
	for(int i = 0; i<ambulancias.size();i++){
		aux = ambulancias.get(i);
		if(aux.getNumRegistro().equals(num)){
			ambulancias.remove(i);
			return aux;
		}
	}
	return null;
}

//hospitales

public ArrayList<Hospital> listarHospitales(){
	return hospitales;
}

public void anyadirHospital (Hospital a){
	hospitales.add(a);
}

public Hospital obtenerHospital(String nom){
	Hospital aux;
	for(int i = 0; i<hospitales.size();i++){
		aux = hospitales.get(i);
		if(aux.getNombre().equals(nom)){
			return aux;
		}
	}
	return null;
}

public Hospital eliminarHospital(String nom){
	Hospital aux;
	for(int i = 0; i<hospitales.size();i++){
		aux = hospitales.get(i);
		if(aux.getNombre().equals(nom)){
			hospitales.remove(i);
			return aux;
		}
	}
	return null;
}

public ArrayList<Especialidad> listarEspecialidades(String nom){
	Hospital aux = obtenerHospital(nom);
	return aux.getEspecialidades();
}
public ArrayList<Especialidad> listarEspecialidades(){
	ArrayList<Especialidad> list = null;
	try{
		list = dal.esp.listarEspecialidades();
	}catch(DAOExcepcion ex){}
	return list;
}

//Regsitros
public void anyadirRegistroEmergencia (RegistroEmergencia a){
	registros.add(a);
}


public RegistroEmergencia asignarAmbyHosp(RegistroEmergencia e){
	///Aqui calcularemos lo que toca y a�adiremos a "e" la Amb y Hosp
	
	//Asignar hospital
	ArrayList<Sintoma> ls =e.getSintomas();
	ArrayList<Especialidad> le = new ArrayList<Especialidad> ();
	for(int i = 0; i<ls.size();i++){
		le.add(new Especialidad(ls.get(i).getEspecialidad()));    
	}
	//Hospitales posibles
	ArrayList<Hospital> posibles = new ArrayList<Hospital>();
	for(int i = 0; i<hospitales.size();i++){
		ArrayList<Especialidad> espAct = hospitales.get(i).getEspecialidades();
		boolean existe = true;
		for(int j = 0; j<le.size()&&existe;j++){
			existe= esta(espAct,le.get(j));
			
		}
		if(existe)posibles.add(hospitales.get(i));
	}
	if(posibles.size()==0) return null;
	//Ambulancias disponibles
	ArrayList<Ambulancia> ambDisp = new ArrayList<Ambulancia>();
	for(int i = 0; i < ambulancias.size();i++){
		if(ambulancias.get(i).getDisponible())
			ambDisp.add(ambulancias.get(i));
	}
	if(ambDisp.size()==0) return null;
	ArrayList<Asignacion> pares = new ArrayList<Asignacion>();
	for(int i = 0; i< posibles.size();i++){
		for(int j = 0; j < ambDisp.size();j++){
			if(dal.amb.esPrivada(ambDisp.get(j).getNumRegistro())){
				pares.add(new Asignacion(posibles.get(i),ambDisp.get(j)));
			}else{
				if(dal.amb.esPublica(ambDisp.get(j).getNumRegistro()).equals(posibles.get(i).getNombre())){
					pares.add(new Asignacion(posibles.get(i),ambDisp.get(j)));
				}
			}
		}
	}
	if(pares.size()==0)return null;
	
	//Asignacion de verdad
	double max = Double.MAX_VALUE;
	for(int i = 0; i<pares.size();i++){
		if(pares.get(i).distancia(e)< max){
			max = pares.get(i).distancia(e);
			e.setAmbulancia(pares.get(i).amb);
			cambiarDisponibilidad(pares.get(i).amb.getNumRegistro());
			e.setHospital(pares.get(i).hos);
		}
	}
	
	
	//Cuando el registro de emergencia est� completo, entonces lo damos de alta en la base de datos.
	anyadirRegistroEmergencia(e);
	try{
		dal.reg.crearRegistro(e);
	}catch(DAOExcepcion es){}
	return e;
}

public ArrayList<RegistroEmergencia> listarRegistros(){
	ArrayList<RegistroEmergencia> lreg = null;
	try{
	lreg = dal.reg.listarRegistros();
	}catch(DAOExcepcion e){}
	return lreg;
}

public boolean esta(ArrayList<Especialidad> lista, Especialidad e){
	boolean encontrado = false;
	for(int i = 0; i<lista.size();i++){
		if(lista.get(i).getNombre().equals(e.getNombre())){
			encontrado = true;
			return encontrado;}
	}
	return encontrado;
}

public RegistroEmergencia obtenerRegistroEmergencia(int id){
	RegistroEmergencia aux;
	for(int i = 0; i<registros.size();i++){
		aux = registros.get(i);
		if(aux.getIDregistro()==id){
			return aux;
		}
	}
	return null;
}

public RegistroEmergencia eliminarRegistroEmergencia(int id){
	RegistroEmergencia aux;
	for(int i = 0; i<registros.size();i++){
		aux = registros.get(i);
		if(aux.getIDregistro()==id){
			registros.remove(i);
			return aux;
		}
	}
	return null;
}
//Pacientes

public ArrayList<Paciente> listarPacientes(){
	ArrayList<Paciente> s = null;
	try {
	 s = dal.pac.listarPacientes();
	}catch(DAOExcepcion e){}
	return s;
}

public boolean altaPaciente(Paciente p){
	try{
		for(int i = 0; i< pacientes.size();i++){
			if(p.getDni().equals(pacientes.get(i).getDni()))
				return false;
		}
			dal.pac.crearPaciente(p);
			pacientes.add(p);
			return true;
		}catch(DAOExcepcion e){}
	return true;
	
}

public Paciente buscarPaciente(String dni){
	for(int i = 0; i< pacientes.size();i++){
		if(dni.equals(pacientes.get(i).getDni()))
			return pacientes.get(i);
	}
	return null;
}
//sintomas




}

