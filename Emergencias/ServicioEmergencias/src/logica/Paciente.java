package logica;

public class Paciente {
		private String dni,nombre,apellidos,direccion;
		private int edad, telefono;
		private char sexo;
		
		public Paciente(String id, String nom, String ap, String dir, int ed, int tfn, char sex){
			dni = id;
			nombre = nom;
			apellidos = ap;
			direccion = dir;
			edad = ed;
			telefono = tfn;
			sexo = sex;
		}
		
		public String getDni() {
			return dni;
		}
		public void setDni(String dni) {
			this.dni = dni;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getApellidos() {
			return apellidos;
		}
		public void setApellidos(String apellidos) {
			this.apellidos = apellidos;
		}
		public String getDireccion() {
			return direccion;
		}
		public void setDireccion(String direccion) {
			this.direccion = direccion;
		}
		public int getEdad() {
			return edad;
		}
		public void setEdad(int edad) {
			this.edad = edad;
		}
		public int getTelefono() {
			return telefono;
		}
		public void setTelefono(int telefono) {
			this.telefono = telefono;
		}
		public char getSexo() {
			return sexo;
		}
		public void setSexo(char sexo) {
			this.sexo = sexo;
		}
}
