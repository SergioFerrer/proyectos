package logica;

public class Asignacion {
	
	Ambulancia amb;
	Hospital hos;
	
	public Asignacion(Hospital hos, Ambulancia amb){
		this.amb=amb;
		this.hos=hos;
	}
	
	public double distancia(RegistroEmergencia e){

			double distHospital = Math.sqrt(Math.pow(hos.getLatitud()-e.getLatitud(),2)+
					Math.pow(hos.getLongitud()-e.getLongitud(),2));
			double distAmb = Math.sqrt(Math.pow(amb.getLatitud()-e.getLatitud(),2)+
					Math.pow(amb.getLongitud()-e.getLongitud(),2));
			
			return distHospital + distAmb;
	}
}