package logica;

import java.util.*;
public class OC {
	ServicioEmergencia s;
	private static OC controlador = null;
	
	public OC(){
		s = new ServicioEmergencia();
	}
	public static OC dameControlador(){
		if(controlador==null) controlador = new OC();
		return controlador;
	}
	
	public Ambulancia buscarAmbulancia(String numreg){
		return s.obtenerAmbulancia(numreg);
	}
	
	public boolean altaPaciente(Paciente p){
		return s.altaPaciente(p);
	}
	
	public void cambiarDisponibilidad(String numreg){
		s.cambiarDisponibilidad(numreg);
	}
	
	public void cambiarCoordenadas(String numreg, double lat,double lon){
		s.cambiarCoordenadas(numreg,lat,lon);
	}
	
	public ArrayList<Hospital> listarHospitales(){
		return s.listarHospitales();
	}
	
	public ArrayList<Especialidad> listarEspecialidades(String nombre){
		return s.listarEspecialidades(nombre);
	}
	public ArrayList<Especialidad> listarEspecialidades(){
		return s.listarEspecialidades();
	}
	public ArrayList<Paciente> listarPacientes(){
		return s.listarPacientes();
	}
	
	
	
	public Paciente buscarPaciente(String dni){
		return s.buscarPaciente(dni);
	}
	
	public ArrayList<RegistroEmergencia> listarRegistros(){
		return s.listarRegistros();
	}
	
	public RegistroEmergencia asignarAmbyHosp(RegistroEmergencia e){
		return s.asignarAmbyHosp(e);
	}
	
	public boolean esPrivada(Ambulancia a){
		return s.esPrivada(a);
	}
	
	public String hospitalDeAmb(Ambulancia a){
		return s.hospitalDeAmb(a);
	}
	
	public String companyiaDeAmb(Ambulancia a){
		 return s.companyiaDeAmb(a);
	}
	
}
