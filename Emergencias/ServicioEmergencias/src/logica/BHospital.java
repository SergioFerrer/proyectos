package logica;

import java.util.ArrayList;


public class BHospital extends Ambulancia {
	private Hospital hospital;

	public BHospital(String numReg, String eq, double lat, double lon, ArrayList<RegistroEmergencia> alre, Hospital hosp){
		super(numReg, eq, lat, lon, alre);
		hospital = hosp;
	}
	
	public Hospital getHospital() {
		return hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}
}
