#!/usr/bin/octave -qf

if(nargin!=1)
	printf("Usage:multinomial.m <data_filename> <epsilon>");
	exit(1);
end
arglist = argv();
datafile=arglist{1};
disp("Loading data...");
load(datafile);
disp("Data load complete.");

disp("Barajando y dividiendo datos");
[nrows,ncols]=size(data);
rand("seed",23);
perm=randperm(nrows);
pdata=data(perm,:);

trper=0.9;
ntr=floor(nrows*trper);
nte=nrows-ntr;
tr=pdata(1:ntr,:);
te=pdata(ntr+1:nrows,:);

epsilon = 0.1
matrizErrores = zeros(20,2);
contadorMatrizErrores = 1;
while (epsilon > 1e-20)
disp(epsilon);
##################################
### Entrenamiento clasificador ###
##################################
#spam=1 ham =0

##Calculo de probabilidades a priori

#Para la clase spam
[filas,columnas]=size(tr);
vectorClases = tr(:,columnas);
f=find(vectorClases==1);
numeroSpam=size(f)(1);
disp(numeroSpam);
probabilidadSpam=numeroSpam/filas;

##para la clase ham
probabilidadHam=1-probabilidadSpam;

##Calculo de prototipos multinomiales
#para spam 
#quitamos la ultima columna
trSinEt = tr(:,[1:columnas-1]);

vectoresSpam= trSinEt(f,:);
sumaVectores = sum(vectoresSpam);
ps = sumaVectores/sum(sumaVectores);

#para ham 
indices = find(vectorClases ==0);
vectoresHam = trSinEt(indices,:);
sumaVectoresHam = sum(vectoresHam);
ph = sumaVectoresHam/sum(sumaVectoresHam);

##SUAVIZADO DE LAPLACE##
sumph =ph+epsilon;
ph = ph + epsilon;
ph = ph/sum(sumph);
sumps =ps+epsilon;
ps = ps + epsilon;
ps = ps/sum(sumps);

##CLASIFICACION##
wh = log(ph);
wh0 = log(probabilidadHam);
ws = log(ps);
ws0 = log(probabilidadSpam);

## g(h)
[filas,columnas]=size(te);
vectorClases = te(:,columnas);
teSinEt = te(:,[1:columnas-1]);
gh = wh*teSinEt'+ wh0;

##g(s)
gs = ws*teSinEt'+ ws0;


##Calcular el error
vectorClasificacion = gh<gs;
errores = vectorClases != vectorClasificacion';
PorcentajeErrores = sum(errores)/filas;
matrizErrores(contadorMatrizErrores,1) = epsilon;
matrizErrores(contadorMatrizErrores,2) = PorcentajeErrores;
contadorMatrizErrores++;
epsilon=epsilon/10;
endwhile
menorError = find(matrizErrores(:,2) == min(matrizErrores(:,2)));
disp("El menor error es");
disp(matrizErrores(menorError,2));
disp("Con un epsilon de");
disp(matrizErrores(menorError,1));
