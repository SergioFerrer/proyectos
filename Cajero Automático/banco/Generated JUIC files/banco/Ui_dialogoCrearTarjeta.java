/********************************************************************************
** Form generated from reading ui file 'dialogoCrearTarjeta.jui'
**
** Created: mar 13. may 10:20:15 2014
**      by: Qt User Interface Compiler version 4.5.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

package banco;

import com.trolltech.qt.core.*;
import com.trolltech.qt.gui.*;

public class Ui_dialogoCrearTarjeta implements com.trolltech.qt.QUiForm<QDialog>
{
    public QHBoxLayout horizontalLayout_5;
    public QVBoxLayout verticalLayout;
    public QHBoxLayout horizontalLayout;
    public QLabel label;
    public QComboBox comboBox;
    public QHBoxLayout horizontalLayout_2;
    public QLabel label_2;
    public QLineEdit ln;
    public QHBoxLayout horizontalLayout_3;
    public QLabel label_3;
    public QDoubleSpinBox ls;
    public QSpacerItem horizontalSpacer_2;
    public QHBoxLayout horizontalLayout_4;
    public QSpacerItem horizontalSpacer;
    public QPushButton bct;
    public QPushButton bs;

    public Ui_dialogoCrearTarjeta() { super(); }

    public void setupUi(QDialog dialogoCrearTarjeta)
    {
        dialogoCrearTarjeta.setObjectName("dialogoCrearTarjeta");
        dialogoCrearTarjeta.resize(new QSize(400, 300).expandedTo(dialogoCrearTarjeta.minimumSizeHint()));
        horizontalLayout_5 = new QHBoxLayout(dialogoCrearTarjeta);
        horizontalLayout_5.setObjectName("horizontalLayout_5");
        verticalLayout = new QVBoxLayout();
        verticalLayout.setObjectName("verticalLayout");
        horizontalLayout = new QHBoxLayout();
        horizontalLayout.setObjectName("horizontalLayout");
        label = new QLabel(dialogoCrearTarjeta);
        label.setObjectName("label");

        horizontalLayout.addWidget(label);

        comboBox = new QComboBox(dialogoCrearTarjeta);
        comboBox.setObjectName("comboBox");

        horizontalLayout.addWidget(comboBox);


        verticalLayout.addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2.setObjectName("horizontalLayout_2");
        label_2 = new QLabel(dialogoCrearTarjeta);
        label_2.setObjectName("label_2");

        horizontalLayout_2.addWidget(label_2);

        ln = new QLineEdit(dialogoCrearTarjeta);
        ln.setObjectName("ln");

        horizontalLayout_2.addWidget(ln);


        verticalLayout.addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3.setObjectName("horizontalLayout_3");
        label_3 = new QLabel(dialogoCrearTarjeta);
        label_3.setObjectName("label_3");

        horizontalLayout_3.addWidget(label_3);

        ls = new QDoubleSpinBox(dialogoCrearTarjeta);
        ls.setObjectName("ls");

        horizontalLayout_3.addWidget(ls);

        horizontalSpacer_2 = new QSpacerItem(40, 20, com.trolltech.qt.gui.QSizePolicy.Policy.Expanding, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum);

        horizontalLayout_3.addItem(horizontalSpacer_2);


        verticalLayout.addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4.setObjectName("horizontalLayout_4");
        horizontalSpacer = new QSpacerItem(40, 20, com.trolltech.qt.gui.QSizePolicy.Policy.Expanding, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum);

        horizontalLayout_4.addItem(horizontalSpacer);

        bct = new QPushButton(dialogoCrearTarjeta);
        bct.setObjectName("bct");

        horizontalLayout_4.addWidget(bct);

        bs = new QPushButton(dialogoCrearTarjeta);
        bs.setObjectName("bs");

        horizontalLayout_4.addWidget(bs);


        verticalLayout.addLayout(horizontalLayout_4);


        horizontalLayout_5.addLayout(verticalLayout);

        retranslateUi(dialogoCrearTarjeta);

        dialogoCrearTarjeta.connectSlotsByName();
    } // setupUi

    void retranslateUi(QDialog dialogoCrearTarjeta)
    {
        dialogoCrearTarjeta.setWindowTitle(com.trolltech.qt.core.QCoreApplication.translate("dialogoCrearTarjeta", "Dialog", null));
        label.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoCrearTarjeta", "Cuenta vinculada a la tarjeta", null));
        label_2.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoCrearTarjeta", "Nombre de la tarjeta", null));
        label_3.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoCrearTarjeta", "Saldo inicial de la tarjeta", null));
        bct.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoCrearTarjeta", "Crear tarjeta", null));
        bs.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoCrearTarjeta", "Salir", null));
    } // retranslateUi

}

