/********************************************************************************
** Form generated from reading ui file 'dialogoIngresoTarjeta.jui'
**
** Created: vie 23. may 17:27:45 2014
**      by: Qt User Interface Compiler version 4.5.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

package banco;

import com.trolltech.qt.core.*;
import com.trolltech.qt.gui.*;

public class Ui_dialogoIngresoTarjeta implements com.trolltech.qt.QUiForm<QWidget>
{
    public QVBoxLayout verticalLayout;
    public QHBoxLayout horizontalLayout_3;
    public QLabel label;
    public QComboBox comboBox;
    public QHBoxLayout horizontalLayout_4;
    public QLabel label_2;
    public QDoubleSpinBox doubleSpinBox;
    public QHBoxLayout horizontalLayout_2;
    public QLabel label_3;
    public QLineEdit lc;
    public QHBoxLayout horizontalLayout;
    public QSpacerItem horizontalSpacer;
    public QPushButton pushButton;
    public QPushButton pushButton_2;

    public Ui_dialogoIngresoTarjeta() { super(); }

    public void setupUi(QWidget dialogoIngresoTarjeta)
    {
        dialogoIngresoTarjeta.setObjectName("dialogoIngresoTarjeta");
        dialogoIngresoTarjeta.resize(new QSize(244, 182).expandedTo(dialogoIngresoTarjeta.minimumSizeHint()));
        verticalLayout = new QVBoxLayout(dialogoIngresoTarjeta);
        verticalLayout.setObjectName("verticalLayout");
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3.setObjectName("horizontalLayout_3");
        label = new QLabel(dialogoIngresoTarjeta);
        label.setObjectName("label");

        horizontalLayout_3.addWidget(label);

        comboBox = new QComboBox(dialogoIngresoTarjeta);
        comboBox.setObjectName("comboBox");

        horizontalLayout_3.addWidget(comboBox);


        verticalLayout.addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4.setObjectName("horizontalLayout_4");
        label_2 = new QLabel(dialogoIngresoTarjeta);
        label_2.setObjectName("label_2");

        horizontalLayout_4.addWidget(label_2);

        doubleSpinBox = new QDoubleSpinBox(dialogoIngresoTarjeta);
        doubleSpinBox.setObjectName("doubleSpinBox");

        horizontalLayout_4.addWidget(doubleSpinBox);


        verticalLayout.addLayout(horizontalLayout_4);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2.setObjectName("horizontalLayout_2");
        label_3 = new QLabel(dialogoIngresoTarjeta);
        label_3.setObjectName("label_3");

        horizontalLayout_2.addWidget(label_3);

        lc = new QLineEdit(dialogoIngresoTarjeta);
        lc.setObjectName("lc");

        horizontalLayout_2.addWidget(lc);


        verticalLayout.addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout.setObjectName("horizontalLayout");
        horizontalSpacer = new QSpacerItem(40, 20, com.trolltech.qt.gui.QSizePolicy.Policy.Expanding, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum);

        horizontalLayout.addItem(horizontalSpacer);

        pushButton = new QPushButton(dialogoIngresoTarjeta);
        pushButton.setObjectName("pushButton");

        horizontalLayout.addWidget(pushButton);

        pushButton_2 = new QPushButton(dialogoIngresoTarjeta);
        pushButton_2.setObjectName("pushButton_2");

        horizontalLayout.addWidget(pushButton_2);


        verticalLayout.addLayout(horizontalLayout);

        retranslateUi(dialogoIngresoTarjeta);

        dialogoIngresoTarjeta.connectSlotsByName();
    } // setupUi

    void retranslateUi(QWidget dialogoIngresoTarjeta)
    {
        dialogoIngresoTarjeta.setWindowTitle(com.trolltech.qt.core.QCoreApplication.translate("dialogoIngresoTarjeta", "Form", null));
        label.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoIngresoTarjeta", "Elige tarjeta", null));
        label_2.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoIngresoTarjeta", "Cantidad a recargar", null));
        label_3.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoIngresoTarjeta", "Concepto", null));
        pushButton.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoIngresoTarjeta", "Cargar", null));
        pushButton_2.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoIngresoTarjeta", "Cancelar", null));
    } // retranslateUi

}

