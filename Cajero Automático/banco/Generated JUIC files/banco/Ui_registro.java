/********************************************************************************
** Form generated from reading ui file 'registro.jui'
**
** Created: mar 6. may 09:21:54 2014
**      by: Qt User Interface Compiler version 4.5.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

package banco;

import com.trolltech.qt.core.*;
import com.trolltech.qt.gui.*;

public class Ui_registro implements com.trolltech.qt.QUiForm<QDialog>
{
    public QHBoxLayout horizontalLayout_6;
    public QVBoxLayout verticalLayout_2;
    public QVBoxLayout verticalLayout;
    public QLabel etiquetaError;
    public QLabel label;
    public QSpacerItem verticalSpacer;
    public QHBoxLayout horizontalLayout;
    public QLabel label_2;
    public QSpacerItem horizontalSpacer_2;
    public QLineEdit ln;
    public QSpacerItem verticalSpacer_2;
    public QHBoxLayout horizontalLayout_2;
    public QLabel label_3;
    public QLineEdit lc;
    public QSpacerItem verticalSpacer_3;
    public QHBoxLayout horizontalLayout_3;
    public QLabel label_5;
    public QLineEdit lc2;
    public QSpacerItem verticalSpacer_4;
    public QHBoxLayout horizontalLayout_4;
    public QLabel label_4;
    public QPushButton aceptar;
    public QPushButton cerrar;
    public QSpacerItem horizontalSpacer;
    public QSpacerItem verticalSpacer_5;

    public Ui_registro() { super(); }

    public void setupUi(QDialog registro)
    {
        registro.setObjectName("registro");
        registro.resize(new QSize(370, 264).expandedTo(registro.minimumSizeHint()));
        horizontalLayout_6 = new QHBoxLayout(registro);
        horizontalLayout_6.setObjectName("horizontalLayout_6");
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2.setObjectName("verticalLayout_2");
        verticalLayout = new QVBoxLayout();
        verticalLayout.setObjectName("verticalLayout");
        etiquetaError = new QLabel(registro);
        etiquetaError.setObjectName("etiquetaError");
        etiquetaError.setEnabled(true);
        QFont font = new QFont();
        font.setPointSize(11);
        font.setBold(true);
        font.setWeight(75);
        etiquetaError.setFont(font);

        verticalLayout.addWidget(etiquetaError);

        label = new QLabel(registro);
        label.setObjectName("label");

        verticalLayout.addWidget(label);

        verticalSpacer = new QSpacerItem(20, 7, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);

        verticalLayout.addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout.setObjectName("horizontalLayout");
        label_2 = new QLabel(registro);
        label_2.setObjectName("label_2");

        horizontalLayout.addWidget(label_2);

        horizontalSpacer_2 = new QSpacerItem(10, 20, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum);

        horizontalLayout.addItem(horizontalSpacer_2);

        ln = new QLineEdit(registro);
        ln.setObjectName("ln");
        QSizePolicy sizePolicy = new QSizePolicy(com.trolltech.qt.gui.QSizePolicy.Policy.Expanding, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);
        sizePolicy.setHorizontalStretch((byte)0);
        sizePolicy.setVerticalStretch((byte)0);
        sizePolicy.setHeightForWidth(ln.sizePolicy().hasHeightForWidth());
        ln.setSizePolicy(sizePolicy);

        horizontalLayout.addWidget(ln);


        verticalLayout.addLayout(horizontalLayout);

        verticalSpacer_2 = new QSpacerItem(20, 7, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);

        verticalLayout.addItem(verticalSpacer_2);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2.setObjectName("horizontalLayout_2");
        label_3 = new QLabel(registro);
        label_3.setObjectName("label_3");

        horizontalLayout_2.addWidget(label_3);

        lc = new QLineEdit(registro);
        lc.setObjectName("lc");
        lc.setEchoMode(com.trolltech.qt.gui.QLineEdit.EchoMode.Password);

        horizontalLayout_2.addWidget(lc);


        verticalLayout.addLayout(horizontalLayout_2);

        verticalSpacer_3 = new QSpacerItem(20, 7, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);

        verticalLayout.addItem(verticalSpacer_3);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3.setObjectName("horizontalLayout_3");
        label_5 = new QLabel(registro);
        label_5.setObjectName("label_5");

        horizontalLayout_3.addWidget(label_5);

        lc2 = new QLineEdit(registro);
        lc2.setObjectName("lc2");
        lc2.setEchoMode(com.trolltech.qt.gui.QLineEdit.EchoMode.Password);

        horizontalLayout_3.addWidget(lc2);


        verticalLayout.addLayout(horizontalLayout_3);

        verticalSpacer_4 = new QSpacerItem(20, 15, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);

        verticalLayout.addItem(verticalSpacer_4);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4.setObjectName("horizontalLayout_4");
        label_4 = new QLabel(registro);
        label_4.setObjectName("label_4");

        horizontalLayout_4.addWidget(label_4);

        aceptar = new QPushButton(registro);
        aceptar.setObjectName("aceptar");
        QSizePolicy sizePolicy1 = new QSizePolicy(com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Maximum);
        sizePolicy1.setHorizontalStretch((byte)200);
        sizePolicy1.setVerticalStretch((byte)200);
        sizePolicy1.setHeightForWidth(aceptar.sizePolicy().hasHeightForWidth());
        aceptar.setSizePolicy(sizePolicy1);

        horizontalLayout_4.addWidget(aceptar);

        cerrar = new QPushButton(registro);
        cerrar.setObjectName("cerrar");

        horizontalLayout_4.addWidget(cerrar);

        horizontalSpacer = new QSpacerItem(120, 20, com.trolltech.qt.gui.QSizePolicy.Policy.Expanding, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum);

        horizontalLayout_4.addItem(horizontalSpacer);


        verticalLayout.addLayout(horizontalLayout_4);


        verticalLayout_2.addLayout(verticalLayout);

        verticalSpacer_5 = new QSpacerItem(20, 20, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Expanding);

        verticalLayout_2.addItem(verticalSpacer_5);


        horizontalLayout_6.addLayout(verticalLayout_2);

        retranslateUi(registro);

        registro.connectSlotsByName();
    } // setupUi

    void retranslateUi(QDialog registro)
    {
        registro.setWindowTitle(com.trolltech.qt.core.QCoreApplication.translate("registro", "Dialog", null));
        etiquetaError.setText("");
        label.setText(com.trolltech.qt.core.QCoreApplication.translate("registro", "Introduce los datos con los que quieres darte de alta", null));
        label_2.setText(com.trolltech.qt.core.QCoreApplication.translate("registro", "Nomre de usuario", null));
        label_3.setText(com.trolltech.qt.core.QCoreApplication.translate("registro", "Elige una contrase\u00f1a", null));
        label_5.setText(com.trolltech.qt.core.QCoreApplication.translate("registro", "Repite la contrase\u00f1a", null));
        label_4.setText(com.trolltech.qt.core.QCoreApplication.translate("registro", "Nota: \u00a1No olvides estos datos!", null));
        aceptar.setText(com.trolltech.qt.core.QCoreApplication.translate("registro", "Darse de alta", null));
        cerrar.setText(com.trolltech.qt.core.QCoreApplication.translate("registro", "Cancelar", null));
    } // retranslateUi

}

