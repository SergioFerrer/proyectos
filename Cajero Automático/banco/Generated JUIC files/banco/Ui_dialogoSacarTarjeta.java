/********************************************************************************
** Form generated from reading ui file 'dialogoSacarTarjeta.jui'
**
** Created: vie 23. may 18:05:19 2014
**      by: Qt User Interface Compiler version 4.5.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

package banco;

import com.trolltech.qt.core.*;
import com.trolltech.qt.gui.*;

public class Ui_dialogoSacarTarjeta implements com.trolltech.qt.QUiForm<QDialog>
{
    public QHBoxLayout horizontalLayout_4;
    public QVBoxLayout verticalLayout;
    public QHBoxLayout horizontalLayout;
    public QLabel label;
    public QComboBox lt;
    public QHBoxLayout horizontalLayout_2;
    public QLabel label_2;
    public QDoubleSpinBox ls;
    public QHBoxLayout horizontalLayout_3;
    public QSpacerItem horizontalSpacer;
    public QPushButton bd;
    public QPushButton bs;

    public Ui_dialogoSacarTarjeta() { super(); }

    public void setupUi(QDialog dialogoSacarTarjeta)
    {
        dialogoSacarTarjeta.setObjectName("dialogoSacarTarjeta");
        dialogoSacarTarjeta.resize(new QSize(311, 160).expandedTo(dialogoSacarTarjeta.minimumSizeHint()));
        horizontalLayout_4 = new QHBoxLayout(dialogoSacarTarjeta);
        horizontalLayout_4.setObjectName("horizontalLayout_4");
        verticalLayout = new QVBoxLayout();
        verticalLayout.setObjectName("verticalLayout");
        horizontalLayout = new QHBoxLayout();
        horizontalLayout.setObjectName("horizontalLayout");
        label = new QLabel(dialogoSacarTarjeta);
        label.setObjectName("label");

        horizontalLayout.addWidget(label);

        lt = new QComboBox(dialogoSacarTarjeta);
        lt.setObjectName("lt");

        horizontalLayout.addWidget(lt);


        verticalLayout.addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2.setObjectName("horizontalLayout_2");
        label_2 = new QLabel(dialogoSacarTarjeta);
        label_2.setObjectName("label_2");

        horizontalLayout_2.addWidget(label_2);

        ls = new QDoubleSpinBox(dialogoSacarTarjeta);
        ls.setObjectName("ls");

        horizontalLayout_2.addWidget(ls);


        verticalLayout.addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3.setObjectName("horizontalLayout_3");
        horizontalSpacer = new QSpacerItem(40, 20, com.trolltech.qt.gui.QSizePolicy.Policy.Expanding, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum);

        horizontalLayout_3.addItem(horizontalSpacer);

        bd = new QPushButton(dialogoSacarTarjeta);
        bd.setObjectName("bd");

        horizontalLayout_3.addWidget(bd);

        bs = new QPushButton(dialogoSacarTarjeta);
        bs.setObjectName("bs");

        horizontalLayout_3.addWidget(bs);


        verticalLayout.addLayout(horizontalLayout_3);


        horizontalLayout_4.addLayout(verticalLayout);

        retranslateUi(dialogoSacarTarjeta);

        dialogoSacarTarjeta.connectSlotsByName();
    } // setupUi

    void retranslateUi(QDialog dialogoSacarTarjeta)
    {
        dialogoSacarTarjeta.setWindowTitle(com.trolltech.qt.core.QCoreApplication.translate("dialogoSacarTarjeta", "Dialog", null));
        label.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoSacarTarjeta", "Elije tarjeta", null));
        label_2.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoSacarTarjeta", "Cantidad a descargar", null));
        bd.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoSacarTarjeta", "Descargar", null));
        bs.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoSacarTarjeta", "Salir", null));
    } // retranslateUi

}

