/********************************************************************************
** Form generated from reading ui file 'dialogoExtraer.jui'
**
** Created: mar 20. may 10:23:40 2014
**      by: Qt User Interface Compiler version 4.5.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

package banco;

import com.trolltech.qt.core.*;
import com.trolltech.qt.gui.*;

public class Ui_dialogoExtraer implements com.trolltech.qt.QUiForm<QDialog>
{
    public QHBoxLayout horizontalLayout_5;
    public QVBoxLayout verticalLayout;
    public QHBoxLayout horizontalLayout_4;
    public QLabel label;
    public QComboBox lcu;
    public QHBoxLayout horizontalLayout_2;
    public QLabel label_2;
    public QDoubleSpinBox ls;
    public QHBoxLayout horizontalLayout;
    public QLabel label_3;
    public QLineEdit lc;
    public QHBoxLayout horizontalLayout_3;
    public QSpacerItem horizontalSpacer;
    public QPushButton be;
    public QPushButton bs;

    public Ui_dialogoExtraer() { super(); }

    public void setupUi(QDialog dialogoExtraer)
    {
        dialogoExtraer.setObjectName("dialogoExtraer");
        dialogoExtraer.resize(new QSize(315, 190).expandedTo(dialogoExtraer.minimumSizeHint()));
        horizontalLayout_5 = new QHBoxLayout(dialogoExtraer);
        horizontalLayout_5.setObjectName("horizontalLayout_5");
        verticalLayout = new QVBoxLayout();
        verticalLayout.setObjectName("verticalLayout");
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4.setObjectName("horizontalLayout_4");
        label = new QLabel(dialogoExtraer);
        label.setObjectName("label");

        horizontalLayout_4.addWidget(label);

        lcu = new QComboBox(dialogoExtraer);
        lcu.setObjectName("lcu");

        horizontalLayout_4.addWidget(lcu);


        verticalLayout.addLayout(horizontalLayout_4);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2.setObjectName("horizontalLayout_2");
        label_2 = new QLabel(dialogoExtraer);
        label_2.setObjectName("label_2");

        horizontalLayout_2.addWidget(label_2);

        ls = new QDoubleSpinBox(dialogoExtraer);
        ls.setObjectName("ls");

        horizontalLayout_2.addWidget(ls);


        verticalLayout.addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout.setObjectName("horizontalLayout");
        label_3 = new QLabel(dialogoExtraer);
        label_3.setObjectName("label_3");

        horizontalLayout.addWidget(label_3);

        lc = new QLineEdit(dialogoExtraer);
        lc.setObjectName("lc");

        horizontalLayout.addWidget(lc);


        verticalLayout.addLayout(horizontalLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3.setObjectName("horizontalLayout_3");
        horizontalSpacer = new QSpacerItem(40, 20, com.trolltech.qt.gui.QSizePolicy.Policy.Expanding, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum);

        horizontalLayout_3.addItem(horizontalSpacer);

        be = new QPushButton(dialogoExtraer);
        be.setObjectName("be");

        horizontalLayout_3.addWidget(be);

        bs = new QPushButton(dialogoExtraer);
        bs.setObjectName("bs");

        horizontalLayout_3.addWidget(bs);


        verticalLayout.addLayout(horizontalLayout_3);


        horizontalLayout_5.addLayout(verticalLayout);

        retranslateUi(dialogoExtraer);

        dialogoExtraer.connectSlotsByName();
    } // setupUi

    void retranslateUi(QDialog dialogoExtraer)
    {
        dialogoExtraer.setWindowTitle(com.trolltech.qt.core.QCoreApplication.translate("dialogoExtraer", "Dialog", null));
        label.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoExtraer", "Elige una cuenta", null));
        label_2.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoExtraer", "Cantidad a extraer", null));
        label_3.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoExtraer", "Concepto", null));
        be.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoExtraer", "Extraer", null));
        bs.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoExtraer", "Cancelar", null));
    } // retranslateUi

}

