/********************************************************************************
** Form generated from reading ui file 'dialogoCrearCuenta.jui'
**
** Created: mar 13. may 09:57:55 2014
**      by: Qt User Interface Compiler version 4.5.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

package banco;

import com.trolltech.qt.core.*;
import com.trolltech.qt.gui.*;

public class Ui_dialogoCrearCuenta implements com.trolltech.qt.QUiForm<QDialog>
{
    public QHBoxLayout horizontalLayout_4;
    public QVBoxLayout verticalLayout;
    public QHBoxLayout horizontalLayout_3;
    public QLabel label;
    public QLineEdit ln;
    public QHBoxLayout horizontalLayout_2;
    public QLabel label_2;
    public QDoubleSpinBox ls;
    public QSpacerItem horizontalSpacer_2;
    public QHBoxLayout horizontalLayout;
    public QSpacerItem horizontalSpacer;
    public QPushButton bcc;
    public QPushButton bs;

    public Ui_dialogoCrearCuenta() { super(); }

    public void setupUi(QDialog dialogoCrearCuenta)
    {
        dialogoCrearCuenta.setObjectName("dialogoCrearCuenta");
        dialogoCrearCuenta.resize(new QSize(329, 230).expandedTo(dialogoCrearCuenta.minimumSizeHint()));
        horizontalLayout_4 = new QHBoxLayout(dialogoCrearCuenta);
        horizontalLayout_4.setObjectName("horizontalLayout_4");
        verticalLayout = new QVBoxLayout();
        verticalLayout.setObjectName("verticalLayout");
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3.setObjectName("horizontalLayout_3");
        label = new QLabel(dialogoCrearCuenta);
        label.setObjectName("label");

        horizontalLayout_3.addWidget(label);

        ln = new QLineEdit(dialogoCrearCuenta);
        ln.setObjectName("ln");

        horizontalLayout_3.addWidget(ln);


        verticalLayout.addLayout(horizontalLayout_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2.setObjectName("horizontalLayout_2");
        label_2 = new QLabel(dialogoCrearCuenta);
        label_2.setObjectName("label_2");

        horizontalLayout_2.addWidget(label_2);

        ls = new QDoubleSpinBox(dialogoCrearCuenta);
        ls.setObjectName("ls");

        horizontalLayout_2.addWidget(ls);

        horizontalSpacer_2 = new QSpacerItem(64, 20, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum);

        horizontalLayout_2.addItem(horizontalSpacer_2);


        verticalLayout.addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout.setObjectName("horizontalLayout");
        horizontalSpacer = new QSpacerItem(110, 20, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum);

        horizontalLayout.addItem(horizontalSpacer);

        bcc = new QPushButton(dialogoCrearCuenta);
        bcc.setObjectName("bcc");

        horizontalLayout.addWidget(bcc);

        bs = new QPushButton(dialogoCrearCuenta);
        bs.setObjectName("bs");

        horizontalLayout.addWidget(bs);


        verticalLayout.addLayout(horizontalLayout);


        horizontalLayout_4.addLayout(verticalLayout);

        retranslateUi(dialogoCrearCuenta);

        dialogoCrearCuenta.connectSlotsByName();
    } // setupUi

    void retranslateUi(QDialog dialogoCrearCuenta)
    {
        dialogoCrearCuenta.setWindowTitle(com.trolltech.qt.core.QCoreApplication.translate("dialogoCrearCuenta", "Dialog", null));
        label.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoCrearCuenta", "Nombre de la cuenta", null));
        label_2.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoCrearCuenta", "Saldo inicial", null));
        bcc.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoCrearCuenta", "Crear cuenta", null));
        bs.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoCrearCuenta", "Salir", null));
    } // retranslateUi

}

