/********************************************************************************
** Form generated from reading ui file 'inicio.jui'
**
** Created: mar 6. may 09:41:07 2014
**      by: Qt User Interface Compiler version 4.5.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

package banco;

import com.trolltech.qt.core.*;
import com.trolltech.qt.gui.*;

public class Ui_inicio implements com.trolltech.qt.QUiForm<QDialog>
{
    public QVBoxLayout verticalLayout_2;
    public QVBoxLayout verticalLayout;
    public QLabel etiquetaError;
    public QLabel label_4;
    public QSpacerItem verticalSpacer_5;
    public QHBoxLayout horizontalLayout;
    public QLabel label;
    public QSpacerItem horizontalSpacer;
    public QLineEdit ln;
    public QSpacerItem verticalSpacer_4;
    public QHBoxLayout horizontalLayout_2;
    public QLabel label_2;
    public QSpacerItem horizontalSpacer_2;
    public QLineEdit lc;
    public QSpacerItem verticalSpacer_3;
    public QHBoxLayout horizontalLayout_3;
    public QSpacerItem horizontalSpacer_3;
    public QPushButton acceder;
    public QHBoxLayout horizontalLayout_4;
    public QLabel label_3;
    public QSpacerItem horizontalSpacer_4;
    public QPushButton registrarse;
    public QSpacerItem verticalSpacer_2;

    public Ui_inicio() { super(); }

    public void setupUi(QDialog inicio)
    {
        inicio.setObjectName("inicio");
        inicio.resize(new QSize(376, 238).expandedTo(inicio.minimumSizeHint()));
        inicio.setMinimumSize(new QSize(100, 0));
        verticalLayout_2 = new QVBoxLayout(inicio);
        verticalLayout_2.setObjectName("verticalLayout_2");
        verticalLayout = new QVBoxLayout();
        verticalLayout.setObjectName("verticalLayout");
        etiquetaError = new QLabel(inicio);
        etiquetaError.setObjectName("etiquetaError");
        QFont font = new QFont();
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        etiquetaError.setFont(font);

        verticalLayout.addWidget(etiquetaError);

        label_4 = new QLabel(inicio);
        label_4.setObjectName("label_4");

        verticalLayout.addWidget(label_4);

        verticalSpacer_5 = new QSpacerItem(20, 10, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);

        verticalLayout.addItem(verticalSpacer_5);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout.setObjectName("horizontalLayout");
        label = new QLabel(inicio);
        label.setObjectName("label");

        horizontalLayout.addWidget(label);

        horizontalSpacer = new QSpacerItem(10, 0, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum);

        horizontalLayout.addItem(horizontalSpacer);

        ln = new QLineEdit(inicio);
        ln.setObjectName("ln");

        horizontalLayout.addWidget(ln);


        verticalLayout.addLayout(horizontalLayout);

        verticalSpacer_4 = new QSpacerItem(20, 10, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);

        verticalLayout.addItem(verticalSpacer_4);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2.setObjectName("horizontalLayout_2");
        label_2 = new QLabel(inicio);
        label_2.setObjectName("label_2");

        horizontalLayout_2.addWidget(label_2);

        horizontalSpacer_2 = new QSpacerItem(35, 20, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum);

        horizontalLayout_2.addItem(horizontalSpacer_2);

        lc = new QLineEdit(inicio);
        lc.setObjectName("lc");
        lc.setEchoMode(com.trolltech.qt.gui.QLineEdit.EchoMode.Password);

        horizontalLayout_2.addWidget(lc);


        verticalLayout.addLayout(horizontalLayout_2);

        verticalSpacer_3 = new QSpacerItem(20, 10, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);

        verticalLayout.addItem(verticalSpacer_3);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3.setObjectName("horizontalLayout_3");
        horizontalSpacer_3 = new QSpacerItem(280, 0, com.trolltech.qt.gui.QSizePolicy.Policy.Expanding, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum);

        horizontalLayout_3.addItem(horizontalSpacer_3);

        acceder = new QPushButton(inicio);
        acceder.setObjectName("acceder");
        QSizePolicy sizePolicy = new QSizePolicy(com.trolltech.qt.gui.QSizePolicy.Policy.Fixed, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);
        sizePolicy.setHorizontalStretch((byte)0);
        sizePolicy.setVerticalStretch((byte)0);
        sizePolicy.setHeightForWidth(acceder.sizePolicy().hasHeightForWidth());
        acceder.setSizePolicy(sizePolicy);

        horizontalLayout_3.addWidget(acceder);


        verticalLayout.addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4.setObjectName("horizontalLayout_4");
        label_3 = new QLabel(inicio);
        label_3.setObjectName("label_3");

        horizontalLayout_4.addWidget(label_3);

        horizontalSpacer_4 = new QSpacerItem(60, 20, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum);

        horizontalLayout_4.addItem(horizontalSpacer_4);

        registrarse = new QPushButton(inicio);
        registrarse.setObjectName("registrarse");
        QSizePolicy sizePolicy1 = new QSizePolicy(com.trolltech.qt.gui.QSizePolicy.Policy.Fixed, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);
        sizePolicy1.setHorizontalStretch((byte)0);
        sizePolicy1.setVerticalStretch((byte)0);
        sizePolicy1.setHeightForWidth(registrarse.sizePolicy().hasHeightForWidth());
        registrarse.setSizePolicy(sizePolicy1);

        horizontalLayout_4.addWidget(registrarse);


        verticalLayout.addLayout(horizontalLayout_4);

        verticalSpacer_2 = new QSpacerItem(0, 0, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);

        verticalLayout.addItem(verticalSpacer_2);


        verticalLayout_2.addLayout(verticalLayout);

        retranslateUi(inicio);

        inicio.connectSlotsByName();
    } // setupUi

    void retranslateUi(QDialog inicio)
    {
        inicio.setWindowTitle(com.trolltech.qt.core.QCoreApplication.translate("inicio", "Dialog", null));
        etiquetaError.setText("");
        label_4.setText(com.trolltech.qt.core.QCoreApplication.translate("inicio", "Introduce tus datos de indetificaci\u00f3n", null));
        label.setText(com.trolltech.qt.core.QCoreApplication.translate("inicio", "Nomre de usuario", null));
        label_2.setText(com.trolltech.qt.core.QCoreApplication.translate("inicio", "Contrase\u00f1a:", null));
        acceder.setText(com.trolltech.qt.core.QCoreApplication.translate("inicio", "Acceder", null));
        label_3.setText(com.trolltech.qt.core.QCoreApplication.translate("inicio", "\u00bfEres nuevo y aun no te has dado de alta?", null));
        registrarse.setText(com.trolltech.qt.core.QCoreApplication.translate("inicio", "Reg\u00edstrate", null));
    } // retranslateUi

}

