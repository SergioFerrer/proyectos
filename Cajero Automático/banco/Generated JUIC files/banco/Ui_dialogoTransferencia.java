/********************************************************************************
** Form generated from reading ui file 'dialogoTransferencia.jui'
**
** Created: vie 23. may 18:48:36 2014
**      by: Qt User Interface Compiler version 4.5.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

package banco;

import com.trolltech.qt.core.*;
import com.trolltech.qt.gui.*;

public class Ui_dialogoTransferencia implements com.trolltech.qt.QUiForm<QDialog>
{
    public QHBoxLayout horizontalLayout_5;
    public QVBoxLayout verticalLayout;
    public QHBoxLayout horizontalLayout_4;
    public QLabel label;
    public QComboBox lce;
    public QHBoxLayout horizontalLayout_3;
    public QLabel label_2;
    public QComboBox lci;
    public QHBoxLayout horizontalLayout_2;
    public QLabel label_3;
    public QDoubleSpinBox ls;
    public QHBoxLayout horizontalLayout;
    public QSpacerItem horizontalSpacer;
    public QPushButton bt;
    public QPushButton bc;

    public Ui_dialogoTransferencia() { super(); }

    public void setupUi(QDialog dialogoTransferencia)
    {
        dialogoTransferencia.setObjectName("dialogoTransferencia");
        dialogoTransferencia.resize(new QSize(400, 300).expandedTo(dialogoTransferencia.minimumSizeHint()));
        horizontalLayout_5 = new QHBoxLayout(dialogoTransferencia);
        horizontalLayout_5.setObjectName("horizontalLayout_5");
        verticalLayout = new QVBoxLayout();
        verticalLayout.setObjectName("verticalLayout");
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4.setObjectName("horizontalLayout_4");
        label = new QLabel(dialogoTransferencia);
        label.setObjectName("label");

        horizontalLayout_4.addWidget(label);

        lce = new QComboBox(dialogoTransferencia);
        lce.setObjectName("lce");

        horizontalLayout_4.addWidget(lce);


        verticalLayout.addLayout(horizontalLayout_4);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3.setObjectName("horizontalLayout_3");
        label_2 = new QLabel(dialogoTransferencia);
        label_2.setObjectName("label_2");

        horizontalLayout_3.addWidget(label_2);

        lci = new QComboBox(dialogoTransferencia);
        lci.setObjectName("lci");

        horizontalLayout_3.addWidget(lci);


        verticalLayout.addLayout(horizontalLayout_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2.setObjectName("horizontalLayout_2");
        label_3 = new QLabel(dialogoTransferencia);
        label_3.setObjectName("label_3");

        horizontalLayout_2.addWidget(label_3);

        ls = new QDoubleSpinBox(dialogoTransferencia);
        ls.setObjectName("ls");

        horizontalLayout_2.addWidget(ls);


        verticalLayout.addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout.setObjectName("horizontalLayout");
        horizontalSpacer = new QSpacerItem(40, 20, com.trolltech.qt.gui.QSizePolicy.Policy.Expanding, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum);

        horizontalLayout.addItem(horizontalSpacer);

        bt = new QPushButton(dialogoTransferencia);
        bt.setObjectName("bt");

        horizontalLayout.addWidget(bt);

        bc = new QPushButton(dialogoTransferencia);
        bc.setObjectName("bc");

        horizontalLayout.addWidget(bc);


        verticalLayout.addLayout(horizontalLayout);


        horizontalLayout_5.addLayout(verticalLayout);

        retranslateUi(dialogoTransferencia);

        dialogoTransferencia.connectSlotsByName();
    } // setupUi

    void retranslateUi(QDialog dialogoTransferencia)
    {
        dialogoTransferencia.setWindowTitle(com.trolltech.qt.core.QCoreApplication.translate("dialogoTransferencia", "Dialog", null));
        label.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoTransferencia", "Cuenta de la que EXTRAER", null));
        label_2.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoTransferencia", "Cuenta en la que INGRESAR", null));
        label_3.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoTransferencia", "Cantidad a transferir", null));
        bt.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoTransferencia", "Transferir", null));
        bc.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoTransferencia", "Cancelar", null));
    } // retranslateUi

}

