/********************************************************************************
** Form generated from reading ui file 'sesionUsuario.jui'
**
** Created: vie 23. may 20:35:12 2014
**      by: Qt User Interface Compiler version 4.5.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

package banco;

import com.trolltech.qt.core.*;
import com.trolltech.qt.gui.*;

public class Ui_sesionUsuario implements com.trolltech.qt.QUiForm<QMainWindow>
{
    public QWidget centralwidget;
    public QVBoxLayout verticalLayout_4;
    public QVBoxLayout verticalLayout_3;
    public QHBoxLayout horizontalLayout;
    public QPushButton botonCrearCuenta;
    public QPushButton botonCrearTarjeta;
    public QPushButton botonIngresar;
    public QPushButton botonSacar;
    public QPushButton botonCargarTarjeta;
    public QPushButton botonDescargarTarjeta;
    public QPushButton botonTransferencia;
    public QSpacerItem verticalSpacer_3;
    public QHBoxLayout horizontalLayout_3;
    public QVBoxLayout verticalLayout;
    public QLabel label;
    public QListWidget cajaCuentas;
    public QLabel label_2;
    public QListWidget cajaTarjetas;
    public QSpacerItem horizontalSpacer_2;
    public QVBoxLayout verticalLayout_2;
    public QSpacerItem verticalSpacer_2;
    public QTextBrowser caja;
    public QSpacerItem verticalSpacer;
    public QHBoxLayout horizontalLayout_2;
    public QSpacerItem horizontalSpacer;
    public QPushButton botonSalir;
    public QMenuBar menubar;
    public QStatusBar statusbar;

    public Ui_sesionUsuario() { super(); }

    public void setupUi(QMainWindow sesionUsuario)
    {
        sesionUsuario.setObjectName("sesionUsuario");
        sesionUsuario.resize(new QSize(762, 610).expandedTo(sesionUsuario.minimumSizeHint()));
        centralwidget = new QWidget(sesionUsuario);
        centralwidget.setObjectName("centralwidget");
        verticalLayout_4 = new QVBoxLayout(centralwidget);
        verticalLayout_4.setObjectName("verticalLayout_4");
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3.setObjectName("verticalLayout_3");
        horizontalLayout = new QHBoxLayout();
        horizontalLayout.setObjectName("horizontalLayout");
        botonCrearCuenta = new QPushButton(centralwidget);
        botonCrearCuenta.setObjectName("botonCrearCuenta");
        QSizePolicy sizePolicy = new QSizePolicy(com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);
        sizePolicy.setHorizontalStretch((byte)0);
        sizePolicy.setVerticalStretch((byte)7);
        sizePolicy.setHeightForWidth(botonCrearCuenta.sizePolicy().hasHeightForWidth());
        botonCrearCuenta.setSizePolicy(sizePolicy);

        horizontalLayout.addWidget(botonCrearCuenta);

        botonCrearTarjeta = new QPushButton(centralwidget);
        botonCrearTarjeta.setObjectName("botonCrearTarjeta");
        QSizePolicy sizePolicy1 = new QSizePolicy(com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);
        sizePolicy1.setHorizontalStretch((byte)0);
        sizePolicy1.setVerticalStretch((byte)7);
        sizePolicy1.setHeightForWidth(botonCrearTarjeta.sizePolicy().hasHeightForWidth());
        botonCrearTarjeta.setSizePolicy(sizePolicy1);

        horizontalLayout.addWidget(botonCrearTarjeta);

        botonIngresar = new QPushButton(centralwidget);
        botonIngresar.setObjectName("botonIngresar");
        QSizePolicy sizePolicy2 = new QSizePolicy(com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);
        sizePolicy2.setHorizontalStretch((byte)0);
        sizePolicy2.setVerticalStretch((byte)7);
        sizePolicy2.setHeightForWidth(botonIngresar.sizePolicy().hasHeightForWidth());
        botonIngresar.setSizePolicy(sizePolicy2);

        horizontalLayout.addWidget(botonIngresar);

        botonSacar = new QPushButton(centralwidget);
        botonSacar.setObjectName("botonSacar");
        QSizePolicy sizePolicy3 = new QSizePolicy(com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);
        sizePolicy3.setHorizontalStretch((byte)0);
        sizePolicy3.setVerticalStretch((byte)7);
        sizePolicy3.setHeightForWidth(botonSacar.sizePolicy().hasHeightForWidth());
        botonSacar.setSizePolicy(sizePolicy3);

        horizontalLayout.addWidget(botonSacar);

        botonCargarTarjeta = new QPushButton(centralwidget);
        botonCargarTarjeta.setObjectName("botonCargarTarjeta");
        QSizePolicy sizePolicy4 = new QSizePolicy(com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);
        sizePolicy4.setHorizontalStretch((byte)0);
        sizePolicy4.setVerticalStretch((byte)7);
        sizePolicy4.setHeightForWidth(botonCargarTarjeta.sizePolicy().hasHeightForWidth());
        botonCargarTarjeta.setSizePolicy(sizePolicy4);

        horizontalLayout.addWidget(botonCargarTarjeta);

        botonDescargarTarjeta = new QPushButton(centralwidget);
        botonDescargarTarjeta.setObjectName("botonDescargarTarjeta");
        QSizePolicy sizePolicy5 = new QSizePolicy(com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);
        sizePolicy5.setHorizontalStretch((byte)0);
        sizePolicy5.setVerticalStretch((byte)7);
        sizePolicy5.setHeightForWidth(botonDescargarTarjeta.sizePolicy().hasHeightForWidth());
        botonDescargarTarjeta.setSizePolicy(sizePolicy5);

        horizontalLayout.addWidget(botonDescargarTarjeta);

        botonTransferencia = new QPushButton(centralwidget);
        botonTransferencia.setObjectName("botonTransferencia");
        QSizePolicy sizePolicy6 = new QSizePolicy(com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);
        sizePolicy6.setHorizontalStretch((byte)0);
        sizePolicy6.setVerticalStretch((byte)7);
        sizePolicy6.setHeightForWidth(botonTransferencia.sizePolicy().hasHeightForWidth());
        botonTransferencia.setSizePolicy(sizePolicy6);

        horizontalLayout.addWidget(botonTransferencia);


        verticalLayout_3.addLayout(horizontalLayout);

        verticalSpacer_3 = new QSpacerItem(20, 10, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);

        verticalLayout_3.addItem(verticalSpacer_3);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3.setObjectName("horizontalLayout_3");
        verticalLayout = new QVBoxLayout();
        verticalLayout.setObjectName("verticalLayout");
        label = new QLabel(centralwidget);
        label.setObjectName("label");
        QFont font = new QFont();
        font.setPointSize(9);
        label.setFont(font);

        verticalLayout.addWidget(label);

        cajaCuentas = new QListWidget(centralwidget);
        cajaCuentas.setObjectName("cajaCuentas");
        cajaCuentas.setEnabled(false);

        verticalLayout.addWidget(cajaCuentas);

        label_2 = new QLabel(centralwidget);
        label_2.setObjectName("label_2");
        QFont font1 = new QFont();
        font1.setPointSize(9);
        label_2.setFont(font1);

        verticalLayout.addWidget(label_2);

        cajaTarjetas = new QListWidget(centralwidget);
        cajaTarjetas.setObjectName("cajaTarjetas");
        cajaTarjetas.setEnabled(false);

        verticalLayout.addWidget(cajaTarjetas);


        horizontalLayout_3.addLayout(verticalLayout);

        horizontalSpacer_2 = new QSpacerItem(40, 20, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum);

        horizontalLayout_3.addItem(horizontalSpacer_2);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2.setObjectName("verticalLayout_2");
        verticalSpacer_2 = new QSpacerItem(20, 16, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);

        verticalLayout_2.addItem(verticalSpacer_2);

        caja = new QTextBrowser(centralwidget);
        caja.setObjectName("caja");
        QFont font2 = new QFont();
        font2.setPointSize(10);
        caja.setFont(font2);

        verticalLayout_2.addWidget(caja);

        verticalSpacer = new QSpacerItem(20, 20, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);

        verticalLayout_2.addItem(verticalSpacer);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2.setObjectName("horizontalLayout_2");
        horizontalSpacer = new QSpacerItem(268, 20, com.trolltech.qt.gui.QSizePolicy.Policy.Expanding, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum);

        horizontalLayout_2.addItem(horizontalSpacer);

        botonSalir = new QPushButton(centralwidget);
        botonSalir.setObjectName("botonSalir");
        QSizePolicy sizePolicy7 = new QSizePolicy(com.trolltech.qt.gui.QSizePolicy.Policy.Minimum, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);
        sizePolicy7.setHorizontalStretch((byte)0);
        sizePolicy7.setVerticalStretch((byte)10);
        sizePolicy7.setHeightForWidth(botonSalir.sizePolicy().hasHeightForWidth());
        botonSalir.setSizePolicy(sizePolicy7);
        QFont font3 = new QFont();
        font3.setPointSize(10);
        botonSalir.setFont(font3);

        horizontalLayout_2.addWidget(botonSalir);


        verticalLayout_2.addLayout(horizontalLayout_2);


        horizontalLayout_3.addLayout(verticalLayout_2);


        verticalLayout_3.addLayout(horizontalLayout_3);


        verticalLayout_4.addLayout(verticalLayout_3);

        sesionUsuario.setCentralWidget(centralwidget);
        menubar = new QMenuBar(sesionUsuario);
        menubar.setObjectName("menubar");
        menubar.setGeometry(new QRect(0, 0, 762, 22));
        sesionUsuario.setMenuBar(menubar);
        statusbar = new QStatusBar(sesionUsuario);
        statusbar.setObjectName("statusbar");
        sesionUsuario.setStatusBar(statusbar);
        retranslateUi(sesionUsuario);

        sesionUsuario.connectSlotsByName();
    } // setupUi

    void retranslateUi(QMainWindow sesionUsuario)
    {
        sesionUsuario.setWindowTitle(com.trolltech.qt.core.QCoreApplication.translate("sesionUsuario", "MainWindow", null));
        botonCrearCuenta.setText(com.trolltech.qt.core.QCoreApplication.translate("sesionUsuario", "Crear cuenta", null));
        botonCrearTarjeta.setText(com.trolltech.qt.core.QCoreApplication.translate("sesionUsuario", "Crear Tarjeta", null));
        botonIngresar.setText(com.trolltech.qt.core.QCoreApplication.translate("sesionUsuario", "Realizar ingreso", null));
        botonSacar.setText(com.trolltech.qt.core.QCoreApplication.translate("sesionUsuario", "Sacar dinero", null));
        botonCargarTarjeta.setText(com.trolltech.qt.core.QCoreApplication.translate("sesionUsuario", "Cargar Tarjeta", null));
        botonDescargarTarjeta.setText(com.trolltech.qt.core.QCoreApplication.translate("sesionUsuario", "Descargar Tarjeta", null));
        botonTransferencia.setText(com.trolltech.qt.core.QCoreApplication.translate("sesionUsuario", "Realziar transferencia", null));
        label.setText(com.trolltech.qt.core.QCoreApplication.translate("sesionUsuario", "Cuentas disponibles", null));
        label_2.setText(com.trolltech.qt.core.QCoreApplication.translate("sesionUsuario", "Tarjetas disponibles", null));
        botonSalir.setText(com.trolltech.qt.core.QCoreApplication.translate("sesionUsuario", "Salir", null));
    } // retranslateUi

}

