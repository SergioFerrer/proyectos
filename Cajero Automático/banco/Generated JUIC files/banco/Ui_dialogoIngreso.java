/********************************************************************************
** Form generated from reading ui file 'dialogoIngreso.jui'
**
** Created: mar 20. may 09:51:01 2014
**      by: Qt User Interface Compiler version 4.5.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

package banco;

import com.trolltech.qt.core.*;
import com.trolltech.qt.gui.*;

public class Ui_dialogoIngreso implements com.trolltech.qt.QUiForm<QDialog>
{
    public QWidget widget;
    public QVBoxLayout verticalLayout;
    public QHBoxLayout horizontalLayout;
    public QLabel label;
    public QComboBox lcu;
    public QSpacerItem horizontalSpacer_3;
    public QHBoxLayout horizontalLayout_2;
    public QLabel label_2;
    public QDoubleSpinBox ld;
    public QSpacerItem horizontalSpacer_5;
    public QHBoxLayout horizontalLayout_4;
    public QLabel label_3;
    public QLineEdit lc;
    public QHBoxLayout horizontalLayout_3;
    public QSpacerItem horizontalSpacer;
    public QPushButton bi;
    public QPushButton bs;

    public Ui_dialogoIngreso() { super(); }

    public void setupUi(QDialog dialogoIngreso)
    {
        dialogoIngreso.setObjectName("dialogoIngreso");
        dialogoIngreso.resize(new QSize(333, 135).expandedTo(dialogoIngreso.minimumSizeHint()));
        widget = new QWidget(dialogoIngreso);
        widget.setObjectName("widget");
        widget.setGeometry(new QRect(10, 10, 319, 114));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout.setObjectName("verticalLayout");
        horizontalLayout = new QHBoxLayout();
        horizontalLayout.setObjectName("horizontalLayout");
        label = new QLabel(widget);
        label.setObjectName("label");
        QSizePolicy sizePolicy = new QSizePolicy(com.trolltech.qt.gui.QSizePolicy.Policy.Preferred, com.trolltech.qt.gui.QSizePolicy.Policy.Fixed);
        sizePolicy.setHorizontalStretch((byte)0);
        sizePolicy.setVerticalStretch((byte)0);
        sizePolicy.setHeightForWidth(label.sizePolicy().hasHeightForWidth());
        label.setSizePolicy(sizePolicy);

        horizontalLayout.addWidget(label);

        lcu = new QComboBox(widget);
        lcu.setObjectName("lcu");

        horizontalLayout.addWidget(lcu);

        horizontalSpacer_3 = new QSpacerItem(150, 20, com.trolltech.qt.gui.QSizePolicy.Policy.Expanding, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum);

        horizontalLayout.addItem(horizontalSpacer_3);


        verticalLayout.addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2.setObjectName("horizontalLayout_2");
        label_2 = new QLabel(widget);
        label_2.setObjectName("label_2");

        horizontalLayout_2.addWidget(label_2);

        ld = new QDoubleSpinBox(widget);
        ld.setObjectName("ld");

        horizontalLayout_2.addWidget(ld);

        horizontalSpacer_5 = new QSpacerItem(40, 20, com.trolltech.qt.gui.QSizePolicy.Policy.Expanding, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum);

        horizontalLayout_2.addItem(horizontalSpacer_5);


        verticalLayout.addLayout(horizontalLayout_2);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4.setObjectName("horizontalLayout_4");
        label_3 = new QLabel(widget);
        label_3.setObjectName("label_3");

        horizontalLayout_4.addWidget(label_3);

        lc = new QLineEdit(widget);
        lc.setObjectName("lc");

        horizontalLayout_4.addWidget(lc);


        verticalLayout.addLayout(horizontalLayout_4);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3.setObjectName("horizontalLayout_3");
        horizontalSpacer = new QSpacerItem(40, 20, com.trolltech.qt.gui.QSizePolicy.Policy.Expanding, com.trolltech.qt.gui.QSizePolicy.Policy.Minimum);

        horizontalLayout_3.addItem(horizontalSpacer);

        bi = new QPushButton(widget);
        bi.setObjectName("bi");

        horizontalLayout_3.addWidget(bi);

        bs = new QPushButton(widget);
        bs.setObjectName("bs");

        horizontalLayout_3.addWidget(bs);


        verticalLayout.addLayout(horizontalLayout_3);

        retranslateUi(dialogoIngreso);

        dialogoIngreso.connectSlotsByName();
    } // setupUi

    void retranslateUi(QDialog dialogoIngreso)
    {
        dialogoIngreso.setWindowTitle(com.trolltech.qt.core.QCoreApplication.translate("dialogoIngreso", "Dialog", null));
        label.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoIngreso", "Elige una cuenta", null));
        label_2.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoIngreso", "Cantidad a ingresar", null));
        label_3.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoIngreso", "Concepto", null));
        bi.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoIngreso", "Ingresar", null));
        bs.setText(com.trolltech.qt.core.QCoreApplication.translate("dialogoIngreso", "Cancelar", null));
    } // retranslateUi

}

