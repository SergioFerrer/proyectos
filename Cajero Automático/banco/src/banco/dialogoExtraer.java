package banco;

import com.trolltech.qt.gui.*;

public class dialogoExtraer extends QDialog {

    Ui_dialogoExtraer ui = new Ui_dialogoExtraer();

    public static void main(String[] args) {
        QApplication.initialize(args);

        dialogoExtraer testdialogoExtraer = new dialogoExtraer();
        testdialogoExtraer.show();

        QApplication.exec();
    }

    public dialogoExtraer() {
        ui.setupUi(this);
    }

    public dialogoExtraer(QWidget parent) {
        super(parent);
        ui.setupUi(this);
    }
}
