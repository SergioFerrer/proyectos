package banco;

import com.trolltech.qt.gui.*;

public class dialogoIngresoTarjeta extends QWidget {

    Ui_dialogoIngresoTarjeta ui = new Ui_dialogoIngresoTarjeta();

    public static void main(String[] args) {
        QApplication.initialize(args);

        dialogoIngresoTarjeta testdialogoIngresoTarjeta = new dialogoIngresoTarjeta();
        testdialogoIngresoTarjeta.show();

        QApplication.exec();
    }

    public dialogoIngresoTarjeta() {
        ui.setupUi(this);
    }

    public dialogoIngresoTarjeta(QWidget parent) {
        super(parent);
        ui.setupUi(this);
    }
}
