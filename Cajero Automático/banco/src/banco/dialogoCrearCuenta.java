package banco;

import com.trolltech.qt.gui.*;

public class dialogoCrearCuenta extends QDialog {
	usuario user;
    Ui_dialogoCrearCuenta ui = new Ui_dialogoCrearCuenta();

    public static void main(String[] args) {
        QApplication.initialize(args);

        dialogoCrearCuenta testdialogoCrearCuenta = new dialogoCrearCuenta();
        testdialogoCrearCuenta.show();

        QApplication.exec();
    }

    public dialogoCrearCuenta() {
        ui.setupUi(this);
    }

    public dialogoCrearCuenta(QWidget parent) {

    	super(parent);
        ui.setupUi(this);
    }
   
}
