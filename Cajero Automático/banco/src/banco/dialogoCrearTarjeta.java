package banco;

import com.trolltech.qt.gui.*;

public class dialogoCrearTarjeta extends QDialog {

    Ui_dialogoCrearTarjeta ui = new Ui_dialogoCrearTarjeta();

    public static void main(String[] args) {
        QApplication.initialize(args);

        dialogoCrearTarjeta testdialogoCrearTarjeta = new dialogoCrearTarjeta();
        testdialogoCrearTarjeta.show();

        QApplication.exec();
    }

    public dialogoCrearTarjeta() {
        ui.setupUi(this);
    }

    public dialogoCrearTarjeta(QWidget parent) {
        super(parent);
        ui.setupUi(this);
    }
}
