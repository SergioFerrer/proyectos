package banco;


import java.io.*;
import java.util.*;

public class tarjeta {
public double saldo;
private String id;
private Cuenta c;

public tarjeta(Cuenta cu, String str,double s,boolean x){
	this.c=cu;
	this.id=this.id = cu.id+"/tarjetas/" +str+"/";
	this.saldo = s;
}	
public tarjeta(Cuenta cu, String str,boolean x){
	this.c=cu;
	this.id=this.id = cu.id+"/tarjetas/" +str+"/";
	
}	
	
public tarjeta(Cuenta cu,String str){
		this.c= cu;
		this.id= str;
		saldo = 0;
		this.id = cu.id+"/tarjetas/" +str+"/";
		File directorio = new File(c.id+"/tarjetas/"+str+"/");
		File f = new File(this.id+"/saldo.txt");
		File f2 = new File (this.id+"/ultimosMovimientos.txt");
		try{
			directorio.mkdir();
			PrintWriter p = new PrintWriter(f);
			p.println("0");
			p.close();
			p = new PrintWriter (f2);
			p.print("");
			p.close();
		}catch(IOException e){}
		
	}
	public  tarjeta(Cuenta cu,String str,double saldo){
		
		this.c= cu;
		this.id= str;
		this.saldo = saldo;
		this.id = cu.id+"/tarjetas/" +str+"/";
		File directorio = new File(c.id+"/tarjetas/"+str+"/");
		File f = new File(this.id+"/saldo.txt");
		File f2 = new File (this.id+"/ultimosMovimientos.txt");
		try{
			directorio.mkdir();
			PrintWriter p = new PrintWriter(f);
			p.println(saldo);
			p.close();
			p = new PrintWriter(f2);
			p.println("INGRESO: Saldo inicial "+saldo);
			p.close();
			
		}catch(IOException e){}
		
			
		
	}	
	
	public double saldo(){return this.saldo;}
	
	public String id(){return this.id;}
	
	public boolean cargar(double s,String concept){
		if(concept.equals("")) concept="Has cargado la tarjeta virtual";
		if(s>c.saldo) return false;
		this.saldo+= s;
		c.retirar(s, "Carga de la tarjeta virutal");
		try{
			String str = this.id+"saldo.txt";
			File f = new File(str);
			Scanner t = new Scanner(f);
			
			double ant = Double.parseDouble(t.nextLine());
			t.close();
			
			PrintWriter p =  new PrintWriter(f);
			ant+= this.saldo;
			p.println(ant);
			p.close();
			}catch(IOException e){}
		File f2 = new File (this.id+"/ultimosMovimientos.txt");
		try{
		 Scanner t = new Scanner (f2);
         String str = "";
         while(t.hasNextLine()){
             str += t.nextLine()+"\n";
         }
         t.close();
         str+="INGRESO: "+concept + " " + s+" �";
         PrintWriter p = new PrintWriter(f2);
         t= new Scanner(str);
         while(t.hasNextLine()){
             p.println(t.nextLine());
         	}
         p.close();
         t.close();
         }catch(IOException e){}
		return true;
	}
	
	public boolean descargar (double s,String concept){
		if(concept.equals("")) concept="Descarga de la tarjeta virtual";
		if(s> this.saldo) return false;
		saldo-=s;
		try{
			PrintWriter p =  new PrintWriter(this.id+"/saldo.txt");
			p.println(this.saldo);
			p.close();
			c.ingresar(s,"Descarga de la tarjeta virtual");
			
			}catch(IOException e){}
		File f2 = new File (this.id+"/ultimosMovimientos.txt");
		try{
			 Scanner t = new Scanner (f2);
	         String str = "";
	         while(t.hasNextLine()){
	             str += t.nextLine()+"\n";
	         }
	         t.close();
	         str+="EXTRACI�N: "+concept + " " + s+" �";
	         PrintWriter p = new PrintWriter(f2);
	         t= new Scanner(str);
	         while(t.hasNextLine()){
	             p.println(t.nextLine());
	         	}
	         p.close();
	         t.close();
	         }catch(IOException e){}
		return true;
	}

	public void vaciar(){
		this.descargar(this.saldo,"Vaciado de la tarjeta virtual");
		c.ingresar(this.saldo, "Vaciado de la tarjeta virutal");
	}
}
