package banco;

import java.util.*;
import java.io.*;


public class Cuenta {
String id;
double saldo;
public Cuenta(usuario user, String name,double s, boolean b){
	this.id= user.nombre+"/"+name+"/";
	saldo =s;
}
	public Cuenta(usuario user,String name){
		this.id= user.nombre+"/"+name+"/";
		File f = new File (this.id);
		f.mkdir();
		saldo = 0;
		File sal = new File(id+"saldo.txt");
		File mov = new File(id+"movimientos.txt");
		File tar = new File(id+"tarjetas.txt");
		File dirTar = new File(id+"tarjetas/");
		dirTar.mkdir();
		try{
			PrintWriter p = new PrintWriter(sal);
			p.print("0");
			p.close();
			
			PrintWriter p2 = new PrintWriter(mov);
			p2.print("");
			p2.close();
			
			PrintWriter p3 = new PrintWriter(tar);
			p3.print("");
			p3.close();
		}catch(IOException e){}
		
	}
	public Cuenta(usuario user,String name, double s){
		this.id= user.nombre+"/"+name+"/";
		File f = new File (this.id);
		f.mkdir();
		saldo = s;
		File sal = new File(id+"saldo.txt");
		File mov = new File(id+"movimientos.txt");
		File tar = new File(id+"tarjetas.txt");
		File dirTar = new File(id+"tarjetas/");
		dirTar.mkdir();
		try{
			PrintWriter p = new PrintWriter(sal);
			p.print(s);
			p.close();
			
			PrintWriter p2 = new PrintWriter(mov);
			p2.println("Saldo inicial de la cuenta "+saldo+" €");
			p2.close();
			
			PrintWriter p3 = new PrintWriter(tar);
			p3.print("");
			p3.close();
		}catch(IOException e){}
	}
	public void ingresar(double s,String concept){
        if(concept.equals("")) concept = "Ingreso desde cajero automatico";
        this.saldo+=s;
        File f = new File(id+"saldo.txt");
        File f2 = new File(id+"movimientos.txt");
        try{

            PrintWriter p = new PrintWriter(f);
            p.println(saldo);
            p.close();

            Scanner t = new Scanner (f2);
            String str = "";
            while(t.hasNextLine()){
                str += t.nextLine()+"\n";
            }
            t.close();
            str+="INGRESO: "+concept + " " + s+" €";
            p = new PrintWriter(f2);
            t= new Scanner(str);
            while(t.hasNextLine()){
                p.println(t.nextLine());
            }
            p.close();
            t.close();
        }catch(IOException e){}
    }	
	public void retirar(double s, String concept){
	if(s<= this.saldo){	 
		if(concept.equals("")) concept = "Extracción desde cajero automatico";
	        this.saldo-=s;
	        File f = new File(id+"saldo.txt");
	        File f2 = new File(id+"movimientos.txt");
	        try{

	            PrintWriter p = new PrintWriter(f);
	            p.println(saldo);
	            p.close();

	            Scanner t = new Scanner (f2);
	            String str = "";
	            while(t.hasNextLine()){
	                str += t.nextLine()+"\n";
	            }
	            str+="EXTRACCION: "+concept + " " + s+" €";
	            p = new PrintWriter(f2);
	            t= new Scanner(str);
	            while(t.hasNextLine()){
	                p.println(t.nextLine());
	            }
	            p.close();
	        }catch(IOException e){}
	}
	}
	public tarjeta crearTarjetaVacia(Cuenta c,String name){
		
		try{ 
		File f = new File(id+"tarjetas.txt");
		Scanner t = new Scanner (f);
        String str = "";
        while(t.hasNextLine()){
            str += t.nextLine()+"\n";
        }
        t.close();
        str+=name+"\n";
        PrintWriter p = new PrintWriter(f);
        t= new Scanner(str);
        while(t.hasNextLine()){
            p.println(t.nextLine());
        }
        p.close();
        tarjeta ta = new tarjeta(c,name);
        return ta;
		}catch(IOException e){return null;}
		
	}
	public tarjeta crearTarjeta(Cuenta c, String name,double s){
		
		try{ 
		File f = new File(id+"tarjetas.txt");
		Scanner t = new Scanner (f);
        String str = "";
        while(t.hasNextLine()){
            str += t.nextLine()+"\n";
        }
        t.close();
        str+=name+"\n";
        PrintWriter p = new PrintWriter(f);
        t= new Scanner(str);
        while(t.hasNextLine()){
            p.println(t.nextLine());
        }
        p.close();
        
        tarjeta ta = new tarjeta(c,name,s);
        this.retirar(s,"Carga inicial tarjeta virtual");
        return ta;
			
		}catch(IOException e){return null;}
		
	}
}
