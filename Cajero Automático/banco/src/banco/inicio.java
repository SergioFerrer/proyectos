package banco;
import java.io.File;
import java.util.Scanner;

//import com.sun.java_cup.internal.runtime.Scanner;
import com.trolltech.qt.gui.QApplication;
import com.trolltech.qt.gui.QDialog;
import com.trolltech.qt.gui.QWidget;

public class inicio extends QDialog {

    Ui_inicio ui = new Ui_inicio();
    registro ventana;
    sesionUsuario ventana2;

    public static void main(String[] args) {
        QApplication.initialize(args);

        inicio testinicio = new inicio(null);
        testinicio.show();

        QApplication.exec();
    }

    public inicio() {
        ui.setupUi(this);
    }

    public inicio(QWidget parent) {
        super(parent);
        ui.setupUi(this);

        
        ui.registrarse.clicked.connect(this,"registrarse()");
        ui.acceder.clicked.connect(this,"acceder()");
    }
    public void registrarse(){
    	
    	if(ventana==null){
    		ventana = new registro(null);
    		ventana.ui.aceptar.clicked.connect(this,"altaUsuario()");
    		ventana.ui.cerrar.clicked.connect(this,"cerrarRegistro()");
    	}
    	ventana.exec();
    }
    public void altaUsuario(){
    	String nombre = ventana.ui.ln.text();
    	String pass = ventana.ui.lc.text();
    	String pass2 = ventana.ui.lc2.text();
    	if(nombre.equals("")||pass.equals("")||pass2.equals("")){
    		ventana.ui.etiquetaError.setText("POR FAVOR, RELLENA TODOS LOS CAMPOS");
    	}else if(!pass.equals(pass2)){
    		ventana.ui.etiquetaError.setText("LAS CONTRASEŅAS NO COINCIDEN");
    	}else{
    			nombre = nombre.toLowerCase();
    			File f = new File("cuentas/"+nombre+"/pass.txt");
    			boolean existe = funcionesAuxiliares.existe(f);
    			if(existe){
    				ventana.ui.etiquetaError.setText("NOMBRE DE USUARIO NO DISPONIBLE");
    			}else{
    				usuario user = new usuario(nombre,pass);
    				nombre="cuentas/"+nombre+"/";
    				ventana2 = new sesionUsuario(null,nombre);
    				ventana2.show();this.close();
    				cerrarRegistro();
    			}
    	}
    	
    }
    public void cerrarRegistro(){
    	ventana.ui.ln.setText("");
    	ventana.ui.lc.setText("");
    	ventana.ui.lc2.setText("");
    	ventana.ui.etiquetaError.setText("");
    	ventana.close();
    }
    public void acceder(){
    	String contra = ui.lc.text();
    	String nombre = ui.ln.text();
    	nombre = nombre.toLowerCase();
    	String nombre2 = "cuentas/"+nombre+"/pass.txt";
    	if(funcionesAuxiliares.todoBien(nombre2, contra)){
    		if(ventana2==null){
    			nombre = "cuentas/"+nombre+"/";
    			ventana2= new sesionUsuario(null,nombre);
    		}
    		ventana2.show();
			this.close();
    	}else{
    		ui.etiquetaError.setText("USUARIO O CONTRASEŅA INCORRECTOS");
    		ui.lc.setText("");
    	}
    }
}
