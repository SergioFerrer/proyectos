package banco;

//import com.sun.java_cup.internal.runtime.Scanner;
import com.trolltech.qt.gui.*;

import java.util.*;
import java.io.*;
public class sesionUsuario extends QMainWindow {

    Ui_sesionUsuario ui = new Ui_sesionUsuario();
    String nombre;
    usuario user;
    inicio volver;
    dialogoCrearCuenta dcc;
    dialogoCrearTarjeta dct;
    dialogoExtraer de;
    dialogoIngreso di;
    dialogoIngresoTarjeta dit;
    dialogoSacarTarjeta dst;
    dialogoTransferencia dt;
    public static void main(String[] args) {
        QApplication.initialize(args);

        sesionUsuario testsesionUsuario = new sesionUsuario(null,"");
        testsesionUsuario.show();

        QApplication.exec();
    }

    public sesionUsuario() {
        ui.setupUi(this);
    }

    public sesionUsuario(QWidget parent,String name) {
        super(parent);
        ui.setupUi(this);
        this.nombre = name;
        this.user = new usuario(name);
        
        ui.botonCargarTarjeta.clicked.connect(this,"vCargarTarjeta()");
        ui.botonCrearCuenta.clicked.connect(this,"vCrearCuenta()");
        ui.botonCrearTarjeta.clicked.connect(this,"vCrearTarjeta()");
        ui.botonDescargarTarjeta.clicked.connect(this,"vDescargarTarjeta()");
        ui.botonIngresar.clicked.connect(this,"vIngresar()");
        ui.botonSacar.clicked.connect(this,"vSacar()");
        ui.botonSalir.clicked.connect(this,"salir()");
        ui.botonTransferencia.clicked.connect(this,"vTransferencia()");
        ui.cajaCuentas.itemPressed.connect(this,"selecCuenta()");
        ui.cajaCuentas.currentItemChanged.connect(this,"selecCuenta()");
        ui.cajaTarjetas.itemSelectionChanged.connect(this,"selecTar()");

        
       iniciar();
        
        
        
    }
    public void iniciar(){
    	ui.cajaCuentas.clear();
    	ui.caja.setText("");
    	File f = new File(nombre+"/cuentas.txt");
         try{
         	Scanner c = new Scanner(f);
         	
         	while(c.hasNextLine()){
         		ui.cajaCuentas.setEnabled(true);
         		String str = c.nextLine();
         		ui.cajaCuentas.addItem(str);
         	}
      
   
         	c.close();
         	
         }catch(Exception e){}
         
    }
    
    public void selecTar(){
    	String cuenta = ui.cajaCuentas.currentItem().text();
    	String tar = ui.cajaTarjetas.currentItem().text();
    	File f = new File(nombre+cuenta+"/tarjetas/"+tar+"/saldo.txt");
    
    	File f2 = new File(nombre+cuenta+"/tarjetas/"+tar+"/ultimosMovimientos.txt");
    	
    	try{
    		Scanner t = new Scanner(f);
    		ui.caja.setText("##SALDO ACTUAL##\n"+ t.nextLine()+"€\n");
    		t.close();
    		t = new Scanner(f2);
    		ui.caja.append("##ULTIMOS MOVIMIENTOS##\n");
    		while(t.hasNextLine()){
    			ui.caja.append(t.nextLine());
    		}
    		t.close();
    	}catch(IOException e){}
    }
    public void selecCuenta(){
    	
    	ui.cajaTarjetas.clear();
    	ui.cajaTarjetas.setEnabled(false);
    	String cuenta =  ui.cajaCuentas.currentItem().text();
    	File f = new File(nombre+cuenta+"/tarjetas.txt");
    	File f2 = new File(nombre+cuenta+"/saldo.txt");

    	File f3 = new File(nombre+cuenta+"/movimientos.txt");
    	try{
    		Scanner t = new Scanner(f);
    		
    		while(t.hasNextLine()){
    			ui.cajaTarjetas.setEnabled(true);
    			ui.cajaTarjetas.addItem(t.nextLine());
    		}
    		t.close();
    		t= new Scanner(f2);
    		ui.caja.setText("##SALDO ACTUAL##\n"+ t.nextLine()+"€\n");
    		t.close();
    		ui.caja.append("##ULTIMOS MOVIMIENTOS##\n");
    		t=new Scanner (f3);
    		while(t.hasNextLine()){
    			ui.caja.append(t.nextLine());
    		}
    	}catch(Exception e){
    	}
    	
    	
    }
    public void vCargarTarjeta(){
    	int contador = 0;
    	dit = new dialogoIngresoTarjeta(null);
    	try{
        	File f = new File (nombre+"cuentas.txt");
        
        	Scanner t = new Scanner (f);
        	if(!t.hasNextLine()){
        	
        	}
        	while(t.hasNextLine()){
        		String tarjetas = nombre+t.nextLine()+"/tarjetas.txt";
        	
        		File f2 = new File(tarjetas);
        		Scanner t2 = new Scanner(f2);
        		if(t2.hasNextLine()==false){
        		
        		}
        		while(t2.hasNextLine()){
        			dit.ui.comboBox.addItem(t2.nextLine());
        			contador++;
        		}
        		t2.close();
        	}
        	t.close();
        	if(contador==0){
        		ui.caja.setText("ERROR: No tiene tarjetas creadas");
        		
        	}
        	
        }catch(Exception e){}
    
    	if(contador != 0)dit.show();
    	//hasta aqui para cargar el comboBOX y controlar errores
    	dit.ui.pushButton_2.clicked.connect(this,"sdit()");
    	dit.ui.pushButton.clicked.connect(this,"fit()");
    
    
    	
    	
    }
    public void fit(){
    	String nombTar = dit.ui.comboBox.currentText();
    	try{
        	File f = new File (nombre+"cuentas.txt");
        	String cuentaAct="";
        	Scanner t = new Scanner (f);
        	boolean seguir = true;
        	
        	while(t.hasNextLine()&&seguir){
        		cuentaAct = t.nextLine();
        		String tarjetas = nombre+cuentaAct+"/tarjetas.txt";
        		
        		File f2 = new File(tarjetas);
        		Scanner t2 = new Scanner(f2);
        		if(t2.hasNextLine()==false){
        		
        		}
        		while(t2.hasNextLine()&&seguir){
        			if(t2.nextLine().equals(nombTar)){
        				//ya hemos encontrado la cuenta
        				seguir = false;
        				
        			}
        			
        		}
        		t2.close();
        	}
        	t.close();
        	f = new File(nombre+cuentaAct+"/saldo.txt");
        	t= new Scanner(f);
        	Double saldo = Double.parseDouble(t.nextLine());
        	t.close();
        	Double ingreso = dit.ui.doubleSpinBox.value();
        	Cuenta c = new Cuenta(user,cuentaAct,saldo,true);
        	if(ingreso<saldo){
        		tarjeta j = new tarjeta(c,dit.ui.comboBox.currentText(),true);
        		j.cargar(ingreso, dit.ui.lc.text());
        		ui.caja.setText("Se ha cargado la tarjeta "+dit.ui.comboBox.currentText()+" desde la cuenta "+cuentaAct);
        		dit.close();
        		ui.cajaCuentas.setCurrentIndex(null);
        		ui.cajaTarjetas.setCurrentIndex(null);
        	}
        	else{
        		ui.caja.setText("##Saldo insuficiente##");
        		dit.close();
        	}
        }catch(Exception e){}
    
    }
    public void sdit(){
    	dit.close();
    }
    public void fbs(){
    	dcc.close();
    }
    public void fbcc(){
    	user.crearCuenta(user, dcc.ui.ln.text(), dcc.ui.ls.value());
    	iniciar();
    	dcc.close();
    }
    public void vCrearCuenta(){

    		dcc = new dialogoCrearCuenta(null);
    		dcc.show();
    		dcc.ui.bcc.clicked.connect(this,"fbcc()");
    		dcc.ui.bs.clicked.connect(this,"fbs()");
    		
    	
    }
    public void vCrearTarjeta(){

    		dct = new dialogoCrearTarjeta(null);
    		File f = new File(nombre+"/cuentas.txt");
            try{
            	Scanner c = new Scanner(f);
            	
            	while(c.hasNextLine()){
            		ui.cajaCuentas.setEnabled(true);
            		String str = c.nextLine();
            		dct.ui.comboBox.addItem(str);
            	}
            }catch(IOException e){}
    		dct.ui.bct.clicked.connect(this,"fct()");
    		dct.ui.bs.clicked.connect(this,"fsct()");
            dct.show();
    }
    public void fsct (){
    	dct.close();
    }
    public void fct(){
    	try{
    	File f2 = new File(nombre+dct.ui.comboBox.currentText()+"/saldo.txt");
    	double saldo=0;
    	try{
    	Scanner t = new Scanner(f2);
    	saldo = Double.parseDouble(t.nextLine());
    	t.close();
    	}catch(Exception e){}
    
    	Cuenta c = new Cuenta(user,dct.ui.comboBox.currentText(),saldo,true);

    	double saldoVentana = dct.ui.ls.value();
    	if(saldo>saldoVentana) {
    		
    		c.crearTarjeta(c, dct.ui.ln.text(), saldoVentana);
    		ui.caja.setText("Tarjeta creada con un saldo inicial de "+saldoVentana+"€");
    		ui.cajaCuentas.setCurrentItem(null);
    	}
    	else{
    		ui.caja.setText("##SALDO INSUFICIENTE##");
    	}
    	dct.close();
    	}catch(Exception e){}
    }
    public void vDescargarTarjeta(){
    
    	int contador = 0;
    	dst = new dialogoSacarTarjeta(null);
    	try{
        	File f = new File (nombre+"cuentas.txt");
        
        	Scanner t = new Scanner (f);
        	if(!t.hasNextLine()){
        	
        	}
        	while(t.hasNextLine()){
        		String tarjetas = nombre+t.nextLine()+"/tarjetas.txt";
        	
        		File f2 = new File(tarjetas);
        		Scanner t2 = new Scanner(f2);
        		
        		while(t2.hasNextLine()){
        			dst.ui.lt.addItem(t2.nextLine());
        			contador++;
        		}
        		t2.close();
        	}
        	t.close();
        	if(contador==0){
        		ui.caja.setText("ERROR: No tiene tarjetas creadas");
        		
        	}
        	
        }catch(Exception e){}
    
    	if(contador != 0)dst.show();
    	//hasta aqui para cargar el comboBOX y controlar errores
    	dst.ui.bs.clicked.connect(this,"sdst()");
    	dst.ui.bd.clicked.connect(this,"fdt()");
    	
    	dst.show();
    }
    public void fdt(){
    	String nombTar = dst.ui.lt.currentText();
    	try{
        	File f = new File (nombre+"cuentas.txt");
        	String cuentaAct="";
        	Scanner t = new Scanner (f);
        	boolean seguir = true;
        	
        	while(t.hasNextLine()&&seguir){
        		cuentaAct = t.nextLine();
        		String tarjetas = nombre+cuentaAct+"/tarjetas.txt";
        		
        		File f2 = new File(tarjetas);
        		Scanner t2 = new Scanner(f2);
        		if(t2.hasNextLine()==false){
        		
        		}
        		while(t2.hasNextLine()&&seguir){
        			if(t2.nextLine().equals(nombTar)){
        				//ya hemos encontrado la cuenta
        				seguir = false;
        				
        			}
        			
        		}
        		t2.close();
        	}
        	t.close();
        	f = new File(nombre+cuentaAct+"/saldo.txt");
        	t= new Scanner(f);
        	Double saldo = Double.parseDouble(t.nextLine());
        	t.close();
        	Double ingreso = dst.ui.ls.value();
        	Cuenta c = new Cuenta(user,cuentaAct,saldo,true);
        	f = new File(nombre+cuentaAct+"/tarjetas/"+dst.ui.lt.currentText()+"/saldo.txt");
        	t = new Scanner(f);
        	double s = Double.parseDouble(t.nextLine());
        	t.close();
        	tarjeta j = new tarjeta(c,dst.ui.lt.currentText(),s,true);
        	if(ingreso<j.saldo){
        		
        		j.descargar(ingreso, "");
        		ui.caja.setText("Se ha ingresado la descarga en la cuenta "+cuentaAct);
        		dit.close();
        	}
        	else{
        		ui.caja.setText("##Saldo insuficiente##");
        		dst.close();
        	}
        }catch(Exception e){}
    	dst.close();
    	ui.cajaTarjetas.setCurrentItem(null);
    } 
    public void sdst(){
    	dst.close();
    }
    public void vIngresar(){
    	di = new dialogoIngreso(null);
		File f = new File(nombre+"/cuentas.txt");
        try{
        	Scanner c = new Scanner(f);
        	
        	while(c.hasNextLine()){
        	di.ui.lcu.addItem(c.nextLine());
  
        	}
        	c.close();
        }catch(IOException e){}
    	
      	
    	di.show();
    	di.ui.bi.clicked.connect(this,"botonIngresar()");
    	di.ui.bs.clicked.connect(this,"fsi()");
    	
    }
    public void fsi(){
    	di.close();
    }
    public void botonIngresar(){
    	String cuenta = di.ui.lcu.currentText();
    	Double ingreso = di.ui.ld.value();
    	String concepto = di.ui.lc.text();
    	Double saldo = 0.0;
    	try{
    		File f2 = new File(nombre+cuenta+"/saldo.txt");
    		Scanner t = new Scanner(f2);
    		saldo = Double.parseDouble(t.nextLine());
    		t.close();
    	}catch(Exception e){}
    	Cuenta c = new Cuenta(user,cuenta,saldo,true);
    	c.ingresar(ingreso, concepto);
    	di.close();
    	ui.caja.setText("Se ha realizado un ingreso de "+ingreso+"€ en la cuenta "+cuenta);
    			
    }
    public void vSacar(){
    	de=new dialogoExtraer(null);
    	File f = new File(nombre+"/cuentas.txt");
        try{
        	Scanner c = new Scanner(f);
        	
        	while(c.hasNextLine()){
        	de.ui.lcu.addItem(c.nextLine());
  
        	}
        	c.close();
        }catch(IOException e){}
        de.show();
        de.ui.be.clicked.connect(this,"fe()");
        de.ui.bs.clicked.connect(this,"fse()");
    }
    public void fe(){
    	String cuenta = de.ui.lcu.currentText();
    	Double ingreso = de.ui.ls.value();
    	String concepto = de.ui.lc.text();
    	Double saldo = 0.0;
    	try{
    		File f2 = new File(nombre+cuenta+"/saldo.txt");
    		Scanner t = new Scanner(f2);
    		saldo = Double.parseDouble(t.nextLine());
    		t.close();
    	}catch(Exception e){}
    	Cuenta c = new Cuenta(user,cuenta,saldo,true);
    	if(ingreso<saldo){
    	c.retirar(ingreso, concepto);
    	de.close();
    	ui.caja.setText("Se ha realizado una extracción de "+ingreso+"€ en la cuenta "+cuenta);
    	}else{
    		ui.caja.setText("Saldo insuficiente");
    	}
    	de.close();
    	}
    public void fse(){
    	de.close();
    }
    
    public void vTransferencia(){
    	dt= new dialogoTransferencia(null);
    	File f = new File(nombre+"/cuentas.txt");
        try{
        	Scanner c = new Scanner(f);
        	String str;
        	while(c.hasNextLine()){
        		str = c.nextLine();
        		dt.ui.lce.addItem(str);
        		dt.ui.lci.addItem(str);
        	}
        	c.close();
        }catch(IOException e){}
    	dt.show();
    	dt.ui.bc.clicked.connect(this,"fsdt()");
    	dt.ui.bt.clicked.connect(this,"ft()");
    }
    public void ft(){
    	String c1 = dt.ui.lce.currentText();
    	String c2 = dt.ui.lci.currentText();
    	File f = new File(nombre+c1+"/saldo.txt");
    	File f2 = new File(nombre+c2+"/saldo.txt");
		
    	try{
    	Scanner t = new Scanner(f);
    	Double s1 = Double.parseDouble(t.nextLine());
    	t.close();
    	t= new Scanner(f2);
    	Double s2 = Double.parseDouble(t.nextLine());
    	t.close();
    	Cuenta cuentaExt = new Cuenta(user,c1,s1,true);
    	Cuenta cuentaIng= new Cuenta(user,c2,s2,true);
    	double saldoVentana = dt.ui.ls.value();
    	if(s1>saldoVentana){
    		cuentaExt.retirar(saldoVentana, "Transferencia hacia la cuenta "+c2);
    		cuentaIng.ingresar(saldoVentana, "Transferencia desde la cuenta "+c1);
    		ui.caja.setText("Transferencia realizada correctamente");
    		dt.close();
    	}else{
    		ui.caja.setText("##Saldo insuficiente##");
    		dt.close();
    	}
		}catch(Exception e){}
    }
    public void fsdt(){
    	dt.close();
    }
    public void salir(){
    	close();
    	volver = new inicio(null);
    	volver.show();
    	
    }
}
