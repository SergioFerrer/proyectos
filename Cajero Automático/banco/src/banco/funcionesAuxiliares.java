package banco;
import java.io.*;
import java.util.*;

public class funcionesAuxiliares{
	public static boolean existe(File f){
		try{
			Scanner tec = new Scanner(f);
			tec.close();
			return true;
		}catch(FileNotFoundException e){
			return false;
		}
		
	}
	public static boolean todoBien(String ruta, String pass){
		File f = new File(ruta);
		try{
			Scanner t = new Scanner(f);
			String passReal = t.nextLine();
			t.close();
			return passReal.equals(pass);
		}catch(IOException e){
			return false;
		}
	}
}
