package banco;

import com.trolltech.qt.gui.*;

public class dialogoIngreso extends QDialog {

    Ui_dialogoIngreso ui = new Ui_dialogoIngreso();

    public static void main(String[] args) {
        QApplication.initialize(args);

        dialogoIngreso testdialogoIngreso = new dialogoIngreso();
        testdialogoIngreso.show();

        QApplication.exec();
    }

    public dialogoIngreso() {
        ui.setupUi(this);
    }

    public dialogoIngreso(QWidget parent) {
        super(parent);
        ui.setupUi(this);
    }
}
