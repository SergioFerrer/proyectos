package banco;

import com.trolltech.qt.gui.*;

public class dialogoSacarTarjeta extends QDialog {

    Ui_dialogoSacarTarjeta ui = new Ui_dialogoSacarTarjeta();

    public static void main(String[] args) {
        QApplication.initialize(args);

        dialogoSacarTarjeta testdialogoSacarTarjeta = new dialogoSacarTarjeta();
        testdialogoSacarTarjeta.show();

        QApplication.exec();
    }

    public dialogoSacarTarjeta() {
        ui.setupUi(this);
    }

    public dialogoSacarTarjeta(QWidget parent) {
        super(parent);
        ui.setupUi(this);
    }
}
