package banco;

import com.trolltech.qt.gui.*;

public class dialogoTransferencia extends QDialog {

    Ui_dialogoTransferencia ui = new Ui_dialogoTransferencia();

    public static void main(String[] args) {
        QApplication.initialize(args);

        dialogoTransferencia testdialogoTransferencia = new dialogoTransferencia();
        testdialogoTransferencia.show();

        QApplication.exec();
    }

    public dialogoTransferencia() {
        ui.setupUi(this);
    }

    public dialogoTransferencia(QWidget parent) {
        super(parent);
        ui.setupUi(this);
    }
}
