package Data;

import java.util.ArrayList;

import Logica.Planta;
import Logica.Tipo;
import Logica.Venta;
import Logica.VentaTabla;

public class DAL {
 
	private static DAL dal;
	private DAOPlanta daoPlanta;
	private DAOVenta daoVenta;
	private DAOTipo daoTipo;

	private DAL(){
		daoPlanta=new DAOPlanta();
		daoVenta=new DAOVenta();
		daoTipo=new DAOTipo();
	}
	
	public static DAL dameDAL(){
		if(dal==null) dal = new DAL();
		return dal;
	}

	public ArrayList<Tipo> getTypes(){
		return daoTipo.getAll();
	}
	
	public ArrayList<Planta> getPlants(){
		return daoPlanta.getAll();
	}
	
	public ArrayList<Planta> getPlantsByType(String t){
		return daoPlanta.getType(t);
	}
	
	public void anyadeTipo(Tipo t){
		daoTipo.add(t);
	}
	
	public void cambiarTipos(String old, String nw){
		daoTipo.cambiarTipos(old,nw);
		daoPlanta.cambiarTipos(old,nw);
	}
	
	public void deleteType(String s){
		daoTipo.delete(s);
		daoPlanta.deleteType(s);
	}
	
	public int getMaxCode(){
		return daoPlanta.maxCod();
	}
	
	public void anyadirPlanta(Planta p){
		daoPlanta.add(p);
	}
	
	public void deletePlanta(Planta p){
		daoPlanta.deletePlant(p.getCod());
	}
	
	public boolean isCodeValid(int n){
		return daoPlanta.isCodeValid(n);
	}
	
	public int isCantidadValid(int n, int cod){
		return daoPlanta.isCantidadValid(n,cod);
	}
	
	public void anyadirUnidades(Venta v){
		daoPlanta.anyadirUnidades(v);
		daoVenta.add(v);
	}
	
	public void sacarUnidades(Venta v){
		daoPlanta.sacarUnidades(v);
		daoVenta.add(v);
	}
	
	public Planta getPlant(int code){
		return daoPlanta.getPlanta(code);
	}
	public ArrayList<Venta> getVentas(){
		return daoVenta.getAll();
	}
	
	public void removeVenta(VentaTabla v){
		daoVenta.removeVenta(v);
	}
	
	public void borrarTodasVentas(){
		daoVenta.deleteAll();
	}
}
