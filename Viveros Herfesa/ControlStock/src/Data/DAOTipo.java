package Data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Logica.Tipo;

public class DAOTipo {

	public void add(Tipo t){
		Connection con = Conector.dameConector().getConnect();
		try{
		PreparedStatement st = con.prepareStatement("insert into tipos values ('"+t.getNombre()+"')");
       st.execute();
       con.close();
		
		}catch(SQLException e){
			System.out.println("Error en addTipo");
		}
	}
	public ArrayList<Tipo> getAll(){
		ArrayList<Tipo> res = new ArrayList<Tipo>();
		Connection con = Conector.dameConector().getConnect();
		try{
			PreparedStatement st = con.prepareStatement("select * from tipos");
	        ResultSet result = st.executeQuery();
	        while(result.next()){
	        	res.add(new Tipo(result.getString("Nombre")));
	        	}
	        con.close();
		}catch(SQLException e){
			System.out.println("Error recuperando los tipos");
		}
		
		return res;
		
	}
	
	public void delete(String s){
		Connection con = Conector.dameConector().getConnect();
		try{
		PreparedStatement st = con.prepareStatement("delete from tipos where nombre='"+s
				+"'");
       st.execute();
       con.close();
		
		}catch(SQLException e){
			System.out.println("Error borrando tipo");
		}
		
	}
	
	public void cambiarTipos(String old, String nw){
		Connection con = Conector.dameConector().getConnect();
		try{
			PreparedStatement st = con
					.prepareStatement("UPDATE tipos SET nombre='" + nw
							+ "' where nombre='" + old + "'");
			st.execute();
			con.close();

		}catch(SQLException e){
			System.out.println("Error editantos tipo en TIPO");
		}
		
		
	}
}
