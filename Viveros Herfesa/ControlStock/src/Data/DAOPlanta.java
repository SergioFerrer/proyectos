package Data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Logica.Planta;
import Logica.Tipo;
import Logica.Venta;

public class DAOPlanta {

	public void add(Planta p) {
		Connection con = Conector.dameConector().getConnect();
		try {
			String t = "insert into plantas values ("
					+ p.getCod() + "," + p.getTam() + ",'"
					+ p.getVariedad() + "'," + p.getCantidad() + ",'"
					+ p.getComentario() + "','"
					+ p.getTipo().getNombre() + "')";
			PreparedStatement st = con.prepareStatement(t);
			st.execute();
			con.close();

		} catch (SQLException e) {
			System.out.println("Error en addPlanta");
		}
	}
	
	public int maxCod(){
		Connection con = Conector.dameConector().getConnect();
		int cod = -1;
		try{
			PreparedStatement st = con.prepareStatement("select MAX(COD) COD from Plantas");
	        ResultSet result = st.executeQuery();
	        cod = result.getInt("COD");
	        con.close();
		}catch(SQLException e){
			System.out.println("Error recuperando macCod");
		}
		
		return cod;
		}
	
	public boolean isCodeValid(int n){
		Connection con = Conector.dameConector().getConnect();
		ResultSet result;
		try{
			PreparedStatement st = con.prepareStatement("select COD from Plantas where COD="+n);
	        result = st.executeQuery();
	        con.close();
	        if(result.next()) 
	        	return true;
	        else
	        	return false;
		}catch(SQLException e){
			System.out.println("Error comprobando codeValid");
			return false;
		}
	}
	
	public int isCantidadValid(int n,int cod){
		Connection con = Conector.dameConector().getConnect();
		ResultSet result;
		try{
			PreparedStatement st = con.prepareStatement("select Cantidad from Plantas where COD="+cod);
	        result = st.executeQuery();
	        int realCantidad = result.getInt("Cantidad");
	        con.close();
	        if(realCantidad>=n) 
	        	return -1;
	        else
	        	return realCantidad;
		}catch(SQLException e){
			System.out.println("Error comprobando cantidadValid");
			return -2;
		}
	}
	public void deleteType(String s){
		Connection con = Conector.dameConector().getConnect();
		try{
		PreparedStatement st = con.prepareStatement("delete from plantas where tipo='"+s
				+"'");
       st.execute();
       con.close();
		
		}catch(SQLException e){
			System.out.println("Error borrando plantas por tipo");
		}
	}
	
	public ArrayList<Planta> getType(String s){
		ArrayList<Planta> res = new ArrayList<Planta>();
		Connection con = Conector.dameConector().getConnect();
		try{
			PreparedStatement st = con.prepareStatement("select * from plantas where tipo ='"+s+"'");
	        ResultSet result = st.executeQuery();
	        Planta temp = null;
	        while(result.next()){
	        	temp = new Planta();
	        	temp.setCod(result.getInt("COD"));
	        	temp.setTam(result.getInt("Tama�o"));
	        	temp.setVariedad(result.getString("Variedad"));
	        	temp.setCantidad(result.getInt("Cantidad"));
	        	temp.setComentario(result.getString("Comentario"));
	        	temp.setTipo(new Tipo(result.getString("Tipo")));
	        	res.add(temp);
	        	temp=null;
	        }
	        con.close();
		}catch(SQLException e){
			System.out.println("Error recuperando plantas por tipos");
		}
		return res;
		
	}
	
	public Planta getPlanta(int code){
		Connection con = Conector.dameConector().getConnect();
		 Planta temp = null;
		try{
			PreparedStatement st = con.prepareStatement("select * from plantas where cod ="+code);
	        ResultSet result = st.executeQuery();
	        temp = null;
	        if(result.next()){
	        	temp = new Planta();
	        	temp.setCod(result.getInt("COD"));
	        	temp.setTam(result.getInt("Tama�o"));
	        	temp.setVariedad(result.getString("Variedad"));
	        	temp.setCantidad(result.getInt("Cantidad"));
	        	temp.setComentario(result.getString("Comentario"));
	        	temp.setTipo(new Tipo(result.getString("Tipo")));
	        }
	        con.close();
		}catch(SQLException e){
			System.out.println("Error recuperando plantas por tipos");
		}
		return temp;
		
	}
	
	public void deletePlant(int cod){
		Connection con = Conector.dameConector().getConnect();
		try{
		PreparedStatement st = con.prepareStatement("delete from plantas where COD="+cod);
       st.execute();
       con.close();
		
		}catch(SQLException e){
			System.out.println("Error borrando planta por tipo");
		}
		
	}
	
	public ArrayList<Planta> getAll(){
		ArrayList<Planta> res = new ArrayList();
		Connection con = Conector.dameConector().getConnect();
		try{
			PreparedStatement st = con.prepareStatement("select * from plantas");
	        ResultSet result = st.executeQuery();
	        Planta temp = null;
	        while(result.next()){
	        	temp = new Planta();
	        	temp.setCod(result.getInt("COD"));
	        	temp.setTam(result.getInt("Tama�o"));
	        	temp.setVariedad(result.getString("Variedad"));
	        	temp.setCantidad(result.getInt("Cantidad"));
	        	temp.setComentario(result.getString("Comentario"));
	        	temp.setTipo(new Tipo(result.getString("Tipo")));
	        	res.add(temp);
	        	temp=null;
	        }
	        con.close();
		}catch(SQLException e){
			System.out.println("Error recuperando todas las plantas");
		}
		return res;
		
	}
	
	public void cambiarTipos(String old, String nw){
		Connection con = Conector.dameConector().getConnect();
		try{
			PreparedStatement st = con
					.prepareStatement("UPDATE plantas SET Tipo='" + nw
							+ "' where tipo='" + old + "'");
			st.execute();
			con.close();

		}catch(SQLException e){
			System.out.println("Error editantos tipo en PLANTA");
		}
	}
	public void anyadirUnidades(Venta v){
		Connection con = Conector.dameConector().getConnect();
		try{
			PreparedStatement st = con
					.prepareStatement("UPDATE plantas SET Cantidad=(Select cantidad from plantas where cod="
							+ v.getCodPlanta()
							+ ")+"
							+ v.getUnidades()
							+ " where COD=" + v.getCodPlanta());
							
			st.execute();
			con.close();

		}catch(SQLException e){
			System.out.println("Error introduciendo planta en anyadirUnidades.DAOPlanta");
		}
	}
	
	public void sacarUnidades(Venta v){
		Connection con = Conector.dameConector().getConnect();
		try{
			PreparedStatement st = con
					.prepareStatement("UPDATE plantas SET Cantidad=(Select cantidad from plantas where cod="
							+ v.getCodPlanta()
							+ ")-"
							+ v.getUnidades()
							+ " where COD=" + v.getCodPlanta());
							
			st.execute();
			con.close();

		}catch(SQLException e){
			System.out.println("Error introduciendo planta en anyadirUnidades.DAOPlanta");
		}
	}
}
