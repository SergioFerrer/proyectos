package Logica;

public class Venta {

	private String es;
	private String concepto;
	private String fecha;
	private int unidades;
	private String comentario;
	private int codPlanta;
	public String getEs() {
		return es;
	}
	public void setEs(String es) {
		this.es = es;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public int getUnidades() {
		return unidades;
	}
	public void setUnidades(int unidades) {
		this.unidades = unidades;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public int getCodPlanta() {
		return codPlanta;
	}
	public void setCodPlanta(int codPlanta) {
		this.codPlanta = codPlanta;
	}
}
