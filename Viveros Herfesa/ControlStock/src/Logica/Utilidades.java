package Logica;

import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Utilidades<T> {

	public ObservableList<T> toObservableList(ArrayList<T> array){
		if(array.size() <=0) return null;
		else{
			ObservableList<T> obser = FXCollections.observableArrayList(
					array.get(0)
					);
			for(int i =1; i<array.size();i++){
				obser.add(array.get(i));
			}
			return obser;
		}
	}
}
