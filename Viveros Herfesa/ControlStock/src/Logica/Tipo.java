package Logica;

public class Tipo {
	private String nombre;

	public Tipo(String s){
		nombre=s;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String toString(){
		return nombre;
	}
}
