package Logica;

import java.util.ArrayList;

public class VentaTabla {
 private String es;
 private String concepto;
 private String fecha;
 private String tipo;
 private String variedad;
 private String tamanyo;
 private String cantidad;
 private String comentario;
 private String codePlant;
 
 public VentaTabla (Venta v){
	 es=v.getEs();
	 concepto=v.getConcepto();
	 fecha=v.getFecha();
	 cantidad=v.getUnidades()+"";
	 comentario=v.getComentario();
	 codePlant=v.getCodPlanta()+"";
	 OC oc= OC.DameOC();
	 Planta p = oc.getPlant(v.getCodPlanta());
	 tipo=p.getTipo().getNombre();
	 variedad= p.getVariedad();
	 tamanyo = p.getTam()+"";
	 
 }
 
 public static ArrayList<VentaTabla> toTablaList(ArrayList<Venta> list){
	 ArrayList<VentaTabla> res = new ArrayList<VentaTabla>();
	 for(int i = 0; i <list.size();i++){
		 res.add(new VentaTabla(list.get(i)));
	 }
	 return res;
 }
 
 public String getCodePlant(){
	 return codePlant;
 }

public String getEs() {
	return es;
}

public String getConcepto() {
	return concepto;
}

public String getFecha() {
	return fecha;
}

public String getTipo() {
	return tipo;
}

public String getVariedad() {
	return variedad;
}

public String getTamanyo() {
	return tamanyo;
}

public String getCantidad() {
	return cantidad;
}

public String getComentario() {
	return comentario;
}
	
}
