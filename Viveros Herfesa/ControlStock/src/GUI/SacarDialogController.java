package GUI;

import java.util.Calendar;
import java.util.Optional;

import Logica.OC;
import Logica.Venta;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
public class SacarDialogController {
	private Stage dialogStage;
	@FXML
	private RadioButton bajasRadio;
	@FXML
	private RadioButton compraRadio;
	@FXML
	private RadioButton clienteRadio;
	@FXML
	private RadioButton otrosRadio;
	@FXML
	private RadioButton otrosAnyadirRadio;
	@FXML
	private RadioButton otrosSacarRadio;
	@FXML
	private TextField clienteField;
	@FXML
	private TextField anyadirField;
	@FXML
	private TextField sacarField;
	@FXML
	private TextField codeField;
	@FXML
	private TextField cantidadField;
	@FXML
	private TextArea coment;
	private Venta venta;
	private boolean okPulsed;
	
	@FXML
	private void bajasRadioPulsed(){
		otrosAnyadirRadio.setDisable(true);
		otrosSacarRadio.setDisable(true);
		clienteField.setDisable(true);
		anyadirField.setDisable(true);
		sacarField.setDisable(true);
	}
	@FXML
	private void compraRadioPulsed(){
		otrosAnyadirRadio.setDisable(true);
		otrosSacarRadio.setDisable(true);
		clienteField.setDisable(true);
		anyadirField.setDisable(true);
		sacarField.setDisable(true);
	}
	@FXML
	private void clienteRadioPulsed(){
		otrosAnyadirRadio.setDisable(true);
		otrosSacarRadio.setDisable(true);
		clienteField.setDisable(false);
		anyadirField.setDisable(true);
		sacarField.setDisable(true);
	}
	@FXML
	private void anyadirRadioPulsed(){
		anyadirField.setDisable(false);
		sacarField.setDisable(true);
	}
	
	@FXML
	private void sacarPulsed(){
		anyadirField.setDisable(true);
		sacarField.setDisable(false);
	}
	
	@FXML
	private void otrosRadioPulsed(){
		otrosAnyadirRadio.setDisable(false);
		otrosSacarRadio.setDisable(false);
		clienteField.setDisable(true);
		
	}
	
	private boolean validate(){
		if(clienteRadio.isSelected() && clienteField.getText().equals("")){
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("ERROR.Debes poner el nombre del cliente");
			alert.showAndWait();
			return false;
		}else if(otrosRadio.isSelected() && otrosAnyadirRadio.isSelected() && anyadirField.getText().equals("")){
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("ERROR.Escribe en el campo de texto por qu� se van a a�adir plantas");
			alert.showAndWait();
			return false;
		}else if(otrosRadio.isSelected() && otrosSacarRadio.isSelected() && sacarField.getText().equals("")){
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("ERROR.Escribe en el campo de texto por qu� se van a sacar plantas");
			alert.showAndWait();
			return false;
		}
		try{
			Integer.parseInt(codeField.getText());
			Integer.parseInt(cantidadField.getText());
		}catch(Exception e){
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("ERROR.El c�digo y la cantidad deben ser un n�mero");
			alert.showAndWait();
			return false;
		}
		OC oc = OC.DameOC();
		if(!oc.isCodeValid(Integer.parseInt(codeField.getText()))){
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("ERROR.Ninguna planta contiene el c�digo especificado");
			alert.showAndWait();
			return false;
		}
		if(bajasRadio.isSelected() || clienteRadio.isSelected() || otrosSacarRadio.isSelected()){
			int real =oc.isCantidadValid(Integer.parseInt(cantidadField.getText()),Integer.parseInt(codeField.getText())); 
			if(real==-2){
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Error");
				alert.setHeaderText(null);

				alert.setContentText("ERROR.Cantidad erronea");
				alert.showAndWait();
				return false;
			}
			if(real>-1){
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Error");
				alert.setHeaderText(null);

				alert.setContentText("ERROR.No puedes sacar "+cantidadField.getText()+" porque solo tienes "+real+" unidades de esta planta");
				alert.showAndWait();
				return false;
			}
		}
		if(!bajasRadio.isSelected() && !clienteRadio.isSelected()&&!otrosRadio.isSelected() && !compraRadio.isSelected()){
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setHeaderText(null);

			alert.setContentText("ERROR.Tienes que seleccionar un motivo");
			alert.showAndWait();
			return false;
		}if(otrosRadio.isSelected()&& !otrosAnyadirRadio.isSelected() && !otrosSacarRadio.isSelected()){
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setHeaderText(null);

			alert.setContentText("ERROR.Tienes que seleccionar si vas a a�adir o sacar plantas");
			alert.showAndWait();
			return false;
		}
		return true;
	}
	public TextField getCodeField() {
		return codeField;
	}
	@FXML
	private void aceptarPulsado(){
		if(validate()){
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Confirmaci�n");
			alert.setHeaderText(null);
			alert.setContentText("Est�s apunto de modificar la cantidad de plantas.�Seguro que quieres hacerlo?");
			ButtonType buttonTypeOne = new ButtonType("S�");
			ButtonType buttonTypeTwo = new ButtonType("No");	
			alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == buttonTypeOne){
				Calendar c = Calendar.getInstance();
				String dia = Integer.toString(c.get(Calendar.DATE));
				String mes = Integer.toString(c.get(Calendar.MONTH)+1);
				String annio = Integer.toString(c.get(Calendar.YEAR));
				venta = new Venta();
				venta.setFecha(dia+"/"+mes+"/"+annio);
				venta.setCodPlanta(Integer.parseInt(codeField.getText()));
				venta.setUnidades(Integer.parseInt(cantidadField.getText()));
				venta.setComentario(coment.getText());
				if(bajasRadio.isSelected()){
					venta.setConcepto("Bajas");
					venta.setEs("S");
				}else if(compraRadio.isSelected()){
					venta.setConcepto("Compras");
					venta.setEs("E");
				}else if (clienteRadio.isSelected()){
					venta.setConcepto(clienteField.getText());
					venta.setEs("S");
				}else if(otrosRadio.isSelected()){
					if(otrosAnyadirRadio.isSelected()){
						venta.setConcepto(anyadirField.getText());
						venta.setEs("E");
					}else{
						venta.setConcepto(sacarField.getText());
						venta.setEs("S");
					}
				}
				
			}
		okPulsed=true;	
		dialogStage.close();
		
		}
	}
	
	@FXML
	private void cancelPulsed(){
		okPulsed=false;
		venta=null;
		dialogStage.close();
	}
	public Venta getVenta() {
		return venta;
	}
	public boolean isOkPulsed() {
		return okPulsed;
	}
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}
	
}
