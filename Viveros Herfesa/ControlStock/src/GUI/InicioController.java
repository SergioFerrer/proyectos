package GUI;

import java.io.IOException;

import Logica.Principal;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;

public class InicioController {

	Principal mainApp;

	public void setMainApp(Principal p){
		this.mainApp=p;

	}

	@FXML
	private void informesPulsado(){
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(InicioController.class.getResource("/GUI/Informes.fxml"));
			AnchorPane aux = (AnchorPane)loader.load();
			Scene s = new Scene(aux);
			mainApp.setScene(s);
			mainApp.setFull(true);
			//linkar el controlador
			InformesController controller = loader.getController();
			controller.setMainApp(mainApp);
			controller.init();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void gestionarPulsado(){
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(InicioController.class.getResource("/GUI/TiposView.fxml"));
			AnchorPane aux = (AnchorPane)loader.load();
			Scene s = new Scene(aux);
			mainApp.setScene(s);
			mainApp.setFull(true);
			//linkar el controlador
			TiposViewController controller = loader.getController();

			controller.setMainApp(mainApp);
			controller.init();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
