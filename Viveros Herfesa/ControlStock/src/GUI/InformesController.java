package GUI;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Screen;
import Logica.OC;
import Logica.Planta;
import Logica.Principal;
import Logica.Utilidades;
import Logica.Venta;
import Logica.VentaTabla;

public class InformesController {
	Principal mainApp;

	@FXML
	private ComboBox comboEs;

	@FXML
	private TextField tamanyoField;

	@FXML
    private TableView<VentaTabla> ventasTable;

	@FXML
    private TableColumn<VentaTabla,String>conceptoColumn;

    @FXML
    private TableColumn<VentaTabla,String> sizeColumn;

    @FXML
    private TableColumn<VentaTabla,String>variedadColumn;

    @FXML
    private TableColumn<VentaTabla,String> amountColumn;

    @FXML
    private TableColumn<VentaTabla,String> comentColumn;

    @FXML
    private TableColumn<VentaTabla,String> fechaColumn;

    @FXML
    private TableColumn<VentaTabla,String> esColumn;

    @FXML
    private TableColumn<VentaTabla,String> tipoColumn;

    @FXML
    private TextField conceptoField;

    @FXML
    private TextField fechaField;

    @FXML
    private TextField tipoField;

    @FXML
    private TextField variedadField;

    @FXML
    private TextField cantidadField;

    @FXML
    private Button borrarSeleccion;

    @FXML
    private TextField hastaField;


    public static String remove(String input) {
        // Cadena de caracteres original a sustituir.
        String original = "��������������u�������������������";
        // Cadena de caracteres ASCII que reemplazar�n los originales.
        String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
        String output = input;
        for (int i=0; i<original.length(); i++) {
            // Reemplazamos los caracteres especiales.
            output = output.replace(original.charAt(i), ascii.charAt(i));
        }//for i
        return output;
    }

    @FXML
    private void quitarFiltrosPulsed(){
    	fechaField.setText("");
    	conceptoField.setText("");
    	tipoField.setText("");
    	variedadField.setText("");
    	cantidadField.setText("");
    	tamanyoField.setText("");
    	comboEs.setValue("-");
    	ArrayList<VentaTabla> temp = VentaTabla.toTablaList(OC.DameOC().getVentas());
		ObservableList<VentaTabla> items = new Utilidades<VentaTabla>().toObservableList(temp);
		ventasTable.setItems(items);

    }
    @FXML
    private void filtrarPulsed(){
    	if(validate()){
    		borrarSeleccion.setDisable(true);
    		int cantidad = -1;
    		String fecha = InformesController.remove(fechaField.getText().toLowerCase());
    		String hasta = InformesController.remove(hastaField.getText().toLowerCase());
    		String concepto = InformesController.remove(conceptoField.getText().toLowerCase());
    		String tipo = InformesController.remove(tipoField.getText().toLowerCase());
    		String variedad =InformesController.remove(variedadField.getText().toLowerCase());
    		String es = (String) comboEs.getValue();
    		String tam = tamanyoField.getText();
    		if(!cantidadField.getText().equals("")){
    			cantidad = Integer.parseInt(cantidadField.getText());
    		}
    		OC oc = OC.DameOC();
    		ArrayList<Venta> ventas = oc.getVentas();
    		ArrayList<Venta> res = new ArrayList<Venta>();
    		for(int i = 0; i<ventas.size();i++){
    			if(comprobar(ventas.get(i),concepto,fecha,tipo,variedad,cantidad,tam,es,hasta)){
    				res.add(ventas.get(i));
    			}
    		}
    		if(res.size()<=0){
    			Alert alert = new Alert(AlertType.INFORMATION);
    			alert.setTitle("Error");
    			alert.setHeaderText(null);
    			alert.setContentText("No existe ninguna fila que cumpla todos los requisitos especificados");
    			alert.showAndWait();
    		}else{
    			ArrayList<VentaTabla> temp = VentaTabla.toTablaList(res);
    			ObservableList<VentaTabla> items = new Utilidades<VentaTabla>().toObservableList(temp);
    			ventasTable.setItems(items);
    		}

    	}
    }

    @FXML
    private void ocultarPulsed(){
    	int selectedIndex = ventasTable.getSelectionModel().getSelectedIndex();
	    if(selectedIndex >=0){
	    	ventasTable.getItems().remove(selectedIndex);
	    }
    }

    public static int compareFechas(String f1, String f2){
    	String[] fe1 = f1.split("/");
    	String[] fe2 = f2.split("/");
    	if(fe1.length<3 || fe2.length <3){
    		return 10;
    	}
    	for(int i = 0; i<3;i++){
    		try{
    			Integer.parseInt(fe1[i]);
    			Integer.parseInt(fe2[i]);
    		}catch(Exception e){
    			return 10;
    		}
    	}
    	System.out.println(f1+" "+f2);
    	if(Integer.parseInt(fe1[2])< Integer.parseInt(fe2[2])){
    		System.out.println("hola");
    		return -1;}
    	else if(Integer.parseInt(fe1[2])> Integer.parseInt(fe2[2])){
    		System.out.println("hola2");
    		return 1;}
    	else{
    		if(Integer.parseInt(fe1[1])< Integer.parseInt(fe2[1])){
    			System.out.println("Adios");
    			return -1;}
    	 	else if(Integer.parseInt(fe1[1])> Integer.parseInt(fe2[1])){
        		System.out.println("Adios2");
    	 		return 1;}
    	 	else{
    	 		if(Integer.parseInt(fe1[0])< Integer.parseInt(fe2[0])){
        			System.out.println(-1);
    	 			return -1;}
    	 		else if(Integer.parseInt(fe1[0])> Integer.parseInt(fe2[0])){
            		System.out.println(1);
    	 			return 1;}
    	 		else
    	 			System.out.println(fe1[0]);
    	 			System.out.println(fe2[0]);
    	 			System.out.println("lolo");
    	 			return 0;
    	 	}
    	}
   }

    public boolean estaEn(String fventa,String fdesde,String fhasta){
    	if(InformesController.compareFechas(fdesde,fventa)<=0 && InformesController.compareFechas(fventa,fhasta)<=0)
    		return true;
    	else
    		return false;
    }

    private boolean comprobar(Venta v, String concepto, String fecha, String tipo,String variedad,int cantidad,String tam, String es, String hasta){
    	if(!es.equals("-") && !v.getEs().equals(es))
    		return false;

    	if(!concepto.equals("") && !InformesController.remove(v.getConcepto().toLowerCase()).equals(concepto))
    		return false;
    	if(!fecha.equals("")&& hasta.equals("") && !InformesController.remove(v.getFecha().toLowerCase()).contains(fecha))
    		return false;
    	else if(!fecha.equals("") && !hasta.equals("") && !estaEn(v.getFecha(),fecha,hasta))
    		return false;

    	OC oc = OC.DameOC();
    	Planta p = oc.getPlant(v.getCodPlanta());
    	if(!tipo.equals("")&&!InformesController.remove(p.getTipo().getNombre().toLowerCase()).equals(tipo))
    		return false;
    	if(!variedad.equals("")&&!InformesController.remove(p.getVariedad()).toLowerCase().equals(variedad))
    		return false;
       	if(!tam.equals("")&&!(p.getTam()+"").equals(tam))
    		return false;

    	if(cantidad!=-1 &&!(v.getUnidades()==cantidad))
    		return false;
    	return true;
    }


    private boolean validate(){
    	try{
    		if(!cantidadField.getText().equals(""))
    		Integer.parseInt(cantidadField.getText());
    	}
    	catch(Exception e){
    		Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("ERROR.El valor de 'cantidad' debe ser un n�mero");
			alert.showAndWait();
			return false;
    	}
    	return true;
    }
	public void setMainApp(Principal p){
		this.mainApp=p;
	}

	public void init(){
		ObservableList<String> options =
			    FXCollections.observableArrayList(
			        "-",
			    	"E",
			        "S"
			    );
		comboEs.setItems(options);
		comboEs.setValue("-");
		OC oc = OC.DameOC();
		ArrayList<Venta> ventasList = oc.getVentas();
		ArrayList<VentaTabla> ventaTabla = VentaTabla.toTablaList(ventasList);

		if(ventaTabla.size()>0){
			ObservableList<VentaTabla> ventas= new Utilidades<VentaTabla>().toObservableList(ventaTabla);
			conceptoColumn.setCellValueFactory(new PropertyValueFactory<VentaTabla,String>("concepto"));
			sizeColumn.setCellValueFactory(new PropertyValueFactory<VentaTabla,String>("tamanyo"));
			variedadColumn.setCellValueFactory(new PropertyValueFactory<VentaTabla,String>("variedad"));
			amountColumn.setCellValueFactory(new PropertyValueFactory<VentaTabla,String>("cantidad"));
			comentColumn.setCellValueFactory(new PropertyValueFactory<VentaTabla,String>("comentario"));
			esColumn.setCellValueFactory(new PropertyValueFactory<VentaTabla,String>("es"));
			fechaColumn.setCellValueFactory(new PropertyValueFactory<VentaTabla,String>("fecha"));
			tipoColumn.setCellValueFactory(new PropertyValueFactory<VentaTabla,String>("tipo"));
			ventasTable.setItems(ventas);

		}
		Screen screen = Screen.getPrimary();
		Rectangle2D bounds = screen.getVisualBounds();
		double t = bounds.getWidth()-20;
		esColumn.setPrefWidth(t*0.02);
		conceptoColumn.setPrefWidth(t*0.10);
		fechaColumn.setPrefWidth(t*0.07);
		tipoColumn.setPrefWidth(t*0.12);
		variedadColumn.setPrefWidth(t*0.20);
		sizeColumn.setPrefWidth(t*0.05);
		amountColumn.setPrefWidth(t*0.05);
		comentColumn.setPrefWidth(t*0.39);
	}

	@FXML
	private void generarPulsado(){
		  File fichero = null;
	        PrintWriter pw = null;
	        try
	        {
	        	Calendar c = Calendar.getInstance();
				String dia = Integer.toString(c.get(Calendar.DATE));
				String mes = Integer.toString(c.get(Calendar.MONTH)+1);
				String annio = Integer.toString(c.get(Calendar.YEAR));
				String hora = Integer.toString(c.get(Calendar.HOUR_OF_DAY));
				String min = Integer.toString(c.get(Calendar.MINUTE));
				String sec = Integer.toString(c.get(Calendar.SECOND));
				String name = dia+"."+mes+"."+annio+"."+hora+"-"+min+"-"+sec+".html";
				fichero = new File("informes/"+name);
				File style = new File("estilos/estilo.css");
				pw = new PrintWriter(fichero);
				System.out.println("<link href=\""+style.getAbsolutePath() +"\" type=\"text/css\" rel=\"stylesheet\" />");
	            pw.println("<link href=\""+style.getAbsolutePath() +"\"type=\"text/css\" rel=\"stylesheet\" />");
	            pw.println("<div><h1>Viveros Herfesa</h1></div>");
	            pw.println("<br>");
	            pw.println("<table cellspacing=0>");
	            	pw.println("<tr>");
	            	  pw.println("<td class=\"gr\" valign=\"middle\"><strong>E/S</strong></td>");
	            	  pw.println("<td class=\"gr\" valign=\"middle\"><strong>Concepto</strong></td>");
	            	  pw.println("<td class=\"gr\" valign=\"middle\"><strong>Fecha</strong></td>");
	            	  pw.println("<td class=\"gr\" valign=\"middle\"><strong>Tipo</strong></td>");
	            	  pw.println("<td class=\"gr\" valign=\"middle\"><strong>Variedad</strong></td>");
	            	  pw.println("<td class=\"gr\" valign=\"middle\"><strong>Tama�o</strong></td>");
	            	  pw.println("<td class=\"gr\" valign=\"middle\"><strong>Cantidad</strong></td>");
	            	  pw.println("<td class=\"gr\" valign=\"middle\"><strong>Comentario</strong></td>");
	            	pw.println("</tr>");

	            ObservableList<VentaTabla> ventas= ventasTable.getItems();
	            for(int i = 0; i<ventas.size();i++){
	             	pw.println("<tr>");
	             	pw.println("<td class=\"nor\">"+ventas.get(i).getEs()+"</td>");
	             	pw.println("<td class=\"nor\">"+ventas.get(i).getConcepto()+"</td>");
	             	pw.println("<td class=\"nor\">"+ventas.get(i).getFecha()+"</td>");
	             	pw.println("<td class=\"nor\">"+ventas.get(i).getTipo()+"</td>");
	             	pw.println("<td class=\"nor\">"+ventas.get(i).getVariedad()+"</td>");
	             	pw.println("<td class=\"nor\">"+ventas.get(i).getVariedad()+"</td>");
	             	pw.println("<td class=\"nor\">"+ventas.get(i).getCantidad()+"</td>");
	             	pw.println("<td class=\"nor\">"+ventas.get(i).getComentario()+"</td>");
	            	pw.println("</tr>");

	            }
	            pw.println("</table>");
	            pw.close();
	            ///////

	            Alert alert = new Alert(AlertType.INFORMATION);
    			alert.setTitle("Informaci�n");
    			alert.setHeaderText(null);
    			alert.setContentText("Informe generado correctamente.�Quieres verlo ahora?");
    			ButtonType buttonTypeOne = new ButtonType("S�");
    			ButtonType buttonTypeTwo = new ButtonType("No");
    			alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);

    			Optional<ButtonType> result = alert.showAndWait();
    			if (result.get() == buttonTypeOne){
    				File fichero1 = new File("informes/"+name);
    	            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " +fichero1.getAbsolutePath());
    			}

	        } catch (Exception e) {
	            e.printStackTrace();

	        }
	}

	@FXML
	private void eliminarSeleccionPulsed(){
		 Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Alerta");
			alert.setHeaderText(null);
			alert.setContentText("Si borras esta venta no podr�s recuperarla. �Seguro que quieres hacerlo?");
			ButtonType buttonTypeOne = new ButtonType("S�");
			ButtonType buttonTypeTwo = new ButtonType("No");
			alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == buttonTypeOne){
				int selectedIndex = ventasTable.getSelectionModel().getSelectedIndex();
			    if(selectedIndex >=0){
			    	VentaTabla temp = ventasTable.getItems().get(selectedIndex);
			    	ventasTable.getItems().remove(selectedIndex);
			    	OC oc = OC.DameOC();
			    	oc.removeVenta(temp);
			    }
			}
	}

	@FXML
	private void tablaPulsed(){
		borrarSeleccion.setDisable(false);
	}
	@FXML
	private void eliminarTodasPulsed(){
		 Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Alerta");
			alert.setHeaderText(null);
			alert.setContentText("ATENCI�N: Est�s a punto de borrar TODAS las ventas del sistema. �Seguro que quieres hacerlo?");
			ButtonType buttonTypeOne = new ButtonType("S�");
			ButtonType buttonTypeTwo = new ButtonType("No");
			alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);
			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == buttonTypeOne){
				OC oc = OC.DameOC();
				oc.borrarTodasVentas();
				ventasTable.setItems(null);
			}
	}
	@FXML
	private void atrasPulsado(){
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Principal.class.getResource("/GUI/Inicio.fxml"));
			AnchorPane aux = (AnchorPane) loader.load();

			// Show the scene containing the root layout.
			Scene scene = new Scene(aux);
			mainApp.setScene(scene);
			mainApp.setFull(false);
			//link controller con this
			InicioController controller = loader.getController();
			controller.setMainApp(mainApp);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
