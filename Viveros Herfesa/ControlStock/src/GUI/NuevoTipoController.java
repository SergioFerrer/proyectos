package GUI;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class NuevoTipoController {
	private Stage dialogStage;
	private boolean okPressed;
	private String text;
	@FXML
	private TextField textField;
	
	public String getText(){
		return text;
	}
	
	public void setTextOfField(String s){
		textField.setText(s);
	}
	
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public boolean isOkPressed(){
		return okPressed;
	}
	
	@FXML
	private void okPressed(){
		okPressed=true;
		text=textField.getText();
		dialogStage.close();
	}
	
	@FXML
	private void cancelPressed(){
		okPressed=false;
		text="";
		dialogStage.close();
	}
}