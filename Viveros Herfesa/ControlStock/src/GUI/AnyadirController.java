package GUI;

import Logica.OC;
import Logica.Planta;
import Logica.Tipo;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AnyadirController {
	@FXML
	TextField tipoField;
	@FXML
	TextField tamField;
	@FXML
	TextField variedadField;
	@FXML
	TextField cantidadField;
	@FXML
	TextArea comentarioField;

	private Stage dialogStage;
	private Planta planta;
	private boolean okPulsed;
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}
	
	public void init(String tipo){
		tipoField.setText(tipo);
		tipoField.setDisable(true);
		
	}
	
	@FXML
	public void okPulsed(){
		if(validate()){
			okPulsed=true;
			Planta p = new Planta();
			p.setTipo(new Tipo(tipoField.getText()));
			p.setCantidad(Integer.parseInt(cantidadField.getText()));
			p.setTam(Integer.parseInt(tamField.getText()));
			p.setComentario(comentarioField.getText());
			p.setVariedad(variedadField.getText());
			OC oc=OC.DameOC();
			int code =oc.getMaxCode();
			p.setCod(code+1);
			this.planta=p;
			dialogStage.close();
		}
	}
	

	
	private boolean validate(){
		try{
			Integer.parseInt(tamField.getText());
		}catch(Exception e){
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("ERROR. El valor del campo 'Tama�o' debe ser un n�mero entero");
			alert.showAndWait();
			return false;
		}
		
		try{
			Integer.parseInt(cantidadField.getText());
		}catch(Exception e){
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("ERROR. El valor del campo 'Cantidad inicial' debe ser un n�mero entero");
			alert.showAndWait();
			return false;
		}
		if(variedadField.getText().equals("")){
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("ERROR.Debes a�adir una variedad");
			alert.showAndWait();
			return false;
		}
		return true;
	}
	
	public Planta getPlanta() {
		return planta;
	}

	public boolean isOkPulsed() {
		return okPulsed;
	}



	@FXML
	public void cancelPulsed(){
		planta=null;
		okPulsed = false;
		dialogStage.close();
	}

}