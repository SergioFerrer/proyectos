package GUI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import Logica.OC;
import Logica.Planta;
import Logica.Principal;
import Logica.Tipo;
import Logica.Utilidades;

public class TiposViewController {

	Principal mainApp;
	@FXML
	private ComboBox tipos;
	
	@FXML
    private TableView<Planta> plantasTable;

	@FXML
    private TableColumn<Planta, Integer> codeColumn;
    
    @FXML
    private TableColumn<Planta, Integer> sizeColumn;
	
    @FXML
    private TableColumn<Planta, String> variedadColumn;
    
    @FXML
    private TableColumn<Planta, Integer> amountColumn;
    
    @FXML
    private TableColumn<Planta, String> comentColumn;
	
    @FXML
    private Button editarButton;
    
    @FXML
    private Button eliminarButton;
	public void setMainApp(Principal p){
		this.mainApp=p;
		
	}
	
	@FXML
	public boolean showAnyadirDialog(){
		try {
	        // Load the fxml file and create a new stage for the popup dialog.
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(TiposViewController.class.getResource("anyadir.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();

	        // Create the dialog Stage.
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Nueva plantas");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(mainApp.getStage());
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);

	        // Set the dialog into the controller.
	       AnyadirController controller = loader.getController();
	       controller.setDialogStage(dialogStage);
	        

	        // Show the dialog and wait until the user closes it
	        dialogStage.showAndWait();

	        return true;
	    } catch (IOException e) {
	        e.printStackTrace();
	        return false;
	    }
		
	}
	
	@FXML
	public void showNuevoTipoDialog(){
		try {
	        // Load the fxml file and create a new stage for the popup dialog.
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(TiposViewController.class.getResource("NuevoTipo.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();

	        // Create the dialog Stage.
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Tipos");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(mainApp.getStage());
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);

	        // Set the dialog into the controller.
	       NuevoTipoController controller = loader.getController();
	       controller.setDialogStage(dialogStage);
	        

	        // Show the dialog and wait until the user closes it
	        dialogStage.showAndWait();
	        if(controller.isOkPressed()){
	        	OC oc = OC.DameOC();
	        	oc.anyadeTipo(new Tipo(controller.getText()));
	        	tipos.getItems().addAll(controller.getText());
	        	
	        }
	        
	   
	    } catch (IOException e) {
	        e.printStackTrace();
	   
	    }
	}
	
	@FXML
	private void editarPulsado(){
		try {
	        // Load the fxml file and create a new stage for the popup dialog.
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(TiposViewController.class.getResource("NuevoTipo.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();

	        // Create the dialog Stage.
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Tipos");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(mainApp.getStage());
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);

	        //cargamos el texto
	        
	        // Set the dialog into the controller.
	       NuevoTipoController controller = loader.getController();
	       controller.setDialogStage(dialogStage);
	       String opcion = (String) tipos.getValue();
	       controller.setTextOfField(opcion);

	        // Show the dialog and wait until the user closes it
	        dialogStage.showAndWait();
	        if(controller.isOkPressed()){
	        	OC oc = OC.DameOC();
	        	Alert alert = new Alert(AlertType.WARNING);
	        	alert.setTitle("Confirmaci�n");
				alert.setContentText("Est�s a punto de cambiar el tipo '"
						+ opcion + "' por '" + controller.getText() 
						+ "'. �Seguro que quieres hacerlo?");
				ButtonType buttonTypeOne = new ButtonType("S�");
			    ButtonType buttonTypeTwo = new ButtonType("No");	
			    alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);
			    
			    Optional<ButtonType> result = alert.showAndWait();
			    if (result.get() == buttonTypeOne){
			        oc.cambiarTipos(opcion, controller.getText());
			       // tipos.removeItem("Otro");
			        tipos.getItems().addAll(controller.getText());
			    } else if (result.get() == buttonTypeTwo) {
			        editarPulsado();
			    }
	        }
	      
	   
	    } catch (IOException e) {
	        e.printStackTrace();
	   
	    }
		
	}
	
	@FXML
	public void eliminarTipoPulsado(){
		tipos.getItems().remove(tipos.getValue());
	}
	@FXML
	public boolean showSacarDialog() {
	    try {
	        // Load the fxml file and create a new stage for the popup dialog.
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(TiposViewController.class.getResource("SacarDialog.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();

	        // Create the dialog Stage.
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("A�adir plantas");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(mainApp.getStage());
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);

	        // Set the dialog into the controller.
	       SacarDialogController controller = loader.getController();
	       controller.setDialogStage(dialogStage);
	        

	        // Show the dialog and wait until the user closes it
	        dialogStage.showAndWait();

	        return true;
	    } catch (IOException e) {
	        e.printStackTrace();
	        return false;
	    }
	}
	
	
	@FXML
	private void atrasPulsado(){
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Principal.class.getResource("../GUI/Inicio.fxml"));
			AnchorPane aux = (AnchorPane) loader.load();

			// Show the scene containing the root layout.
			Scene scene = new Scene(aux);
			mainApp.setScene(scene);

			//link controller con this
			InicioController controller = loader.getController();
			controller.setMainApp(mainApp);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private void onUserUpdate(){
		String t =(String) tipos.getValue();
		ArrayList<Planta> plantasList;
		OC oc = OC.DameOC();
		if(t.equals("Todos")){
			plantasList = oc.getPlants();
			eliminarButton.setDisable(true);
			editarButton.setDisable(true);
			
			
		}else{
			plantasList = oc.getPlantsByType(t);
			eliminarButton.setDisable(false);
			editarButton.setDisable(false);
		}
		ObservableList<Planta> plants= new Utilidades<Planta>().toObservableList(plantasList);
		plantasTable.setItems(plants);
		



	}
	
	public void init(){
		
		///Seting the Tipo options
		OC oc = OC.DameOC();
		ArrayList<Tipo> tiposList = oc.getTypes();
		ObservableList<String> options = 
			    FXCollections.observableArrayList(
			        "Todos"
			    );
		tipos.setItems(options);
		for(int i = 0; i<tiposList.size();i++){
			Tipo t = tiposList.get(i);
			tipos.getItems().addAll(t.getNombre());
			
		}
		///
		
		///Load all plants;
		ArrayList<Planta> plantasList = oc.getPlants();
		if(plantasList.size()>0){
			ObservableList<Planta> plants= new Utilidades<Planta>().toObservableList(plantasList);
			codeColumn.setCellValueFactory(new PropertyValueFactory<Planta,Integer>("cod"));
			 sizeColumn.setCellValueFactory(new PropertyValueFactory<Planta,Integer>("tam"));
			 variedadColumn.setCellValueFactory(new PropertyValueFactory<Planta,String>("variedad"));
			 amountColumn.setCellValueFactory(new PropertyValueFactory<Planta,Integer>("cantidad"));
			 comentColumn.setCellValueFactory(new PropertyValueFactory<Planta,String>("comentario"));
			  plantasTable.setItems(plants);
		      //plantasTable.getColumns().addAll(codeColumn, sizeColumn, variedadColumn, amountColumn,comentColumn);
			  
		}
	}
	
	
}
