package datos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Controlador {
String url = "C:\\Program Files (x86)\\SQLite\\Sqliteman-1.2.2\\Bases\\Agenda";
Connection connect;

public Connection connect(){
 try {
	 try {
		 //Aqu� cargamos el driver de SQLITE.
		 Class.forName("org.sqlite.JDBC");
	}
	catch (ClassNotFoundException e) {
		//Esto se ejecuta si hay un error con el driver de la base de datos.
		     e.printStackTrace();
	}
		     

     connect = DriverManager.getConnection("jdbc:sqlite:"+url);
     if (connect!=null) {
         System.out.println("Conectado");
     }
 }catch (SQLException ex) {
     System.err.println("No se ha podido conectar a la base de datos\n"+ex.getMessage());
 }
 return connect;
}
 public void close(){
        try {
            connect.close();
        } catch (SQLException ex) {
           System.err.println("Error cerrando la conexi�n a la base de datos"); 
        }
 }

}
