debug(3).

// Name of the manager
manager("Manager").

// Team of troop.
team("ALLIED").
// Type of troop.
type("CLASS_MEDIC").


esquina(0).


{ include("jgomas.asl") }




// Plans


/*******************************
*
* Actions definitions
*
*******************************/

/////////////////////////////////
//  GET AGENT TO AIM 
/////////////////////////////////  
/**
 * Calculates if there is an enemy at sight.
 *
 * This plan scans the list <tt> m_FOVObjects</tt> (objects in the Field
 * Of View of the agent) looking for an enemy. If an enemy agent is found, a
 * value of aimed("true") is returned. Note that there is no criterion (proximity, etc.) for the
 * enemy found. Otherwise, the return value is aimed("false")
 *
 * <em> It's very useful to overload this plan. </em>
 * 
 */
+!get_agent_to_aim
<-  ?debug(Mode); if (Mode<=2) { .println("Looking for agents to aim."); }
?fovObjects(FOVObjects);
.length(FOVObjects, Length);

?debug(Mode); if (Mode<=1) { .println("El numero de objetos es:", Length); }

if (Length > 0) {
    +bucle(0);
    
    -+aimed("false");
    
    while (aimed("false") & bucle(X) & (X < Length)) {
        
        //.println("En el bucle, y X vale:", X);
        
        .nth(X, FOVObjects, Object);
        // Object structure
        // [#, TEAM, TYPE, ANGLE, DISTANCE, HEALTH, POSITION ]
        .nth(2, Object, Type);
        
        ?debug(Mode); if (Mode<=2) { .println("Objeto Analizado: ", Object); }
        
        if (Type > 1000) {
            ?debug(Mode); if (Mode<=2) { .println("I found some object."); }
        } else {
            // Object may be an enemy
            .nth(1, Object, Team);
            .nth(6,Object,pos(XE,YE,ZE));
			.nth(4,Object,AnguloE);
			?my_formattedTeam(MyTeam);
            
            if (Team == 200) {  // Only if I'm ALLIED
				?my_position(MX,MY,MZ);
				//calcularemos la ecuacion de la recta que nos une con el enemigo para ver si en esa recta hay algun amigo
				//Vector que nos une al enemigo:
				+vector(MX-XE,MY-YE,MZ-ZE);
				//Ecuacion de la recta dada un punto (x1,y1) y un vector(v1,v2):
				//(x-x1)/v1=(y-y1)/v2
				+bucle2(0);
				+disparar(si);
				while(bucle2(IT) & (IT < Length) & disparar(si)){
					.nth(IT, FOVObjects, LoQueVeo);
					if(LoQueVeo==[_,100,_,AnguloA,_,_,pos(XA,YA,ZA)]){
						//si veo un amigo, veo si está en la linea que une el enemigo y yo	
						?vector(V1,V2,V3);
						//comprobamos la ecuacion de la recta
						if((XA-MX)/V1 == (ZA-MZ)/V3 | AnguloA==AnguloE ){
							//cumple la ecuacion de la recta
							-+disparar(no);
						}
					}
					-+bucle2(IT+1)
				}
				if(disparar(si)){
					+aimed_agent(Object);
					-+aimed("true");
				}
				
               
			   // ?debug(Mode); if (Mode<=2) { .println("Aiming an enemy. . .", MyTeam, " ", .number(MyTeam) , " ", Team, " ", .number(Team)); }
               // +aimed_agent(Object);
               // -+aimed("true");
                
            }
            
        }
        
        -+bucle(X+1);
        
    }
    
 }  

-bucle(_);
.

/////////////////////////////////
//  LOOK RESPONSE
/////////////////////////////////
+look_response(FOVObjects)[source(M)]
    <-  //-waiting_look_response;
        .length(FOVObjects, Length);
        if (Length > 0) {
            ///?debug(Mode); if (Mode<=1) { .println("HAY ", Length, " OBJETOS A MI ALREDEDOR:\n", FOVObjects); }
        };    
        -look_response(_)[source(M)];
        -+fovObjects(FOVObjects);
        //.//;
        !look.
      
        
/////////////////////////////////
//  PERFORM ACTIONS
/////////////////////////////////
/**
* Action to do when agent has an enemy at sight.
* 
* This plan is called when agent has looked and has found an enemy,
* calculating (in agreement to the enemy position) the new direction where
* is aiming.
*
*  It's very useful to overload this plan.
* 
*/
+!perform_aim_action
    <-  // Aimed agents have the following format:
        // [#, TEAM, TYPE, ANGLE, DISTANCE, HEALTH, POSITION ]
        ?aimed_agent(AimedAgent);
        ?debug(Mode); if (Mode<=1) { .println("AimedAgent ", AimedAgent); }
        .nth(1, AimedAgent, AimedAgentTeam);
        ?debug(Mode); if (Mode<=2) { .println("BAJO EL PUNTO DE MIRA TENGO A ALGUIEN DEL EQUIPO ", AimedAgentTeam);             }
        ?my_formattedTeam(MyTeam);


        if (AimedAgentTeam == 200) {
    
                .nth(6, AimedAgent, NewDestination);
                ?debug(Mode); if (Mode<=1) { .println("NUEVO DESTINO DEBERIA SER: ", NewDestination); }
          
            }
 .

/**
* Action to do when the agent is looking at.
*
* This plan is called just after Look method has ended.
* 
* <em> It's very useful to overload this plan. </em>
* 
*/
+!perform_look_action.
   /// <- ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR PERFORM_LOOK_ACTION GOES HERE.") }. 

  
/**
* Action to do if this agent cannot shoot.
* 
* This plan is called when the agent try to shoot, but has no ammo. The
* agent will spit enemies out. :-)
* 
* <em> It's very useful to overload this plan. </em>
* 
*/  
+!perform_no_ammo_action . 
   /// <- ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR PERFORM_NO_AMMO_ACTION GOES HERE.") }.
    
/**
     * Action to do when an agent is being shot.
     * 
     * This plan is called every time this agent receives a messager from
     * agent Manager informing it is being shot.
     * 
     * <em> It's very useful to overload this plan. </em>
     * 
     */
+!perform_injury_action .
    ///<- ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR PERFORM_INJURY_ACTION GOES HERE.") }. 
        

/////////////////////////////////
//  SETUP PRIORITIES
/////////////////////////////////
/**  You can change initial priorities if you want to change the behaviour of each agent  **/+!setup_priorities
    <-  +task_priority("TASK_NONE",0);
        +task_priority("TASK_GIVE_MEDICPAKS", 2000);
        +task_priority("TASK_GIVE_AMMOPAKS", 0);
        +task_priority("TASK_GIVE_BACKUP", 0);
        +task_priority("TASK_GET_OBJECTIVE",1000);
        +task_priority("TASK_ATTACK", 1000);
        +task_priority("TASK_RUN_AWAY", 1500);
        +task_priority("TASK_GOTO_POSITION", 750);
        +task_priority("TASK_PATROLLING", 500);
        +task_priority("TASK_WALKING_PATH", 1750).   



/////////////////////////////////
//  UPDATE TARGETS
/////////////////////////////////
/**
 * Action to do when an agent is thinking about what to do.
 *
 * This plan is called at the beginning of the state "standing"
 * The user can add or eliminate targets adding or removing tasks or changing priorities
 *
 * <em> It's very useful to overload this plan. </em>
 *
 */
+!update_targets: esperar(F) & (F==no)
	<-	
	
	?preparado(P);
	if(P==no){
		.wait(4000);
	}
	
	?my_position(X,Y,Z);
	?tengoQueIr(X2,Y2,Z2);
	
	
	//.println(P," ",X," ",X2," ",Y," ",Y2," ",Z," ",Z2);
	
	if(P==no & (X>=X2-2 & X<=X2+2) & (Y>=Y2-2 & Y<=Y2+2)){
		.println("Estoy listo");
		-+preparado(si);
		.my_team("fieldops_ALLIED",Coordinador);
		.concat("listo", Listo);
		.send_msg_with_conversation_id(Coordinador , tell, Listo, "Preparado para atacar");
		-+esperar(si);
		?tasks(T);
		+guardarT(T);
		+primera(si);
		-+tasks([]);//Para esperar dejamos al agente sin tareas. Cuando todos estén colocados, se las volveremos a poner
	}
	if(alAtaque(si)){
		if(primera(si)){
			?guardarT(T);
			-+primera(no);
			-+tasks(T);
			-+state(standing);
			!fsm; //Gestiona la máquina de estados
			.drop_intention(fsm); 
			!update_targets;
			!!fsm;
			
			
			
		}
		else{
			?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR UPDATE_TARGETS GOES HERE.");}
			?tasks(Tareas);
			.length(Tareas,TL);
			if(TL<=0){
				.my_team("ALLIED",E1);
				.concat("whoHasFlag", Content2);
				.send_msg_with_conversation_id(E1 , tell, Content2, "Quien tiene?");
			}
	
		}
	}
	.

+objectivePackTaken(on)
	<-
	.println("LA TENGO");
	-+tengoBandera(si);
	.my_team("ALLIED",E1);
	?my_position(X,Y,Z);
	.concat("iHaveFlag(",X, ", ", Y, ", ", Z, ")", Content2);
	.send_msg_with_conversation_id(E1 , tell, Content2, "TengoLaBandera");
	-+tenemosBandera(si);
	.	
	
	
//quien tenga la bandera nos los dirá
+iHaveFlag(X,Y,Z)[source(A)]
	<-
	?baseEn(XB,YB,ZB);
	!add_task(task(1900,"TASK_GOTO_POSITION",A,pos(XB,YB,ZB),""));
	-+tenemosBandera(si);
	-+state(standing);
.	
	
	
//Recibe mensaje de ayuda.	

+ven(X,Y,Z,Vida)[source(A)]
<-
	!add_task(task(2000,"TASK_GIVE_MEDICPAKS",A,pos(X,Y,Z),""));
	//.println("Voy a ayudarte");
	-+ayudando(si);
	-+state(standing);
.

//ayudando(no) lo usamos para ignorar peticiones de ayuda cuando ya estamos ayudando a alguien.
+ayuda(X,Y,Z,Vida)[source(A)]:ayudando(no)
<-
	//.println("Recibido mensaje de ayuda");
	?my_position(X2,Y2,Z2);
	Distancia=math.sqrt(((X2-X)*(X2-X))+((Z2-Z)*(Z2-Z)));
	.concat("miDistancia(1," ,Distancia , "," ,X2, "," , Y2, "," ,Z2,")", Content2);
	//.println(Distancia);
    .send_msg_with_conversation_id(A, tell, Content2, "miDistancia");
	//.println("Distancia enviada");
	
.
/////////////////////////////////
//  CHECK MEDIC ACTION (ONLY MEDICS)
/////////////////////////////////
/**
 * Action to do when a medic agent is thinking about what to do if other agent needs help.
 *
 * By default always go to help
 *
 * <em> It's very useful to overload this plan. </em>
 *
 */
 +!checkMedicAction
     <-  -+medicAction(on).
      // go to help
      
      
/////////////////////////////////
//  CHECK FIELDOPS ACTION (ONLY FIELDOPS)
/////////////////////////////////
/**
 * Action to do when a fieldops agent is thinking about what to do if other agent needs help.
 *
 * By default always go to help
 *
 * <em> It's very useful to overload this plan. </em>
 *
 */
 +!checkAmmoAction
     <-  -+fieldopsAction(on).
      //  go to help



/////////////////////////////////
//  PERFORM_TRESHOLD_ACTION
/////////////////////////////////
/**
 * Action to do when an agent has a problem with its ammo or health.
 *
 * By default always calls for help
 *
 * <em> It's very useful to overload this plan. </em>
 *
 */
+!performThresholdAction
       <-
       
       ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR PERFORM_TRESHOLD_ACTION GOES HERE.") }
       
       ?my_ammo_threshold(At);
       ?my_ammo(Ar);
       
       if (Ar <= At) { 
          ?my_position(X, Y, Z);
		  
       
       
       }
       
       ?my_health_threshold(Ht);
       ?my_health(Hr);
       
       if (Hr <= Ht) {  
			?my_position(X, Y, Z);
			!add_task(task(2000,"TASK_GIVE_MEDICPAKS",A,pos(X,Y,Z),""));
			//.println("Me curo a mi mismo");
			-+state(standing);

        }
       .
       
/////////////////////////////////
//  ANSWER_ACTION_CFM_OR_CFA
/////////////////////////////////


    
+cfm_agree[source(M)]
   <- ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR cfm_agree GOES HERE.")};
      -cfm_agree.  

+cfa_agree[source(M)]
   <- ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR cfa_agree GOES HERE.")};
      -cfa_agree.  

+cfm_refuse[source(M)]
   <- ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR cfm_refuse GOES HERE.")};
      -cfm_refuse.  

+cfa_refuse[source(M)]
   <- ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR cfa_refuse GOES HERE.")};
      -cfa_refuse.  


	  
+posIni(X,Y,Z)
	<- 
	+tengoQueIr(X,Y,Z);
	!add_task( task(1900, "TASK_GOTO_POSITION", M, pos(X, Y, Z), ""));
	-+state(standing);
	!update_targets;

.

+alAtaque(si)
	<-
	-+esperar(no);
	.wait(3000);
	!add_task(task(2000,"TASK_GOTO_POSITION",M,pos(NuevaX,Y,NuevaZ),""));
	-+state(standing);
	!update_targets;
	
.
/////////////////////////////////
//  Initialize variables
/////////////////////////////////

+!init
   <- ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR init GOES HERE.")}
	?my_position(X,Y,Z);
	+baseEn(X,Y,Z);
	+ayudando(no);
	+tenemosBandera(no);
	+tengoBandera(no);
	.wait(2000);
	+preparado(no);
	+esperar(no);
	.my_team("fieldops_ALLIED",Coordinador);
	.concat("posInicial", Inicial);
	.send_msg_with_conversation_id(Coordinador , tell, Inicial, "Dime Pos inicial.");

   .  

