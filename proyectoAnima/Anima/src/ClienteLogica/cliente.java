/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ClienteLogica;
import java.util.*;
import java.io.*;
import java.net.*;
/**
/**
 *
 * @author Fernando
 */
public class cliente {
    public static final String IPSERVER ="192.168.1.5"; 
    Socket s;
    PrintWriter salida;
    Scanner entrada;
    //Solo conecta, dice hola,espera al adversario y configura entrada y salida
    public void conectar(int puerto){
         try{
            s = new Socket(IPSERVER,puerto);
            System.out.println("Conectado de nuevo");
            salida = new PrintWriter(s.getOutputStream(),true);
            salida.println("Hola server");
            entrada = new Scanner(s.getInputStream());
            System.out.println(entrada.nextLine());
            
        }catch(UnknownHostException e){
            System.err.println("Error en la direccion del host");
        }catch(IOException ex){
            System.err.println("Error de IO");
        }
        
    }
    
    
    public String inicio(){
        String str="";
        String str2="";
        str=entrada.nextLine();
        System.out.println("Mis personajes son "+str);
        str2=entrada.nextLine();
        str+=" "+str2;
        System.out.println("Mis dones son "+str2);
        str2=entrada.nextLine();
        str+=" "+str2;
        System.out.println("Las zonas son "+str2);
        str2=entrada.nextLine();
        str+=" "+str2;
        System.out.println("Los eventos son "+str2);
        
        return str;
    
    }
    
    public int pidePuerto(){
        int puerto=-1;
        try{
        Socket conf = new Socket(IPSERVER,6999);
        Scanner t = new Scanner(conf.getInputStream());
        puerto = Integer.parseInt(t.nextLine());
        conf.close();
        
        }catch(Exception e){
            System.out.println("Error en pidePuerto de cliente.java");
            System.out.println(e);
        }
        
        return puerto;
    }
    
    public String empezar(){
     int p =pidePuerto();
     System.out.println("Me han dado el puerto "+p);
     conectar(p);
     return inicio();
    }
 public static void main (String[] args){
     cliente c = new cliente();
     System.out.println(c.empezar());
 } 
    

}