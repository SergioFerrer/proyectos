#!/usr/bin/env python # -- coding: utf-8 -- –
#Autores: Sergio Ferrer Sánchez y Luis Cebrián Chuliá
import sys
import codecs
import re
import os
import nltk
from nltk.corpus import stopwords 
from nltk.stem import SnowballStemmer 
from os import listdir
try:
    import cPickle as pickle
except:
    import pickle
 
 
def save_object(obj, file_name):
    with open(file_name, 'wb') as fh:
        pickle.dump(obj, fh, pickle.HIGHEST_PROTOCOL)

def quitarSimbolos(texto):
    texto =re.sub('[%s]' % re.escape(u"/#~=_!¡'(),-*@&%ºª+.:;¬·|{}[]?[]«»¿"+u'"'), '', texto)
    return texto

#def remove_stopwords(text, language = 'spanish'):
#    stopwords = nltk.corpus.stopwords.words(language)
#    for i in range(len(stopwords)):
#        stopwords[i] = stopwords[i]
#    result = [w for w in text if w.lower() not in stopwords]
#    return result 

def stemmer(text):
    stemming = SnowballStemmer("spanish")
    for palabra in range(len(text)):
        text[palabra]=stemming.stem(text[palabra])
    return text

def getText(etiqueta,texto):
    ind1 = texto.index("<"+etiqueta+">")+ len("<"+etiqueta+">")
    ind2 = texto.index("</"+etiqueta+">")
    return texto[ind1:ind2]

def quitaRepetidos(dic):
    for i in dic:
        dic[i] = list(set(dic[i]))
    return dic

if len(sys.argv) < 3:
    print "Uso: 'python indexador.py <directorio de noticias> <nombre del indice>'"
    sys.exit() 


directorio = "./"+sys.argv[1]
indice = sys.argv[2]

i = 1
ficheros = {}
indiceInvertido = {}
indiceInvertidoStem = {}
indiceTitulos = {}
indiceInvertidoTitulos = {}
indiceInvertidoTitulosStem = {}

indiceTextos = {}
indiceInvertidoFechas = {}
indiceInvertidoCategorias = {}

for cosa in listdir(directorio):
    (nombreFichero, extension) = os.path.splitext(cosa)
    if extension == ".sgml": 
        #f = open(directorio+"/"+nombreFichero+extension, 'r')
        f = codecs.open(directorio+"/"+nombreFichero+extension,'r','utf-8')
        fichero = f.read()
        ficheros[i] = nombreFichero+extension
        new = fichero.split("<DOC>")
        for count in range(1,len(new)):
            texto = getText("TEXT",new[count])
            titulo = getText("TITLE",new[count])
            fecha = getText("DATE",new[count])
            categoria = getText("CATEGORY",new[count])

            #Eliminar tabuladores
            titulo = titulo.split()
            titulo = " ".join(titulo)

            texto = quitarSimbolos(texto)
            texto = texto.lower()
            terminos = texto.split()
            terminosStem = terminos[:]
            terminosStem = stemmer(terminosStem)
            ID = (i,count)
            indiceTextos[ID] = texto 
            indiceTitulos[ID] = titulo
            
            #Construcción del indice invertido de terminos en titulos
            titulo = quitarSimbolos(titulo)
            titulo = titulo.lower()
            textoTitulo = titulo.split()
            textoTituloStem = stemmer(textoTitulo)
            for term in textoTitulo:
                if indiceInvertidoTitulos.has_key(term):
                    indiceInvertidoTitulos[term].append(ID)
                else:
                    indiceInvertidoTitulos[term] = [ID]

            for term in textoTituloStem:
                if indiceInvertidoTitulosStem.has_key(term):
                    indiceInvertidoTitulosStem[term].append(ID)
                else:
                    indiceInvertidoTitulosStem[term] = [ID]
            #Construcción del indice invertido de categorias
            categoria = categoria.lower()
            if indiceInvertidoCategorias.has_key(categoria):
                indiceInvertidoCategorias[categoria].append(ID)
            else:
                indiceInvertidoCategorias[categoria] = [ID]

            #Construcción del indice invertido de fechas
            if indiceInvertidoFechas.has_key(fecha):
                indiceInvertidoFechas[fecha].append(ID)
            else:
                indiceInvertidoFechas[fecha] = [ID]

            #Construcción del indice invertido de terminos
            for t in terminos:
                if indiceInvertido.has_key(t):
                    indiceInvertido[t].append(ID)
                else:
                    indiceInvertido[t] = [ID]

            for t in terminosStem:
                if indiceInvertidoStem.has_key(t):
                    indiceInvertidoStem[t].append(ID)
                else:
                    indiceInvertidoStem[t] = [ID]
    i+=1

indiceInvertido = quitaRepetidos(indiceInvertido)
indiceInvertidoTitulos = quitaRepetidos(indiceInvertidoTitulos)
indiceInvertidoTitulosStem = quitaRepetidos(indiceInvertidoTitulosStem)
indiceInvertidoStem = quitaRepetidos(indiceInvertidoStem)
data = [indiceInvertido,indiceTitulos,ficheros,indiceTextos,indiceInvertidoFechas,indiceInvertidoCategorias,indiceInvertidoTitulos,indiceInvertidoTitulosStem,indiceInvertidoStem]
save_object(data,indice)
