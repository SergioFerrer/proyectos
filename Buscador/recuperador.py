#!/usr/bin/env python # -- coding: utf-8 -- –
#Autores: Sergio Ferrer Sánchez y Luis Cebrián Chuliá
import sys
import re
import os
import nltk
import string
from nltk.corpus import stopwords 
from nltk.stem import SnowballStemmer 
from os import listdir

try:
    import cPicke as pickle
except:
    import pickle

def load_object(file_name):
    with open(file_name, 'rb') as fh:
        obj = pickle.load(fh)
    return obj

def remove_stopwords(text, language = 'spanish'):
    stopwords = nltk.corpus.stopwords.words(language)
    
    result = [w for w in text if w.lower() not in stopwords]
    #para la ampliacion de headline:...
    for i in result:
        if ":" in i and i[string.index(i,":")+1:len(i)] in stopwords:
            result.remove(i)
    return result 

def intersection(list1,list2):
    i = 0 
    j = 0
    res = []
    while i < len(list1) and j < len(list2):
        if list1[i]==list2[j]:
            res.append(list1[i])
            i+=1
            j+=1
        elif list1[i]<list2[j]:
            i+=1 
        else:
            j+=1

    return res

def intersectionAll(lists):
    aux = lists[0]
    for i in lists:
        aux = intersection(aux,i)
    return aux


def snippet(query,text):
    s = "..."
    for i in query:
        if i not in ["AND","OR","NOT"] and ":" not in i:
            try:
                if STEM == 1:
                    text = text.split()
                    textStem = text[:]
                    textStem = stemmer(textStem)
                    pos = textStem.index(i)
                else:
                    pos = text.index(i)
                if STEM == 1:
                    text = " ".join(text)
                s+=text[max(pos-40,0):min(pos+40,len(text))]+"..."
            except:
                pass
        elif "text:" in i:
            try:
                term = i[i.index(":")+1:]
                
                if STEM == 1:
                    text = text.split()
                    textStem = text[:]
                    textStem = stemmer(textStem)
                    pos = textStem.index(term)
                else:
                    pos = text.index(i)
                if STEM == 1:
                    text = " ".join(text)
                s+=text[max(pos-40,0):min(pos+40,len(text))]+"..."
            except:
                pass
    return s

def OR(list1,list2):
    i = 0
    j = 0
    res = []
    while i < len(list1) and j < len(list2):
        if list1[i]==list2[j]:
            res.append(list1[i])
            i+=1
            j+=1
        elif list1[i]<list2[j]:
            res.append(list1[i])
            i+=1
        else:
            res.append(list2[j])
            j+=1
    while i < len(list1):
        res.append(list1[i])
        i+=1
    while j < len(list2):
        res.append(list2[j])
        j+=1
    return res

def ANDNOT(list1,list2):
    i = 0
    j = 0
    res = []
    while i < len(list1) and j < len(list2):
        if list1[i]==list2[j]:
            i+=1
            j+=1
        elif list1[i]<list2[j]:
            res.append(list1[i])
            i+=1
        else:
            j+=1
    while i < len(list1):
        res.append(list1[i])
        i+=1
    return res

def ORNOT(list1,list2):
    if STEM == 1:
        res = [item for sublist in  indiceInvertidoStem.values() for item in sublist]
    else:
        res = [item for sublist in  terminos.values() for item in sublist]

    res = list(set(res))
    res.sort()
    res = ANDNOT(res,list2)
    res = OR(list1,res)
    return res

def primerTermino(query,dic):
    try:
        if query.startswith("headline:"):
            term = query[query.index(":")+1:]
            if STEM == 1:
                res = indiceInvertidoTitulosStem[term]
            else:
                res = indiceInvertidoTitulos[term]
            res.sort()
        elif query.startswith("category:"):
            term = query[query.index(":")+1:]
            res = indiceInvertidoCategorias[term]
            res.sort()
        elif query.startswith("date:"):
            term = query[query.index(":")+1:]
            res = indiceInvertidoFechas[term]
            res.sort()
        elif query.startswith("text:"):
            term = query[query.index(":")+1:]
            if STEM == 1:
                res = indiceInvertidoStem[term]
            else:
                res = dic[term]
        else: 
            if STEM == 1:
                res = indiceInvertidoStem[query]
            else:
                res = dic[query] 
    except:
        res = []
    return res

def stemmer(text):
    stemming = SnowballStemmer("spanish")
    for palabra in range(len(text)):
        if text[palabra] not in ["AND","NOT","OR"] and ":" not in text[palabra]:
            text[palabra]=stemming.stem(text[palabra])
        elif "headline:" in text[palabra] or "text:" in text[palabra]:
            pos = text[palabra].index(":")+1
            term = text[palabra][pos:]
            text[palabra] = text[palabra][:pos]+stemming.stem(term)
    return text

def quitaStop(queryList):
    
    queryList = remove_stopwords(queryList)
    i=0
    while i < len(queryList) and queryList[i] in ["AND","OR","NOT"]:
        i+=1
    queryList = queryList[i:]

    i=len(queryList)-1
    while i !=-1 and queryList[i] in ["AND", "OR","NOT"]:
        i-=1
    queryList=queryList[:i+1]
    
    for i in range(len(queryList)):
        if queryList[i] in ["NOT","AND","OR"]:
            if queryList[i+1] in ["AND","OR"]:
                queryList[i] = "$"
            elif queryList[i+1] == "NOT" and queryList[i+2] in ["AND","OR"]:
                queryList[i] = "$"
                queryList[i+1] = "$"
    while "$" in queryList:
        queryList.remove("$")
    
    return queryList

def queryResult(queryList,dic):
    res = []
    if len(queryList) >= 1:
        res = primerTermino(queryList[0],dic)
    count = 0
    for i in queryList:
        if i in ["AND","NOT","OR"]:
            if i == "AND":
                if queryList[count+1] == "NOT":
                    res = ANDNOT(res,primerTermino(queryList[count+2],dic))
                else:
                    res = intersection(res,primerTermino(queryList[count+1],dic))
            elif i == "OR":
                if queryList[count+1] == "NOT":
                    res = ORNOT(res,primerTermino(queryList[count+2],dic))
                else:
                    res = OR(res,primerTermino(queryList[count+1],dic))
        count+=1
    return res
                
STOP = 0
STEM = 0

if len(sys.argv) < 2:
    print "Uso: 'python recuperador.py <nombre fichero indices>'\n Parametro opcional:\n\t Stemming -> -st y Stopwords -> -sw"
    sys.exit()
if "-sw" in sys.argv:
    STOP = 1
if "-st" in sys.argv:
    STEM = 1

data = load_object(sys.argv[1])

terminos = data[0]
titulos = data[1]
ficheros = data[2]
textos = data[3]
indiceInvertidoFechas = data[4]
indiceInvertidoCategorias = data[5]
indiceInvertidoTitulos = data[6]
indiceInvertidoTitulosStem = data[7]
indiceInvertidoStem = data[8]
query = " "

query =raw_input("Introduzca su query:").decode("utf8")
while query != '':
    queryList=query.split()
    if STOP ==1:
        queryList = quitaStop(queryList)
    if STEM == 1:
        queryList = stemmer(queryList)
    dicAux={}
    
    for term in queryList:
        if term not in ["AND","NOT","OR"]:
            try:
                if STEM == 1:
                    dicAux[term] = indiceInvertidoStem[term]
                else:
                    dicAux[term] = terminos[term]
                dicAux[term].sort()
            except:
                dicAux[term]=[]
    results = queryResult(queryList,dicAux)

    print("NUMERO DE RESULTADOS: " + str(len(results)))
    print("----------------------------------------------")
    #Solo para depurar 
    #results = results[0:4]
    if len(results) <=2:
        for i in results:
            print(titulos[i])
            print(textos[i])
            print("----------------------------------------------")
    elif 3 <= len(results) <= 5:
        for i in results:
            print(titulos[i])
            print(snippet(queryList,textos[i]))
            print("----------------------------------------------")
    else:
        for i in range(min(10,len(results))):
            print(titulos[results[i]])
            print("----------------------------------------------")
    
    query =raw_input("Introduzca su query:").decode("utf8")
        
